class @MonthlyGmudPieChartWidgetRenderer extends BaseTitledWidgetRenderer
    renderContent: (container, data) ->
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: true,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
                renderTo: 'monthlypiechart'
            },
            title: {
                text: 'GMUDs por tipo'
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                       enabled: true,
                       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                       style: {
                           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                       }
                    }
                }
            },
            series: [{
                name: 'Quantidade'
                data: [
                    {
                        name: 'Emergenciais'
                        y: data.emergenciais
                    }
                    {
                        name: 'Planejados'
                        y: data.planejadas
                    }
                ]
            }]
        });