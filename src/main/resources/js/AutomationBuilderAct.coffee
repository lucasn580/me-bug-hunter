@EditBuilderActionViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/customBuilder/editAction")
                size:"lg"
                title: Catalog.getMessage "Editar ação"
              )

    next: next
)()
