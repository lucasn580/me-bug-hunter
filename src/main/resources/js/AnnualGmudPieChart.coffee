@GmudTypeChartWidgetRenderer = (->

    renderGraph = () ->
        $chartData = []
        getData($chartData)

    getData = (data) ->
        utils.get(url:utils.getCustomRootBaseUrl('/do/Management/getGmuds'), (result) =>
            $emergenciais = 0
            $planejados = 0
            for gmud in result
                $emergenciais = $emergenciais + gmud.emergenciais
                $planejados = $planejados + gmud.planejados

            render($emergenciais, $planejados))

    render = (emerg, plan) ->
        new Highcharts.Chart({
            chart: {
                plotBackgroundColor: true,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
                renderTo: 'gmudpiechart'
            },
            title: {
                text: 'GMUDs por tipo'
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                       enabled: true,
                       format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                       style: {
                           color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                       }
                    }
                }
            },
            series: [{
                name: 'Quantidade'
                data: [
                    {
                        name: 'Emergenciais'
                        y: emerg
                    }
                    {
                        name: 'Planejados'
                        y: plan
                    }
                ]
            }]
        });

    renderGraph: renderGraph
)()