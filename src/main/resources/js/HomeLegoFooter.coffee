colors = [
  'red'
  'pink'
  'purple'
  'deep-purple'
  'indigo'
  'blue'
  'light-blue'
  'cyan'
  'teal'
  'green'
  'light-green'
  'lime'
  'yellow'
  'amber'
  'orange'
  'deep-orange'
  'brown'
  'grey'
  'blue-grey'
  'white'
]

$('.brick').each ->
  color = colors[Math.round(Math.random() * colors.length)]
  $(this).addClass 'b-' + color
  return

$('.top-bar').each ->
  color = colors[Math.round(Math.random() * colors.length)]
  $(this).addClass 'b-' + color
  return
