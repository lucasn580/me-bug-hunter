class @MonthlyGmudBarChartWidgetRenderer extends BaseTitledWidgetRenderer
    renderContent: (container, data) ->
        chartData = []
        utils.get(url:utils.getCustomRootBaseUrl('/do/Management/getMonthlyGmudsModalChart/'+data.month), (result) =>
            names = []
            newArray = new Map()
            content = ''
            obj = new Map()

            if data.type in ['classification']
                result.forEach (array) ->
                    obj = objToStrMap(array.classifications)
                    content = 'monthlybarchart'

            if data.type in ['customer']
                result.forEach (array) ->
                    obj = objToStrMap(array.customers)
                    content = 'monthlycustbarchart'

            obj.forEach (value, key) ->
                chartData.push({
                    "name": key
                    "y": value
                })
                names.push(key)
                return

            render(content, data.title, chartData, names))

    objToStrMap = (obj) ->
        strMap = new Map
        for key in Object.keys(obj)
            if strMap.has(key)
                strMap.set(key, obj[key] + strMap.get(key))
            else
                strMap.set(key, obj[key])
        strMap

    render = (content, title, data, names) ->
        new Highcharts.Chart({
            chart: {
                type: 'bar'
                renderTo: content
            },
            title: {
                text: title
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions:
                bar:
                    dataLabels:
                        enabled: true
            xAxis:
                categories: names
            yAxis:
                title:
                    text: 'Quantidade'
                dataLabels:
                    enabled: false
            series: [{
                name: 'Quantidade'
                data: data
            }]
        });