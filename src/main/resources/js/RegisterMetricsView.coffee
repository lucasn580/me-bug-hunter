@RegisterMetricsViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Management/metrics/register/"+e.data.month)
                size:"md"
                title: Catalog.getMessage "Nova métrica mensal"
              )

    newMetrics = () ->
        $values = []

        count = 2
        while count <= 7
            $value = $('#newMetric > div:nth-child('+count+') > input').val()
            $values.push($value)
            count++

        utils.postJSON
            url: utils.getCustomRootBaseUrl "/do/Management/metrics/register/new/"
            data: $values

    next: next
    newMetrics: newMetrics
)()
