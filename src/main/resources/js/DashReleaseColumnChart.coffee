class @DashboardColumnChartWidgetRenderer extends BaseTitledWidgetRenderer
    renderContent: (container, data) ->
        urlRoute = ''
        if data.type == 'passedIssues'
            urlRoute = 'getReleasePassedIssues'
            data.name = 'Status "aguardando build Prod" / "fechado"'

        if data.type == 'returnedIssues'
            urlRoute = 'getReleaseReturnedIssues'
            data.name = 'Qtd. Devolução'

        if data.type == 'buildIssues'
            urlRoute = 'getBuildsIssues'
            data.name = 'Builds pontuais'

        if data.type == 'buildImpactIssues'
            urlRoute = 'getBuildsImpactIssues'
            data.name = 'Chamados de impacto'

        utils.get(url:utils.getCustomRootBaseUrl('/do/Management/' + urlRoute), (result) =>
            chartData = []
            names = []
            issues = objToStrMap(result)
            issues.forEach (count, date) ->
                chartData.push({
                    "name": date
                    "y": count
                })
                names.push(date)
                return

            render(chartData, data, names))

    objToStrMap = (obj) ->
        strMap = new Map
        for k in Object.keys(obj)
            strMap.set k, obj[k]
        strMap

    render = (chartData, data, names) ->
        new Highcharts.Chart({
            chart: {
                type: 'column'
                renderTo: data.divId
            },
            title: {
                text: data.title
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions:
                bar:
                    dataLabels:
                        enabled: true
            xAxis:
                categories: names
            yAxis:
                title:
                    text: 'Quantidade'
                dataLabels:
                    enabled: false

            series: [{
                name: data.name
                data: chartData
            }]
        });