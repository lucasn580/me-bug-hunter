@InformationsFlowViewCoffee = (->

    testCase = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/informationsFlow/"+e.data.id)
                size:"lg"
                title: Catalog.getMessage "Informações do fluxo"
              )

    bdd = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/informationsBDDFlow/"+e.data.id)
                size:"lg"
                title: Catalog.getMessage "Informações do fluxo"
              )

    testCase: testCase
    bdd: bdd
)()
