@FailuresViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/getFailures/"+e.data.id)
                size:"lg"
                title: Catalog.getMessage "Falhas encontradas"
              )

    next: next
)()
