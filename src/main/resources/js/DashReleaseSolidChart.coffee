class @DashboardSolidChartWidgetRenderer extends BaseTitledWidgetRenderer
    renderContent: (container) ->
        utils.get(url:utils.getCustomRootBaseUrl('/do/Management/getReleaseSpeed'), (result) =>
            render(result))

    render = (chartData) ->
        new Highcharts.Chart({
            chart: {
                type: 'solidgauge'
                renderTo: 'solidRelease'
            },
            title: {
                text: 'Velocidade de evolução da Release'
            },
            plotOptions:
                solidgauge:
                    dataLabels:
                        y: 5
                        borderWidth: 0
            pane:
                center: ['50%', '70%'],
                size: '140%'
                startAngle: -90
                endAngle: 90
                background:
                    backgroundColor: '#EEE'
                    innerRadius: '60%'
                    outerRadius: '100%'
                    shape: 'arc'
            yAxis:
                min: 0
                max: 100
                lineWidth: 0
                tickAmount: 2
                title:
                    y: 80
                    text: 'Conclusão'
                labels:
                    y: 15

            series: [{
                name: 'Percentual',
                data: [chartData]
            }]
        });