(($, window, document) ->
  hasTouch = 'ontouchstart' of document
  hasPointerEvents = do ->
    el = document.createElement('div')
    docEl = document.documentElement
    if !('pointerEvents' of el.style)
      return false
    el.style.pointerEvents = 'auto'
    el.style.pointerEvents = 'x'
    docEl.appendChild el
    supports = window.getComputedStyle and window.getComputedStyle(el, '').pointerEvents == 'auto'
    docEl.removeChild el
    ! !supports
  defaults =
    listNodeName: 'ol'
    itemNodeName: 'li'
    rootClass: 'dd'
    listClass: 'dd-list'
    itemClass: 'dd-item'
    dragClass: 'dd-dragel'
    handleClass: 'dd-handle'
    collapsedClass: 'dd-collapsed'
    placeClass: 'dd-placeholder'
    noDragClass: 'dd-nodrag'
    emptyClass: 'dd-empty'
    expandBtnHTML: '<button data-action="expand" type="button">Expand</button>'
    collapseBtnHTML: '<button data-action="collapse" type="button">Collapse</button>'
    group: 0
    maxDepth: 5
    threshold: 20

  Plugin = (element, options) ->
    @w = $(document)
    @el = $(element)
    @options = $.extend({}, defaults, options)
    @init()
    return

  Plugin.prototype =
    init: ->
      list = this
      list.reset()
      list.el.data 'nestable-group', @options.group
      list.placeEl = $('<div class="' + list.options.placeClass + '"/>')
      $.each @el.find(list.options.itemNodeName), (k, el) ->
        list.setParent $(el)
        return
      list.el.on 'click', 'button', (e) ->
        if list.dragEl
          return
        target = $(e.currentTarget)
        action = target.data('action')
        item = target.parent(list.options.itemNodeName)
        if action == 'collapse'
          list.collapseItem item
        if action == 'expand'
          list.expandItem item
        return

      onStartEvent = (e) ->
        handle = $(e.target)
        if !handle.hasClass(list.options.handleClass)
          if handle.closest('.' + list.options.noDragClass).length
            return
          handle = handle.closest('.' + list.options.handleClass)
        if !handle.length or list.dragEl
          return
        list.isTouch = /^touch/.test(e.type)
        if list.isTouch and e.touches.length != 1
          return
        e.preventDefault()
        list.dragStart if e.touches then e.touches[0] else e
        return

      onMoveEvent = (e) ->
        if list.dragEl
          e.preventDefault()
          list.dragMove if e.touches then e.touches[0] else e
        return

      onEndEvent = (e) ->
        if list.dragEl
          e.preventDefault()
          list.dragStop if e.touches then e.touches[0] else e
        return

      if hasTouch
        list.el[0].addEventListener 'touchstart', onStartEvent, false
        window.addEventListener 'touchmove', onMoveEvent, false
        window.addEventListener 'touchend', onEndEvent, false
        window.addEventListener 'touchcancel', onEndEvent, false
      list.el.on 'mousedown', onStartEvent
      list.w.on 'mousemove', onMoveEvent
      list.w.on 'mouseup', onEndEvent
      return
    serialize: ->
      data = undefined
      depth = 0
      list = this

      step = (level, depth) ->
        array = []
        items = level.children(list.options.itemNodeName)
        items.each ->
          li = $(this)
          item = $.extend({}, li.data())
          sub = li.children(list.options.listNodeName)
          if sub.length
            item.children = step(sub, depth + 1)
          array.push item
          return
        array

      data = step(list.el.find(list.options.listNodeName).first(), depth)
      data
    serialise: ->
      @serialize()
    reset: ->
      @mouse =
        offsetX: 0
        offsetY: 0
        startX: 0
        startY: 0
        lastX: 0
        lastY: 0
        nowX: 0
        nowY: 0
        distX: 0
        distY: 0
        dirAx: 0
        dirX: 0
        dirY: 0
        lastDirX: 0
        lastDirY: 0
        distAxX: 0
        distAxY: 0
      @isTouch = false
      @moving = false
      @dragEl = null
      @dragRootEl = null
      @dragDepth = 0
      @hasNewRoot = false
      @pointEl = null
      return
    expandItem: (li) ->
      li.removeClass @options.collapsedClass
      li.children('[data-action="expand"]').hide()
      li.children('[data-action="collapse"]').show()
      li.children(@options.listNodeName).show()
      return
    collapseItem: (li) ->
      lists = li.children(@options.listNodeName)
      if lists.length
        li.addClass @options.collapsedClass
        li.children('[data-action="collapse"]').hide()
        li.children('[data-action="expand"]').show()
        li.children(@options.listNodeName).hide()
      return
    expandAll: ->
      list = this
      list.el.find(list.options.itemNodeName).each ->
        list.expandItem $(this)
        return
      return
    collapseAll: ->
      list = this
      list.el.find(list.options.itemNodeName).each ->
        list.collapseItem $(this)
        return
      return
    setParent: (li) ->
      if li.children(@options.listNodeName).length
        li.prepend $(@options.expandBtnHTML)
        li.prepend $(@options.collapseBtnHTML)
      li.children('[data-action="expand"]').hide()
      return
    unsetParent: (li) ->
      li.removeClass @options.collapsedClass
      li.children('[data-action]').remove()
      li.children(@options.listNodeName).remove()
      return
    dragStart: (e) ->
      mouse = @mouse
      target = $(e.target)
      dragItem = target.closest(@options.itemNodeName)
      @placeEl.css 'height', dragItem.height()
      mouse.offsetX = if e.offsetX != undefined then e.offsetX else e.pageX - (target.offset().left)
      mouse.offsetY = if e.offsetY != undefined then e.offsetY else e.pageY - (target.offset().top)
      mouse.startX = mouse.lastX = e.pageX
      mouse.startY = mouse.lastY = e.pageY
      @dragRootEl = @el
      @dragEl = $(document.createElement(@options.listNodeName)).addClass(@options.listClass + ' ' + @options.dragClass)
      @dragEl.css 'width', dragItem.width()
      dragItem.after @placeEl
      dragItem[0].parentNode.removeChild dragItem[0]
      dragItem.appendTo @dragEl
      $(document.body).append @dragEl
      @dragEl.css
        'left': e.pageX - (mouse.offsetX)
        'top': e.pageY - (mouse.offsetY)
      # total depth of dragging item
      i = undefined
      depth = undefined
      items = @dragEl.find(@options.itemNodeName)
      i = 0
      while i < items.length
        depth = $(items[i]).parents(@options.listNodeName).length
        if depth > @dragDepth
          @dragDepth = depth
        i++
      return
    dragStop: (e) ->
      el = @dragEl.children(@options.itemNodeName).first()
      el[0].parentNode.removeChild el[0]
      @placeEl.replaceWith el
      @dragEl.remove()
      @el.trigger 'change'
      if @hasNewRoot
        @dragRootEl.trigger 'change'
      @reset()
      return
    dragMove: (e) ->
      list = undefined
      parent = undefined
      prev = undefined
      next = undefined
      depth = undefined
      opt = @options
      mouse = @mouse
      @dragEl.css
        'left': e.pageX - (mouse.offsetX)
        'top': e.pageY - (mouse.offsetY)
      # mouse position last events
      mouse.lastX = mouse.nowX
      mouse.lastY = mouse.nowY
      # mouse position this events
      mouse.nowX = e.pageX
      mouse.nowY = e.pageY
      # distance mouse moved between events
      mouse.distX = mouse.nowX - (mouse.lastX)
      mouse.distY = mouse.nowY - (mouse.lastY)
      # direction mouse was moving
      mouse.lastDirX = mouse.dirX
      mouse.lastDirY = mouse.dirY
      # direction mouse is now moving (on both axis)
      mouse.dirX = if mouse.distX == 0 then 0 else if mouse.distX > 0 then 1 else -1
      mouse.dirY = if mouse.distY == 0 then 0 else if mouse.distY > 0 then 1 else -1
      # axis mouse is now moving on
      newAx = if Math.abs(mouse.distX) > Math.abs(mouse.distY) then 1 else 0
      # do nothing on first move
      if !mouse.moving
        mouse.dirAx = newAx
        mouse.moving = true
        return
      # calc distance moved on this axis (and direction)
      if mouse.dirAx != newAx
        mouse.distAxX = 0
        mouse.distAxY = 0
      else
        mouse.distAxX += Math.abs(mouse.distX)
        if mouse.dirX != 0 and mouse.dirX != mouse.lastDirX
          mouse.distAxX = 0
        mouse.distAxY += Math.abs(mouse.distY)
        if mouse.dirY != 0 and mouse.dirY != mouse.lastDirY
          mouse.distAxY = 0
      mouse.dirAx = newAx

      ###*
      # move horizontal
      ###

      if mouse.dirAx and mouse.distAxX >= opt.threshold
        # reset move distance on x-axis for new phase
        mouse.distAxX = 0
        prev = @placeEl.prev(opt.itemNodeName)
        # increase horizontal level if previous sibling exists and is not collapsed
        if mouse.distX > 0 and prev.length and !prev.hasClass(opt.collapsedClass)
          # cannot increase level when item above is collapsed
          list = prev.find(opt.listNodeName).last()
          # check if depth limit has reached
          depth = @placeEl.parents(opt.listNodeName).length
          if depth + @dragDepth <= opt.maxDepth
            # create new sub-level if one doesn't exist
            if !list.length
              list = $('<' + opt.listNodeName + '/>').addClass(opt.listClass)
              list.append @placeEl
              prev.append list
              @setParent prev
            else
              # else append to next level up
              list = prev.children(opt.listNodeName).last()
              list.append @placeEl
        # decrease horizontal level
        if mouse.distX < 0
          # we can't decrease a level if an item preceeds the current one
          next = @placeEl.next(opt.itemNodeName)
          if !next.length
            parent = @placeEl.parent()
            @placeEl.closest(opt.itemNodeName).after @placeEl
            if !parent.children().length
              @unsetParent parent.parent()
      isEmpty = false
      # find list item under cursor
      if !hasPointerEvents
        @dragEl[0].style.visibility = 'hidden'
      @pointEl = $(document.elementFromPoint(e.pageX - (document.body.scrollLeft), e.pageY - (window.pageYOffset or document.documentElement.scrollTop)))
      if !hasPointerEvents
        @dragEl[0].style.visibility = 'visible'
      if @pointEl.hasClass(opt.handleClass)
        @pointEl = @pointEl.parent(opt.itemNodeName)
      if @pointEl.hasClass(opt.emptyClass)
        isEmpty = true
      else if !@pointEl.length or !@pointEl.hasClass(opt.itemClass)
        return
      # find parent list of item under cursor
      pointElRoot = @pointEl.closest('.' + opt.rootClass)
      isNewRoot = @dragRootEl.data('nestable-id') != pointElRoot.data('nestable-id')

      ###*
      # move vertical
      ###

      if !mouse.dirAx or isNewRoot or isEmpty
        # check if groups match if dragging over new root
        if isNewRoot and opt.group != pointElRoot.data('nestable-group')
          return
        # check depth limit
        depth = @dragDepth - 1 + @pointEl.parents(opt.listNodeName).length
        if depth > opt.maxDepth
          return
        before = e.pageY < @pointEl.offset().top + @pointEl.height() / 2
        parent = @placeEl.parent()
        # if empty create new list to replace empty placeholder
        if isEmpty
          list = $(document.createElement(opt.listNodeName)).addClass(opt.listClass)
          list.append @placeEl
          @pointEl.replaceWith list
        else if before
          @pointEl.before @placeEl
        else
          @pointEl.after @placeEl
        if !parent.children().length
          @unsetParent parent.parent()
        if !@dragRootEl.find(opt.itemNodeName).length
          @dragRootEl.append '<div class="' + opt.emptyClass + '"/>'
        # parent root list has changed
        if isNewRoot
          @dragRootEl = pointElRoot
          @hasNewRoot = @el[0] != @dragRootEl[0]
      return

  $.fn.nestable = (params) ->
    lists = this
    retval = this
    lists.each ->
      plugin = $(this).data('nestable')
      if !plugin
        $(this).data 'nestable', new Plugin(this, params)
        $(this).data 'nestable-id', (new Date).getTime()
      else
        if typeof params == 'string' and typeof plugin[params] == 'function'
          retval = plugin[params]()
      return
    retval or lists

  return
) window.jQuery or window.Zepto, window, document

###my scripts###

$('.dd').nestable 'serialize'
$('.viewlist').on 'click', ->
  $('ol.kanban').addClass 'list'
  $('ol.list').removeClass 'kanban'
  $('menu').addClass 'list'
  $('menu').removeClass 'kanban'
  return
$('.viewkanban').on 'click', ->
  $('ol.list').addClass 'kanban'
  $('ol.kanban').removeClass 'list'
  $('menu').addClass 'kanban'
  $('menu').removeClass 'list'
  return