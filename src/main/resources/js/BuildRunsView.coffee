@ExecutionsViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/showExecutions/" + e.data.type)
                size:"lg"
                title: Catalog.getMessage "Builds"
              )

    next: next
)()
