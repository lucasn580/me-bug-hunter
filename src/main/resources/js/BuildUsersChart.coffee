@BuildsChartWidgetRenderer = (->

    renderGraph = () ->
        $chartData = []
        getData($chartData)

    getData = (data) ->
        utils.get(url:utils.getCustomRootBaseUrl('/do/Automation/getUserBuildsBody'), (result) =>
            for user in result
                data.push({
                  "name": user.analistaEmail
                  "y": user.qtdExecucoes
                })
            render(data))

    render = (renderData) ->
        new (Highcharts.Chart)({
              chart: {
                  plotBackgroundColor: true,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
                  renderTo: 'buildsChart'
              },
              title: {
                  text: 'Quantidade de execuções por analista'
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.y}</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                          style: {
                              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                          }
                      }
                  }
              },
              series: [{
                  name: 'Runs',
                  colorByPoint: true,
                  data: renderData
              }]
        });

    renderGraph: renderGraph
)()