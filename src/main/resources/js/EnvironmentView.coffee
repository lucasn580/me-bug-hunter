@EnvironmentViewCoffee = (->

    show = () ->
        $dialog = $ "<div/>", id: "environmentsDialog"
        $dialog.dialog
            show: true
            resizable: true
            urlAction: utils.getCustomRootBaseUrl("/do/Automation/showEnvironmentConfig")
            size:"md"
            title: Catalog.getMessage "Ambientes"
            buttons: [
                text: Catalog.getMessage("label.cancel")
                click: ->
                    $dialog.dialog "close"
            ,
                text: Catalog.getMessage("label.save")
                primary: true
                click: ->
                    saveEnvironments()
            ]

    saveEnvironments = () ->
        $form = $("#formEnvironments")
        data = getGridData()

        if data.length is 0
            $.MEFloatingError Catalog.getMessage("environment.user.error")
            return

        $form.form "postJSON",
            url: $form.attr "action"
            data: (formData) =>
                formData.environments = getGridData()
                formData
            success: (e) ->
                window.location = e.data.redirectTo if e.data?.redirectTo
                meceap.defaultMessageCallBack e
                unless e.errorMessages?.length > 0
                    $grid.megrid "refresh"
                    $("#environmentsDialog").dialog "close"

    addNewEnv = ->
        $form = $("#formEnvironments")
        $grid = $("#grid")

        data = getGridData()
        jsonObject = $form.jsonObject()
        newUser =
            nome: jsonObject.name
            ativo: jsonObject.active

        if !!newUser.nome and not containsEnvironment(data, newUser.nome)
            data.push newUser
            $grid.megrid("setData", data)
            $grid.megrid("ready")

    containsEnvironment = (data, name) ->
        if jlinq.from(data).equals("name", name).first()?
            return true

        return false

    renderRemoveEnvironmentColumn = (td, value, col, row, opt) ->
        removeColumn td, value, col, row, opt, remove

    removeColumn = (td, value, col, row, opt, removeFunc) ->
        icon = "me-icon icon-trash-alt"
        title = Catalog.getMessage("label.remove")

        $link = $ "<a />", style: "cursor: pointer", title: title
        $link.append $ "<i />", class: icon, style: "font-size: 18px; color:black;"
        $link.on "click", ->
            removeFunc(row)

        $link.appendTo td

    remove = (row) ->
        $.MEConfirm Catalog.getMessage("label.confirmWarning"), Catalog.getMessage("label.confirmTitle"), ->
            $grid = $("#grid")

            data = getGridData()
            position = data.indexOf(row)
            data.splice(position, 1)
            $grid.megrid("setData", data)
            $grid.megrid("ready")

    getGridData = ->
        $("#grid").megrid "getData"

    show: show
    addNewEnv: addNewEnv
    renderRemoveEnvironmentColumn: renderRemoveEnvironmentColumn
)()