@RunTestViewCoffee = (->

    onlyChecked = () ->
        validateRun("runSelectedTests", getChecked())

    performAllCts = () ->
        validateRun("runSelectedTests", getAll())

    performAllBDD = () ->
        validateRun("runBDD", getAllBDD())

    unitTest = (e) ->
        if e.data.type == 'bdd'
            validateRun("runBDD", setTestData(e.data.id, "bdd"))
        else
            validateRun("runUnitTest", e.data.id)

    validateRun = (root, data) ->
        environment = getEnvironment()
        if environment?
            #executionCommand = {}
            #executionCommand.environmentCommand = environment
            $.ajax
                type: 'POST'
                contentType: 'application/json; charset=utf-8'
                dataType: 'html'
                url: utils.getCustomRootBaseUrl("/do/Automation/" + root)
                data: data
                success: (htmlResult) ->
                    $dialog = $ "<div/>"
                    $dialog.append htmlResult
                    $dialog.dialog
                        show: true
                        resizable: true
                        keyboard: false
                        size:"lg"
                        title: Catalog.getMessage("button.executar.tudo")
        else
            $.MEFloatingError Catalog.getMessage("execution.environment.empty")

    getChecked = () ->
        data = []
        $checked = ''
        size = $("#customgrid > tbody > tr").length

        i = 0
        while i <= size
            element = $("#customgrid > tbody > tr:nth-child(#{i}) > td:nth-child(1) > div").find("div.check-content.icon.active")
            if element.length > 0
                value = $("#customgrid > tbody > tr:nth-child(#{i}) > td:nth-child(2) > span")[0].textContent
                $checked += (value+',')
            i++

        if $checked == ''
            $checked = 'vazio'
        else
            $checked = $checked.substring(0, $checked.length-1)
        data.push
            type: 'testCase'
            ids: $checked
            environmentCommand: getEnvironment()
        JSON.stringify(data[0])

    getAll = () ->
        checkAll = $("#customgrid > thead > tr > th.multi-select > div > div > i")
        if not checkAll.hasClass("checked")
            checkAll.click()

        getChecked()

    getAllBDD = () ->
        data = []
        $allBDD = ''
        inputElements = document.querySelectorAll('h2 > span')
        i = 0
        while i < inputElements.length
            $allBDD += (inputElements[i].textContent + ',')
            i++

        if $allBDD == ''
            $allBDD = 'vazio'
        else
            $allBDD = $allBDD.substring(0, $allBDD.length-1)
        data.push
            type: 'bdd'
            ids: $allBDD
            environmentCommand: getEnvironment()
        JSON.stringify(data[0])

    setTestData = (id, type) ->
        data = []
        data.push
            type: type
            ids: id
            environmentCommand: getEnvironment()
        JSON.stringify(data[0])

    getEnvironment = () ->
        data = []
        env = document.querySelector('div#s2id_environment a span:first-child')
        if env? && env.textContent != ''
           data.push
              name: env.textContent
        data[0]

    onlyChecked: onlyChecked
    performAllCts: performAllCts
    performAllBDD: performAllBDD
    unitTest: unitTest
)()
