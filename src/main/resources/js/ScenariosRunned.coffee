@ScenariosRunnedViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/showScenariosRunned/"+e.data.ids)
                size:"lg"
                title: Catalog.getMessage "Detalhes da Execução"
              )

    next: next
)()
