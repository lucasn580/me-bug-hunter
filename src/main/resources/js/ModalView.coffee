@ModalViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Home/showModalView")
                size:"lg"
                title: Catalog.getMessage "Ops!"
              )

    next: next
)()
