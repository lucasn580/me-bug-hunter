@GridRenderers = (->
    statusRenderer = ($td, value, col, row, opt) ->
        $td.attr('style', 'background-color: '+value)

    renderLink = ($td, value, col, row, opt) ->
        $link = $ "<a/>", href: value, target: "_blank"
        $link.text(value)
        $td.append($link)

    statusRenderer: statusRenderer
    renderLink: renderLink
)()