items = document.getElementsByClassName('maxRows')

getPagination = (table, id) ->
  lastPage = 1
  $('#maxRows' + id).on('change', (evt) ->
    lastPage = 1
    $('#pagination' + id).find('li').slice(1, -1).remove()
    trnum = 0
    maxRows = parseInt($(this).val())
    if maxRows == 5000
      $('#pagination' + id).hide()
    else
      $('#pagination' + id).show()
    totalRows = $(table + ' div.excUserDiv').length
    $(table + ' div').each ->
      trnum++
      if trnum > maxRows
        $(this).hide()
      if trnum <= maxRows
        $(this).show()
      return
    if totalRows > maxRows
      pagenum = Math.ceil(totalRows / maxRows)
      i = 1
      while i <= pagenum
        $('#pagination' + id + ' #prev').before('<li data-page="' + i + '">\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009      <span>' + i++ + '<span class="sr-only">(current)</span></span>\u0009\u0009\u0009\u0009\u0009\u0009\u0009\u0009    </li>').show()
    else
      $('#pagination' + id).hide()
    $('#pagination' + id + ' [data-page="1"]').addClass 'active'
    $('#pagination' + id + ' li').on 'click', (evt) ->
      `var maxRows`
      evt.stopImmediatePropagation()
      evt.preventDefault()
      pageNum = $(this).attr('data-page')
      maxRows = parseInt($('#maxRows' + id).val())
      if pageNum == 'prev'
        if lastPage == 1
          return
        pageNum = --lastPage
      if pageNum == 'next'
        if lastPage == $('#pagination' + id + ' li').length - 2
          return
        pageNum = ++lastPage
      lastPage = pageNum
      trIndex = 0
      $('#pagination' + id + ' li').removeClass 'active'
      $('#pagination' + id + ' [data-page="' + lastPage + '"]').addClass 'active'
      $(table + ' div').each ->
        trIndex++
        if trIndex > maxRows * pageNum or trIndex <= maxRows * pageNum - maxRows
          $(this).hide()
        else
          $(this).show()
        return
      return
    return
  ).val(5).change()
  return

i = 0
while i < items.length
  getPagination '#table-id' + i, i
  $ ->
    id = 0
    $('#table-id' + i + ' div.excUserDiv').each ->
      id++
      $(this).prepend id
      return
    return
  i++

$('#prev > span').text(' > ');
$('#next > span').text(' < ');