$('#customgrid tr:nth-child(1) > th:nth-child(1)').attr('onclick', 'CustomGrid.sortTable(0)');
$('#customgrid tr:nth-child(1) > th:nth-child(2)').attr('onclick', 'CustomGrid.sortTable(1)');
$('#customgrid tr:nth-child(1) > th:nth-child(3)').attr('onclick', 'CustomGrid.sortTable(2)');

@CustomGrid = (->
    monthNames = [
        'Janeiro'
        'Fevereiro'
        'Março'
        'Abril'
        'Maio'
        'Junho'
        'Julho'
        'Agosto'
        'Setembro'
        'Outubro'
        'Novembro'
        'Dezembro'
    ]

    sortTable = (n) ->
        table = undefined
        rows = undefined
        switching = undefined
        i = undefined
        x = undefined
        y = undefined
        shouldSwitch = undefined
        dir = undefined
        switchcount = 0
        table = document.getElementById('customgrid')
        switching = true
        dir = 'asc'

        while switching
            switching = false
            rows = table.rows

            i = 1
            while i < rows.length - 1
                shouldSwitch = false

                x = rows[i].getElementsByTagName('TD')[n]
                y = rows[i + 1].getElementsByTagName('TD')[n]

                if x
                    if rows[0].children[n].innerText == 'Mês'
                        mes1 = x.innerText
                        mes2 = y.innerText
                        if dir == 'asc'
                            if monthNames.indexOf(mes1) < monthNames.indexOf(mes2)
                                shouldSwitch = true
                                break
                        else if dir == 'desc'
                            if monthNames.indexOf(mes1) > monthNames.indexOf(mes2)
                                shouldSwitch = true
                                break

                    else if isNaN x.innerText
                        if dir == 'asc'
                            if x.innerText.toLowerCase() > y.innerText.toLowerCase()
                                shouldSwitch = true
                                break
                        else if dir == 'desc'
                            if x.innerText.toLowerCase() < y.innerText.toLowerCase()
                                shouldSwitch = true
                                break
                    else
                        num1 = parseInt(x.innerText.toLowerCase())
                        num2 = parseInt(y.innerText.toLowerCase())
                        if dir == 'asc'
                            if num1 > num2
                                shouldSwitch = true
                                break
                        else if dir == 'desc'
                            if num1 < num2
                                shouldSwitch = true
                                break
                i++
            if shouldSwitch
                rows[i].parentNode.insertBefore rows[i + 1], rows[i]
                switching = true
                switchcount++
            else
                if switchcount == 0 and dir == 'asc'
                    dir = 'desc'
                    switching = true
        return

    sortTable: sortTable
)()