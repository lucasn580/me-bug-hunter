class @IssueLinkFieldRenderer extends BaseInputFieldRenderer

    renderReadOnly: () ->

        value = @getValue()
        return "" if value == ""

        value = formatter('issues.miisy.com/Misy/do/issue/show/' + value)

        link = $ "<a/>", href: value, target:"_blank",text:@getValue()
        link.click (e) ->
            e.stopImmediatePropagation();

        @node.append link

    formatter = (value, opts) ->
        value = "" unless value?

        if value != "" && value.indexOf("http") == -1
            value = "http://#{value}"

        return value
