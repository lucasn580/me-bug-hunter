@StartBuildViewCoffee = (->

    next = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Automation/run/" + e.data.obj.type + "/" + e.data.obj.environmentCommand.name)
                size:"sm"
                title: Catalog.getMessage "Running"
            )

    next: next
)()
