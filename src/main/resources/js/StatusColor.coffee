@StatusColorCustomRenderer = ($container, data) ->
	$circle = $ "<div />", class: "bugger-icon-rounded", style: "background-color:#{data.color or "white"};"
	$circle.appendTo $container

	$span = $ "<span />", text: data.status, style: "margin-left: 5px;"
	$span.appendTo $container


@StatusHealthColorCustomRenderer = ($container, data) ->
	$circle = $ "<div />", class: "bugger-icon-rounded", style: "background-color:#{data.colorHex or "white"};"
	$circle.appendTo $container


@StatusColorGridRenderer = ($td, value, col, row) ->
	$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{col.casodeTeste?.status?.color or "white"};"
	$icon.appendTo $td
	$td.append " #{value}"


@StatusHealthColorGridRenderer = ($td, value, col, row) ->
	if col.name == 'sectionOne'
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.timeBalancSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.gerencDepSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.entCompartSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.objOnePagSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
	if col.name == 'sectionTwo'
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.focoNoResSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.inovacaoSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.impactoSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.agilSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
	if col.name == 'sectionThree'
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.confiancaSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.segPsicoSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.confAlinhSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.inspExemSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
	if col.name == 'sectionFour'
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.velocidSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.presContSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.compromSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td
			$icon = $ "<div />", class: "bugger-icon-rounded", style: "background-color: #{row.atributoSaudeTime?.qualidadSts?.colorHex or "white"}; border: solid 1px; margin-right:5px;"
			$icon.appendTo $td

#$('div:has(> div > a:contains("Estrutura e Clareza")) p').attr('style', 'margin: 25px; font-weight: bold;');
#$('div:has(> div > a:contains("Significado / Propósito")) p').attr('style', 'margin: 25px; font-weight: bold;');
#$('div:has(> div > a:contains("Relacionamento")) p').attr('style', 'margin: 25px; font-weight: bold;');
#$('div:has(> div > a:contains("Desempenho")) p').attr('style', 'margin: 25px; font-weight: bold;');