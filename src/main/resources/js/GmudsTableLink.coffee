@GmudsLinkCoffee = (->

    linkColumn = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Management/showMonthlyGmudsDetailsByType/"+e.data.month)
                size:"lg"
              )

    monthColumn = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Management/showMonthlyGmudsChart/"+e.data.month)
                size:"full"
              )

    changeYear = (e) ->
        $(".divYear").append "<span style='margin-left: 10px; color: white'>Carregando...</span>"
        location.href = utils.getCustomRootBaseUrl("/do/Management/gmuds/"+e.added.text)

    linkColumn: linkColumn
    monthColumn: monthColumn
    changeYear: changeYear
)()