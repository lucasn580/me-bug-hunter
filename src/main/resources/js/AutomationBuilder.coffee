$(document).ready ->
  #Counter
  counter = 0
  #Make element draggable
  $('.drag').draggable
    helper: ->
      $(this).clone()
    containment: 'frame'
    stop: (ev, ui) ->
      pos = undefined
      objName = undefined
      pos = $(ui.helper).offset()
      objName = '#clonediv' + counter
      $(objName).css
        'left': pos.left
        'top': pos.top
      $(objName).removeClass 'drag'
      #When an existing object is dragged
      $(objName).draggable
        containment: 'parent'
        stop: (ev, ui) ->
          `var pos`
          pos = $(ui.helper).offset()
          #console.log $(this).attr('id')
          #console.log pos.left
          #console.log pos.top
          return
      return
  #Make element droppable
  $('#frame').droppable drop: (ev, ui) ->
    if ui.helper.attr('id').search(/drag[0-9]/) != -1
      counter++
      element = $(ui.draggable).clone()
      element.addClass 'tempclass'
      $(this).append element
      $('.tempclass').attr 'id', 'clonediv' + counter
      $('#clonediv' + counter).removeClass 'tempclass'
      #Get the dynamically item id
      draggedNumber = ui.helper.attr('id').search(/drag([0-9])/)
      itemDragged = 'dragged' + RegExp.$1
      #console.log itemDragged
      $('#clonediv' + counter).addClass itemDragged
    return
  return