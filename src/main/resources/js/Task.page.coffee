@TaskPage = (->

    saveTask = ->
        $form = $ "#formTask"

        $form.form "postJSON",
            url: $form.attr "action"
            success: (e) ->
                meceap.defaultMessageCallBack e
                window.location = e.data.redirectTo if e.data?.redirectTo?

    saveTask: saveTask
)()