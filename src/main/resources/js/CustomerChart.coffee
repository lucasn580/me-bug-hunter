@CustomerChartWidgetRenderer = (->

    renderGraph = () ->
        $chartData = []
        getData($chartData)

    getData = (data) ->
        utils.get(url:utils.getCustomRootBaseUrl('/do/Home/getCustomersBody'), (result) =>
            for client in result
                data.push({
                  "name": client.cliente
                  "y": client.qtdCenarios
                })
            render(data))

    render = (renderData) ->
        new (Highcharts.Chart)({
              chart: {
                  plotBackgroundColor: true,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
                  renderTo: 'piechart'
              },
              title: {
                  text: 'Quantidade de cenários por cliente'
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.y}</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                          style: {
                              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                          }
                      }
                  }
              },
              series: [{
                  name: 'CTs',
                  colorByPoint: true,
                  data: renderData
              }]
        });

    renderGraph: renderGraph
)()