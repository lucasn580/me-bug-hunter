@KanbanTaskCoffee = (->

    create = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Management/kanban/task/new")
                size:"lg"
              )
    edit = (e) ->
        $("<div />").dialog(
                show: true
                resizable: true
                keyboard: false
                urlAction: utils.getCustomRootBaseUrl("/do/Management/kanban/task/edit/" + e.data.id)
                size:"lg"
              )

    remove = (e) ->
        $.MEConfirm Catalog.getMessage("label.confirmWarning"), Catalog.getMessage("label.confirmTitle"), ->
            utils.postJSON
                url: utils.getCustomRootBaseUrl("/do/Management/kanban/task/remove/" + e.data.id)
                data: e.data
                (result) ->
                    meceap.defaultMessageCallBack result
                    window.location = result.data.redirectTo if result.data?.redirectTo?

    create: create
    edit: edit
    remove: remove
)()
