@GmudBarChartWidgetRenderer = (->

    renderGraph = () ->
        $chartData = []
        getData($chartData)

    getData = (data) ->
        utils.get(url:utils.getCustomRootBaseUrl('/do/Management/getGmuds'), (result) =>
            names = []
            newArray = new Map()

            result.forEach (array) ->
                classif = objToStrMap(array.classifications)
                classif.forEach (value, key) ->
                    if newArray.has(key)
                        newArray.set(key, value + newArray.get(key))
                    else
                        newArray.set(key, value)
                return

            newArray.forEach (value, key) ->
                data.push({
                    "name": key
                    "y": value
                })
                names.push(key)
                return

            render(data, names))

    objToStrMap = (obj) ->
        strMap = new Map
        for k in Object.keys(obj)
            strMap.set k, obj[k]
        strMap

    render = (data, names) ->
        new Highcharts.Chart({
            chart: {
                type: 'bar'
                renderTo: 'gmudbarchart'
            },
            title: {
                text: 'GMUDs por classificação'
            },
            legend: {
                enabled: true
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            plotOptions:
                bar:
                    dataLabels:
                        enabled: true
            xAxis:
                categories: names
            yAxis:
                title:
                    text: 'Quantidade'
                dataLabels:
                    enabled: false
            series: [{
                name: 'Quantidade'
                data: data
            }]
        });

    renderGraph: renderGraph
)()