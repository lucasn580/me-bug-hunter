$('.navDiv li, #divBtns button, #blip-container').each (botao) ->
  $(this).on 'mousemove', ->
    $('#lblMenuOpc').text $(this).data('text')
    $('#lblMenuOpc').attr('style', 'overflow: hidden;
                                  letter-spacing: 2px;
                                  font-family: "Avengers";
                                  animation: typing 1s steps(30, end), blink .60s step-end infinite;
                                  white-space: nowrap;
                                  border-right: 4px solid orange;')
    $('#imgLego').attr('src', $(this).data('img'))
    return
  $(this).on 'mouseout', ->
    $('#lblMenuOpc').text ''
    $('#lblMenuOpc').attr('style', '')
    $('#imgLego').attr('src', '')
    return
  return

$('#blip-container').click ->
  if document.getElementById("talk-bubble").style.display == 'inline-block'
    document.getElementById("talk-bubble").style.display = 'none'
  else
    document.getElementById("talk-bubble").style.display = 'inline-block'
    $('#talk-bubble p, br').remove();
    str = 'Olá, eu sou o Homem de Ferro, criador deste aplicativo.'
    str1 = 'Aqui você encontra diversas funcionalidades para gerenciar ou acompanhar a área de QA, em nível de gerência ou usuário.'
    str2 = 'Boa exploração!! =)'
    spans = '<p>' + str.split('').join('</p><p>') + '</p></br></br>' +
            '<p>' + str1.split('').join('</p><p>') + '</p></br></br>' +
            '<p>' + str2.split('').join('</p><p>') + '</p>'
    $(spans).hide().appendTo('.talktext').each (i) ->
      $(this).delay(30 * i).css(
        display: 'inline'
        opacity: 0).animate { opacity: 1 }, 100
      return
  return

$('#average').on('change', ->
  if @value != 'Média do mês'
    $('#lblAverage').text @value + ' %'
  else
    $('#lblAverage').text 'Selecione'
  return
).trigger 'change'


