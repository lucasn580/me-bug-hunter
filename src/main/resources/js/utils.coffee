@utils = (->
    getCustomRootControllerUrl = (url) ->
        getCustomRootBaseUrl("/do#{url}")

    getCustomRootBaseUrl = (url) ->
        return meceap.getCustomRootUrl url if url?
        meceap.getCustomRootBaseUrl()

    getCustomRootApiUrl = ->
        meceap.getCustomRootApiUrl()

    getCustomRootDocumentSearchUrl = (filters) ->
        getCustomRootBaseUrl("/search/filter?_&#{decodeURIComponent($.param(filters))}")

    post = (options, success) ->
        $.post options.url, (result) ->
            if success?
                success(result)
            else
                messageCallback(result, options)

    get = (options, success) ->
        $.get options.url, (result) ->
            if success?
                success(result)
            else
                messageCallback(result, options)

    canEditField = (field) ->
        return field.screen.readOnlyMode == "toggle"

    postJSON = (options, success) ->
        $.ajax
            dataType: options.dataType or 'json'
            contentType: 'application/json; charset=utf-8'
            type: "POST"
            url: options.url
            data: JSON.stringify options.data
            async: options.async
            success: (result) ->
                if success?
                    success(result)
                else
                    messageCallback(result, options)

    postData = (url, object, method = 'post') ->
        postData = $.MESerializeArray(object)
        postData = decodeURI(postData)
        postData = decodeURIComponent(postData).replace(/\+/g, " ")

        $form = $ "<form method='#{method}' action='#{url}'/>"
        for item in postData.split('&')
            values = item.split('=')
            $form.append $("<input type='hidden' name='#{values[0]}' value='#{values[1]}'/>")

        $form.appendTo $("body")
        $form.submit()

    createDate = (date) ->
        new Date(date)

    formatDate = (date) ->
        createDate(date).format(Catalog.getLocaleInformation().dateTimeFormat)

    formatShortDate = (date) ->
        createDate(date).format(Catalog.getLocaleInformation().dateFormat)

    formatNumber = (value, precision) ->
        precision = Catalog.getLocaleInformation().decimalPlaces unless precision?

        number = new NumberFormat()
        number.setInputDecimal '.'
        number.setNumber value
        number.setPlaces precision, false
        number.setCurrency false
        number.setNegativeFormat number.LEFT_DASH
        number.setSeparators true, Catalog.getLocaleInformation().groupingSeparator, Catalog.getLocaleInformation().decimalSeparator
        number.toFormatted()

    confirm = (options) ->
        $dialog = $ "<div/>"
        $dialog.append "<p>#{options.msg}</p>"

        $dialog.popup
            title: options.title
            size: options.size or 'sm'
            buttons: [
                text: Catalog.getMessage("label.yes")
                primary: true
                click: =>
                    result = options.submit?()
                    $dialog.popup "close" if result isnt false
            ,
                text: Catalog.getMessage("label.no")
                click: =>
                    options.decline?()
                    $dialog.popup "close"
            ]

        $dialog.on "hidden.meceap.dialog", () =>
            options.onClose?()

        $dialog

    openDialog = (url, title, options) ->
        defaults =
            size: 'sm'

        options = $.extend defaults,options

        $dialog = $ "<div/>", id: "dialog"
        $dialog.popup
            title: title,
            size: options.size,
            urlAction: url

    confirmNote = (options) ->
        $form = $ "<form />"

        $group = $ "<div />", class: "form-group"
        $group.appendTo $form

        if options.warn?
            $warnWrapper = $ "<div/>", class: "alert alert-warning"

            $label = $ "<b/>", text: Catalog.getMessage("label.warning") + ": "
            $warn = $ "<span/>", text: options.warn

            $label.appendTo $warnWrapper
            $warn.appendTo $warnWrapper
            $warnWrapper.appendTo $group

        if not options.hideJustificationNote is true
            $label = $ "<label/>", class: "control-label", text: Catalog.getMessage("label.justificationNotes")
            $label.appendTo $group

        outSubmit = ->
            return false unless $form.form("isValid")
            options.submit?($note.val())

        $note = $ "<textarea/>", required: "required", class: "form-control"
        $note.keypress (e) ->
            if e.keyCode is 10 and $form.form("isValid")
                outSubmit()
                $dialog.popup "close"

        $note.appendTo $group

        $dialog = confirm
            title: options.title
            msg: options.msg
            size: 'md'
            submit: outSubmit
            decline: options.decline
            onClose: options.onClose
            buttons: options.buttons
            onOpen: ->
                $note.focus()
                options.onOpen?()

        $form.appendTo $dialog
        $form.form()

        result =
            form: $form
            dialog: $dialog
            note: $note

        return result

    messageCallback = (data, options) ->
        meceap.defaultMessageCallBack data
        if data.errorMessages?.length > 0
            options?.error?(data?.data || data)
        else
            options?.success?(data?.data || data)
            window.location = data.data.redirectTo if data?.data?.redirectTo

    getAcronym = (value) ->
        value?= ""
        parts = value.trim().split(" ")
        acronym = parts[0].charAt(0) + parts[parts.length - 1].charAt(0)
        acronym.toUpperCase()

    serializeObject = ($form) ->
        arrayData = $form.serializeArray()
        objectData = {}

        $.each arrayData, ->
            if @value?
                value = @value
            else
                value = ''

            if objectData[@name]?
                unless objectData[@name].push
                    objectData[@name] = [objectData[@name]]

                objectData[@name].push value
            else
                objectData[@name] = value

        return objectData

    getAllInputsFromDiv = (divId) ->
        $("#" + divId + " :input").not(".select2-focusser")

    getMaskedValue = (value, pattern, callback) ->
        if pattern
            $input = $ "<input />", type: "hidden", value: value
            $input.on "ready.meceap.mask", =>
                callback?($input.inputMask("getMaskedValue"))
            $input.inputMask
                pattern: pattern
        else
            callback?(value)

    renderStarRating = (value, $container) ->
        $starM = $ "<i/>", class: "fa fa-star-half-o"
        $star1 = $ "<i/>", class: "fa fa-star-o"
        $star2 = $ "<i/>", class: "fa fa-star-o"
        $star3 = $ "<i/>", class: "fa fa-star-o"
        $star4 = $ "<i/>", class: "fa fa-star-o"
        $star5 = $ "<i/>", class: "fa fa-star-o"
        $span = $ "<span/>"

        if value
            if value == 0
                $span.append $star1, $star2, $star3, $star4, $star5
            else if value <= 0.5
                $starM.addClass "active"
                $span.append $starM, $star2, $star3, $star4, $star5
            else if value <= 1
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $span.append $star1, $star2, $star3, $star4, $star5
            else if value <= 1.5
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $starM.addClass "active"
                $span.append $star1, $starM, $star3, $star4, $star5
            else if value <= 2
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $span.append $star1, $star2, $star3, $star4, $star5
            else if value <= 2.5
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $starM.addClass "active"
                $span.append $star1, $star2, $starM, $star4, $star5
            else if value <= 3
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $star3.removeClass("fa-star-o").addClass("active fa-star")
                $span.append $star1, $star2, $star3, $star4, $star5
            else if value <= 3.5
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $star3.removeClass("fa-star-o").addClass("active fa-star")
                $starM.addClass "active"
                $span.append $star1, $star2, $star3, $starM, $star5
            else if value <= 4
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $star3.removeClass("fa-star-o").addClass("active fa-star")
                $star4.removeClass("fa-star-o").addClass("active fa-star")
                $span.append $star1, $star2, $star3, $star4, $star5
            else if value <= 4.5
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $star3.removeClass("fa-star-o").addClass("active fa-star")
                $star4.removeClass("fa-star-o").addClass("active fa-star")
                $starM.addClass "active"
                $span.append $star1, $star2, $star3, $star4, $starM
            else if value <= 5
                $star1.removeClass("fa-star-o").addClass("active fa-star")
                $star2.removeClass("fa-star-o").addClass("active fa-star")
                $star3.removeClass("fa-star-o").addClass("active fa-star")
                $star4.removeClass("fa-star-o").addClass("active fa-star")
                $star5.removeClass("fa-star-o").addClass("active fa-star")
                $span.append $star1, $star2, $star3, $star4, $star5
            else
                $span.append $star1, $star2, $star3, $star4, $star5

            $container.append $span
        else
            $span.append $star1, $star2, $star3, $star4, $star5
            $container.append $span

    fnRenderStar = (td, value, col, row, opt) ->
        $star = $("<input>", { type: "checkbox" })
        $star.appendTo(td)
        $star.favorite({ document: row.internalId })

    getCustomRootBaseUrl: getCustomRootBaseUrl
    getCustomRootControllerUrl: getCustomRootControllerUrl
    getCustomRootApiUrl: getCustomRootApiUrl
    getCustomRootDocumentSearchUrl: getCustomRootDocumentSearchUrl
    messageCallback: messageCallback
    openDialog: openDialog
    get: get
    post: post
    postJSON: postJSON
    postData: postData
    formatShortDate:formatShortDate
    formatDate:formatDate
    formatNumber:formatNumber
    confirm: confirm
    confirmNote: confirmNote
    createDate: createDate
    canEditField: canEditField
    getAcronym: getAcronym
    serializeObject: serializeObject
    getAllInputsFromDiv: getAllInputsFromDiv
    getMaskedValue: getMaskedValue
    renderStarRating: renderStarRating
    fnRenderStar: fnRenderStar
)()