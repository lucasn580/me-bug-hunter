package meceap.appbuilder.event.release;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.ceap.dynamic.extension.document.event.DocumentAfterCreate;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.model.exception.AccessViolationException;
import meceap.appbuilder.service.ReleaseCalendarService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: luiza
 * Date: 02/10/12
 * Time: 12:00
 */
@Component
public class AfterCreateRelease implements DocumentAfterCreate<Release> {
// ------------------------------ FIELDS ------------------------------

    @Inject
    private ReleaseCalendarService calendarService;

// ------------------------ INTERFACE METHODS ------------------------


// --------------------- Interface DocumentAfterCreate ---------------------

    @Override
    public void afterCreate(Release release, UserHumanBeing userHumanBeing) throws AccessViolationException {

        if (release.getRelease().getDataAbertura() != null && release.getRelease().getDataProd() != null)
            calendarService.appendToCalendar(release, release.getRelease().getDataAbertura(), release.getRelease().getDataProd(), release.getRelease().getRelease(),
                    userHumanBeing, ReleaseCalendarService.CALENDAR, ReleaseCalendarService.COLOR);
    }
}