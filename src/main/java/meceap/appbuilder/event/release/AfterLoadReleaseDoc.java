package meceap.appbuilder.event.release;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.ceap.dynamic.extension.document.event.DocumentAfterLoad;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.model.exception.AccessViolationException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author lucasns
 * @since #1.6
 */
@Component
public class AfterLoadReleaseDoc implements DocumentAfterLoad<Release> {
// ------------------------------ FIELDS ------------------------------

// --------------------- Interface DocumentAfterLoad ---------------------

    @Override
    public void afterLoad(Release metaDocumentData, UserHumanBeing userHumanBeing) throws AccessViolationException {
        if(metaDocumentData.getRelease().getQtdChamados() != null && metaDocumentData.getRelease().getQtdDevolucao() != null) {
            BigDecimal total = new BigDecimal(metaDocumentData.getRelease().getQtdChamados());
            BigDecimal returns = new BigDecimal(metaDocumentData.getRelease().getQtdDevolucao());
            BigDecimal result = returns
                    .multiply(new BigDecimal(100))
                    .divide(total, 0, RoundingMode.UP);
            metaDocumentData.getRelease().setIndicereteste(result.toString() + " %");
        }
    }
}