package meceap.appbuilder.event.release;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.ceap.dynamic.extension.document.event.DocumentAfterUpdate;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.model.exception.AccessViolationException;
import meceap.appbuilder.service.ReleaseCalendarService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: luiza
 * Date: 22/10/12
 * Time: 12:44
 */

@Component
public class AfterUpdateRelease implements DocumentAfterUpdate<Release> {
// ------------------------------ FIELDS ------------------------------

    @Inject
    private ReleaseCalendarService calendarService;

// ------------------------ INTERFACE METHODS ------------------------


// --------------------- Interface DocumentAfterUpdate ---------------------

    @Override
    public void afterUpdate(Release oldRelease, Release savedRelease, UserHumanBeing userHumanBeing) throws AccessViolationException {
        this.appendToCalendar(oldRelease.getRelease().getDataProd(), savedRelease, userHumanBeing);
    }

// -------------------------- OTHER METHODS --------------------------

    private void appendToCalendar(DateTime old, Release saved, UserHumanBeing userHumanBeing) {
        DateTime expectedDate = saved.getRelease().getDataProd();
        if (old == null && expectedDate == null || (old != null && old.equals(expectedDate))) {
            return;
        }

        calendarService.appendToCalendar(saved, saved.getRelease().getDataAbertura(), saved.getRelease().getDataProd(), saved.getRelease().getRelease(),
                userHumanBeing, ReleaseCalendarService.CALENDAR, ReleaseCalendarService.COLOR);
    }
}