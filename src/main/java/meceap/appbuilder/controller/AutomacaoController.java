package meceap.appbuilder.controller;

import br.com.me.appbuilder.Ambiente;
import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.appbuilder.document.casodeTeste.CasodeTeste;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserHumanBeingPrincipal;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.util.annotations.ResponseGrid;
import br.com.me.ceap.web.command.MessageCommand;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.model.*;
import meceap.appbuilder.service.*;
import meceap.appbuilder.view.automation.*;
import meceap.appbuilder.view.bdd.AutomacaoBDDCardView;
import meceap.appbuilder.view.bdd.AutomacaoBDDRunTestView;
import meceap.appbuilder.view.bdd.AutomacaoBDDView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Controller
@RequestMapping("/{customRootName}/do/Automation")
public class AutomacaoController {
// ------------------------------ FIELDS ------------------------------

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private ExecutionService executionService;
    private TestCasesService testCasesService;
    private EnvironmentService environmentService;
    private BDDService bddService;
    private UserService userService;
    private String cts;

    // --------------------- GETTER / SETTER METHODS ---------------------
    @Inject
    public void setExecutionService(ExecutionService executionService) {
        this.executionService = executionService;
    }

    @Inject
    public void setTestCasesService(TestCasesService testCasesService) {
        this.testCasesService = testCasesService;
    }

    @Inject
    public void setEnvironmentService(EnvironmentService environmentService) {
        this.environmentService = environmentService;
    }

    @Inject
    public void setBddService(BDDService bddService) {
        this.bddService = bddService;
    }

    @Inject
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private String getCts() {
        return cts;
    }

    private void setCts(String cts) {
        this.cts = cts;
    }

    // -------------------------- OTHER METHODS --------------------------
    @GetMapping
    public ModelAndView index(UserPrincipal principal) {
        log.info("Automation controller!!");

        ModelAndView mav = new ModelAndView();
        AutomacaoView view = new AutomacaoView(principal);
        mav.setView(view);
        return mav;
    }

    @GetMapping(value = "/builds")
    public ModelAndView showBuildsPage(UserHumanBeingPrincipal userHumanPrincipal) {
        log.info("Exibindo a tela com as execuções do usuário...");

        boolean isAdmin = userService.userIsAdmin(userHumanPrincipal.getHumanBeing());

        if(isAdmin)
            return new ModelAndView(new BuildRunsView(userHumanPrincipal, true, executionService.getExecutores()));
        else
            return new ModelAndView(new BuildRunsView(userHumanPrincipal, executionService.getUserBuilds(userHumanPrincipal.getHumanBeing().getEmail())));
    }

    @GetMapping(value = "/bddFlows")
    public ModelAndView showBDDFlows(UserPrincipal principal) {
        log.info("Exibindo a tela de automação BDD...");

        return new ModelAndView(new AutomacaoBDDView(bddService.getCustomersBDD(), principal));
    }

    @GetMapping(value = "/showExecutions/{type}")
    public ModelAndView showBuilds(UserPrincipal principal, @PathVariable("type") String type) {
        log.info("Exibindo modal com as configurações gerais...");

        return new ModelAndView(new ExecutionsModalView(principal, type));
    }

    @RolesAllowed(AccessPermission.PERM_BUILD_AUTOMATION_FLOW)
    @GetMapping(value = "/customBuilder")
    public ModelAndView customBuilder(UserPrincipal principal) {
        log.info("Exibindo tela de construção personalizada de CasodeTeste...");

        return new ModelAndView(new CustomBuilderView(principal));
    }

    @GetMapping(value = "/customBuilder/editAction")
    public ModelAndView editAction(UserPrincipal principal) {
        log.info("Exibindo tela de edição da ação selecionada...");

        return new ModelAndView(new CustomBuilderActionView(principal));
    }

    @RolesAllowed(AccessPermission.PERM_RUN_AUTOMATION_FLOW)
    @GetMapping(value = "/run/{type}/{environment}")
    public ModelAndView run(UserHumanBeingPrincipal principal, @PathVariable("type") String type, @PathVariable("environment") String environment) {
        log.info("Retornando a lista de clientes...");

        ExecutionCommand command = new ExecutionCommand();
        command.setType(type);
        command.setIds(getCts());
        command.setUser(principal.getHumanBeing().getEmail());
        EnvironmentCommand environmentCommand = new EnvironmentCommand();
        environmentCommand.setName(environment);
        command.setEnvironmentCommand(environmentCommand);
        executionService.run(command);

        return new ModelAndView(new RunView(principal));
    }

    @RolesAllowed(AccessPermission.PERM_RUN_AUTOMATION_FLOW)
    @PostMapping(value = "/runUnitTest")
    @ResponseBody
    public ModelAndView runUnitTest(UserPrincipal principal, @RequestBody String testCaseId) {
        log.info("Exibindo modal de execução do fluxo único...");

        CasodeTeste testCase = testCasesService.getById(testCaseId);
        setCts(testCaseId);
        ExecutionCommand execution = new ExecutionCommand();
        execution.setIds(testCaseId);
        execution.setType(ExecutionType.TEST_CASE.getType());
        execution.setEnvironmentCommand(new EnvironmentCommand("none"));
        return new ModelAndView(new AutomacaoKanbanRunTestView(principal, execution, testCase));
    }

    @RolesAllowed(AccessPermission.PERM_RUN_AUTOMATION_FLOW)
    @PostMapping(value = "/runSelectedTests")
    @ResponseBody
    public ModelAndView runSelectedTests(UserPrincipal principal, @RequestBody ExecutionCommand execution) {
        log.info("Exibindo modal de execução dos fluxos de Caso de Teste selecionados...");

        setCts(execution.getIds());
        return new ModelAndView(new AutomacaoKanbanRunTestView(principal, execution));
    }

    @RolesAllowed(AccessPermission.PERM_RUN_AUTOMATION_FLOW)
    @PostMapping(value = "/runBDD")
    @ResponseBody
    public ModelAndView runBdd(UserPrincipal principal, @RequestBody ExecutionCommand execution) {
        log.info("Exibindo modal de execução dos fluxos BDD selecionados...");

        setCts(execution.getIds());
        return new ModelAndView(new AutomacaoBDDRunTestView(principal, execution));
    }

    @GetMapping(value = "/showScenariosRunned/{idsCts}")
    public ModelAndView showScenariosRunned(UserPrincipal principal, @PathVariable("idsCts") String idsCts) {
        log.info("Exibindo modal com as informações do fluxo selecionado...");

        List<ClienteModel> clientes = testCasesService.getScenariosRunned(idsCts);

        return new ModelAndView(new ScenariosRunnedView(principal, clientes));
    }

    @GetMapping(value = "/informationsFlow/{casodeTesteId}")
    public ModelAndView showInformationsFlow(UserPrincipal principal, CasodeTeste casodeTeste) {
        log.info("Exibindo modal com as informações do fluxo selecionado...");

        return new ModelAndView(new AutomacaoKanbanCardView(principal, casodeTeste));
    }

    @GetMapping(value = "/informationsBDDFlow/{bddId}")
    public ModelAndView showInformationsBDDFlow(UserPrincipal principal, BDD bdd) {
        log.info("Exibindo modal com as informações do fluxo selecionado...");

        return new ModelAndView(new AutomacaoBDDCardView(principal, bdd));
    }

    @GetMapping(value = "/getFailures/{failures}")
    public ModelAndView getFailures(UserPrincipal principal, @PathVariable("failures") String failures) {
        log.info("Exibindo modal com as falhas da execução...");

        return new ModelAndView(new BuildFailuresView(principal, failures));
    }

    @GetMapping(value = "/showEnvironmentConfig")
    public ModelAndView showEnvironmentConfig(UserPrincipal principal, HttpServletRequest request) {
        log.info("Exibindo modal de configuração de ambientes...");

        EnvironmentCommand command = EnvironmentCommand.mapFrom(environmentService.getAll());
        return new ModelAndView(new EnvironmentView(principal, command, request));
    }

    @ResponseBody
    @PostMapping(value = "/registerEnvironment")
    public MessageCommand registerEnvironment(@RequestBody @Valid EnvironmentCommand environments, UserPrincipal principal) {
        MessageCommand messageCommand = new MessageCommand();
        try {
            environmentService.registerEnvironments(environments.getEnvironments());
        } catch (AccessDeniedException ex) {
            messageCommand.addError(ex.getMessage());
            return messageCommand;
        }
        messageCommand.addData("redirectTo", AutomacaoControllerURL.index(principal.getCustomRoot()));

        return messageCommand;
    }

    @ResponseGrid
    @GetMapping(value = "/getBuilds/{type}")
    public List<ExecutionCommand> getBuilds(@PathVariable("type") String type) {
        log.info("Retornando a lista de builds...");
        return executionService.getBuilds(type);
    }

    @ResponseBody
    @GetMapping(value = "/getUserBuildsBody")
    public List<ExecutorModel> getUserBuildsBody() {
        log.info("Retornando a lista de builds do usuário para o Chart...");
        return executionService.getExecutores();
    }

    @ResponseBody
    @GetMapping(value = "/getActivesEnvironments")
    public List<Ambiente> getActivesEnvironments() {
        log.info("Retornando a lista de ambientes ativos...");
        return environmentService.getActivesEnvironments();
    }

    @ResponseGrid
    @GetMapping(value = "/getActivesTestCases")
    public List<CasodeTeste> getActivesTestCases() {
        log.info("Retornando a lista de casos de teste ativos...");
        return testCasesService.getActivesTestCases();
    }

}