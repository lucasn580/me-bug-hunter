package meceap.appbuilder.controller;

import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserHumanBeingPrincipal;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.util.annotations.ResponseGrid;
import meceap.appbuilder.model.AccessPermission;
import meceap.appbuilder.model.ClienteModel;
import meceap.appbuilder.model.TestCaseModel;
import meceap.appbuilder.service.TestCasesService;
import meceap.appbuilder.service.UserService;
import meceap.appbuilder.view.automation.AutomacaoView;
import meceap.appbuilder.view.bdd.BDDPageView;
import meceap.appbuilder.view.home.CustomersView;
import meceap.appbuilder.view.home.HomeView;
import meceap.appbuilder.view.home.ModalView;
import meceap.appbuilder.view.home.SuitesView;
import meceap.appbuilder.view.management.ManagementView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Controller
@RequestMapping("/{customRootName}/do/Home")
public class HomeController {
// ------------------------------ FIELDS ------------------------------

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private UserService userService;
    private TestCasesService testCasesService;

    // --------------------- GETTER / SETTER METHODS ---------------------
    @Inject
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Inject
    public void setTestCasesService(TestCasesService testCasesService) {
        this.testCasesService = testCasesService;
    }

    // -------------------------- OTHER METHODS --------------------------
    @GetMapping
    public ModelAndView index(@PathVariable String customRootName, UserHumanBeingPrincipal userHumanBeingPrincipal) {
        log.info("Hello world!!");

        // ModelAndView is default Spring MVC return type.
        ModelAndView mav = new ModelAndView();

        // Create the view, demonstrating passing parameter in the constructor (typed 'firstValue' in the view)
        HomeView view = new HomeView(userHumanBeingPrincipal);

        // Set the view in the ModelAnd View
        mav.setView(view);

        // Another parameter to pass the customRootName to be able to link correctly from the view
        mav.addObject("customRootName", customRootName);

        return mav;
    }

    @GetMapping(value = "/bdd")
    public ModelAndView showBDDPage(UserPrincipal principal) {
        log.info("Exibindo a tela de ações BDD...");

        return new ModelAndView(new BDDPageView(principal));
    }

    @GetMapping(value = "/suites")
    public ModelAndView showSuites(UserPrincipal principal) {
        log.info("Exibindo a tela com as informações de suítes...");

        return new ModelAndView(new SuitesView(principal));
    }

    @GetMapping(value = "/customers")
    public ModelAndView showErrorsByCustomer(UserPrincipal principal) {
        log.info("Exibindo a tela com erros agrupados por cliente...");

        return new ModelAndView(new CustomersView(principal));
    }

    @GetMapping(value = "/automation")
    public ModelAndView showAutomationPage(UserPrincipal principal) {
        log.info("Exibindo a tela com erros agrupados por cliente...");

        return new ModelAndView(new AutomacaoView(principal));
    }

    @RolesAllowed(AccessPermission.PERM_ALLOW_SEE_MANAGEMENT)
    @GetMapping(value = "/manager")
    public ModelAndView showManagerPage(UserPrincipal principal) {
        log.info("Exibindo a tela com informações gerenciais...");

        return new ModelAndView(new ManagementView(principal));
    }

    @GetMapping(value = "/showModalView")
    public ModelAndView showModalView(UserPrincipal principal) {
        log.info("Exibindo um modal simples...");

        return new ModelAndView(new ModalView(principal));
    }

    @ResponseGrid
    @GetMapping(value = "/getSuitesInfo")
    public List<TestCaseModel> getSuitesInfo() {
        log.info("Retornando a lista de clientes para o Grid...");
        return testCasesService.getTcsDetails();
    }

    @ResponseGrid
    @GetMapping(value = "/getCustomersInfo")
    public List<ClienteModel> getCustomersInfo() {
        log.info("Retornando a lista de clientes para o Grid...");
        return testCasesService.getCustomersTestCases();
    }

    @ResponseBody
    @GetMapping(value = "/getCustomersBody")
    public List<ClienteModel> getCustomersBody() {
        log.info("Retornando a lista de clientes para o Chart...");
        return testCasesService.getCustomersTestCases();
    }

    @ResponseBody
    @GetMapping(value = "/getUsersAdmin")
    public List<UserHumanBeing> getUsersAdmin() {
        log.info("Retornando a lista de usuários Admin...");
        return userService.getUsersAdmin();
    }
}