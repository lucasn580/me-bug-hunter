package meceap.appbuilder.controller;

import br.com.me.appbuilder.Tarefa;
import br.com.me.appbuilder.document.buildspontuais.Buildspontuais;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserHumanBeingPrincipal;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.command.MessageCommand;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import meceap.appbuilder.integration.MetricsService;
import meceap.appbuilder.model.AccessPermission;
import meceap.appbuilder.model.ExecutionType;
import meceap.appbuilder.model.GmudMensalModel;
import meceap.appbuilder.model.TaskCommand;
import meceap.appbuilder.model.external.CountersCommand;
import meceap.appbuilder.model.external.DashboardCommand;
import meceap.appbuilder.service.*;
import meceap.appbuilder.view.management.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
@Controller
@RequestMapping("/{customRootName}/do/Management")
public class ManagementController {
// ------------------------------ FIELDS ------------------------------

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private GmudService gmudService;
    private MetricsService metricsService;
    private ReleaseService releaseService;
    private ExecutionService executionService;
    private BuildsPontuaisService buildsPontuaisService;
    private KanbanService kanbanService;
    private List<GmudMensalModel> gmuds = new ArrayList<>();

    // --------------------- GETTER / SETTER METHODS ---------------------

    @Inject
    public void setGmudService(GmudService gmudService) {
        this.gmudService = gmudService;
    }

    @Inject
    public void setMetricsService(MetricsService metricsService) {
        this.metricsService = metricsService;
    }

    @Inject
    public void setReleaseService(ReleaseService releaseService) {
        this.releaseService = releaseService;
    }

    @Inject
    public void setExecutionService(ExecutionService executionService) {
        this.executionService = executionService;
    }

    @Inject
    public void setBuildsPontuaisService(BuildsPontuaisService buildsPontuaisService) {
        this.buildsPontuaisService = buildsPontuaisService;
    }

    @Inject
    public void setKanbanService(KanbanService kanbanService) {
        this.kanbanService = kanbanService;
    }

    // -------------------------- OTHER METHODS --------------------------
    @GetMapping
    public ModelAndView index(@PathVariable String customRootName, UserPrincipal principal) {
        log.info("Management controller!!");

        ModelAndView mav = new ModelAndView();
        ManagementView view = new ManagementView(principal);
        mav.setView(view);
        return mav;
    }

    @GetMapping(value = "/metrics")
    public ModelAndView showMetrics(UserPrincipal principal) {
        log.info("Exibindo a tela de Métricas...");

        return new ModelAndView(new MetricsView(principal, metricsService.getMetrics()));
    }

    @GetMapping(value = "/metrics/register/{month}")
    public ModelAndView registerMetrics(UserPrincipal principal, @PathVariable("month") String month) {
        log.info("Exibindo a tela registro de métricas...");
        CountersCommand counters = new CountersCommand();
        counters.setNonCompliantProd(metricsService.getAmountMetricFilterOne());
        counters.setNonCompliantDetectedTest(metricsService.getAmountMetricFilterTwo());
        counters.setAutomRegressionTestsSuccess(0);
        counters.setRetestIndex(0);
        counters.setAmountPointBuild(0);
        counters.setRelationshipMetric(metricsService.getAmountRelationshipMetric());

        return new ModelAndView(new RegisterMetricView(principal, counters, month));
    }

    @PostMapping(value = "/metrics/register/new")
    public ModelAndView newMetric(UserPrincipal principal, @RequestBody String[] values) {
        log.info("Criando registros de novas métricas...");

        metricsService.postMetrics(values);
        return new ModelAndView(new NewMetricModalView(principal));
    }

    @GetMapping(value = "/gmuds/{year}")
    public ModelAndView showGmuds(UserPrincipal principal, @PathVariable("year") String year) {
        log.info("Exibindo a tela de Gmuds...");

        this.gmuds = gmudService.getGmudsByYear(year);
        return new ModelAndView(new GmudsView(principal, gmuds));
    }

    @GetMapping(value = "/showMonthlyGmudsChart/{month}")
    public ModelAndView showMonthlyGmudsCharts(UserPrincipal principal, @PathVariable("month") String month) {
        log.info("Exibindo a tela de charts das Gmuds mensais...");

        return new ModelAndView(new MonthlyGmudsChartView(principal, getMonthlyGmudsModalChart(principal, month)));
    }

    @GetMapping(value = "/showMonthlyGmudsDetailsByType/{month}")
    public ModelAndView showMonthlyGmudsDetailsByType(UserPrincipal principal, @PathVariable("month") String month) {
        log.info("Recuperando a lista de gmuds para exibição detalhada no modal...");

        return new ModelAndView(new GmudsLinkModalView(principal, getMonthlyGmudsModalChart(principal, month), month));
    }

    @GetMapping(value = "/releases")
    public ModelAndView showReleases(UserPrincipal principal) {
        log.info("Exibindo a tela de Releases...");

        return new ModelAndView(new ReleasesView(principal, releaseService.getAll(),
                executionService.getMonthlyBuildsResults(ExecutionType.TEST_CASE.getType()), executionService.getMonthlyBuildsResults(ExecutionType.BDD.getType())));
    }

    @GetMapping(value = "/buildsPontuais")
    public ModelAndView showBuildsPontuais(UserPrincipal principal) {
        log.info("Exibindo a tela de Releases...");

        return new ModelAndView(new BuildsPontuaisView(principal, buildsPontuaisService.getAllProductVersions()));
    }

    @RolesAllowed(AccessPermission.PERM_ALLOW_SEE_DASHBOARD)
    @GetMapping(value = "/dashboard")
    public ModelAndView showDashboard(UserPrincipal principal) {
        log.info("Exibindo a tela de Dashboard...");

        DashboardCommand command = new DashboardCommand();
        command.setCurrentRelease(releaseService.getCurrentReleaseVersion());
        command.setAllReleaseIssues(releaseService.getAllIssues());
        command.setPassedReleaseIssues(releaseService.getPassedIssues().size());
        command.setReturnedReleaseIssues(releaseService.getReturnedIssuesCount());
        command.setAllBuildsIssues(buildsPontuaisService.getBuildsIssues());
        command.setImpactsBuildsIssues(buildsPontuaisService.getBuildsImpactIssues().size());
        command.setUrlIssueFilterBase(releaseService.formatFilterIssueURL());
        command.setUrlBuildFilterBase(buildsPontuaisService.getFilterBuildURL());
        return new ModelAndView(new DashboardView(principal, command));
    }

    @RolesAllowed(AccessPermission.PERM_ALLOW_SEE_KANBAN)
    @GetMapping(value = "/kanban")
    public ModelAndView showKanban(UserPrincipal principal) {
        log.info("Exibindo a tela do Kanban...");

        return new ModelAndView(new KanbanView(kanbanService.getUsersTasks(), principal));
    }

    @GetMapping(value = "/kanban/task/new")
    public ModelAndView newKanbanTask(UserHumanBeingPrincipal principal) {
        log.info("Exibindo a tela de criação de task...");

        TaskCommand command = new TaskCommand();
        command.setUserHumanBeing(principal.getHumanBeing());
        return new ModelAndView(new CreateTaskPageView(principal.getCustomRoot(), command));
    }

    @ResponseBody
    @PostMapping(value = "/kanban/task/create")
    public MessageCommand createTask(@RequestBody TaskCommand command, final UserPrincipal userPrincipal) {
        MessageCommand messageCommand = new MessageCommand();

        try {
            kanbanService.save(TaskCommand.loadTaskFromCommand(command));
            sendRedirect(command, userPrincipal, messageCommand);
        } catch (ConstraintViolationException e) {
            messageCommand.addError(e.getMessage());
        }

        return messageCommand;
    }

    @GetMapping("/kanban/task/edit/{taskId}")
    public ModelAndView editTask(final UserPrincipal userPrincipal, Tarefa task) {
        TaskCommand command = TaskCommand.loadCommandFromTask(task);
        return new ModelAndView(new EditTaskPageView(userPrincipal.getCustomRoot(), command, task));
    }

    @ResponseBody
    @PostMapping(value = "/kanban/task/edit/{taskId}")
    public MessageCommand editTask(Tarefa task, @RequestBody TaskCommand command, final UserPrincipal userPrincipal) {
        MessageCommand messageCommand = new MessageCommand();

        try {
            kanbanService.save(TaskCommand.loadTaskFromCommand(command, task));
            sendRedirect(command, userPrincipal, messageCommand);
        } catch (ConstraintViolationException e) {
            messageCommand.addError(e.getMessage());
        }

        return messageCommand;
    }

    @ResponseBody
    @PostMapping(value = "/kanban/task/remove/{taskId}")
    public MessageCommand removeTask(Tarefa task, @RequestBody TaskCommand command, final UserPrincipal userPrincipal) {
        MessageCommand messageCommand = new MessageCommand();

        try {
            kanbanService.remove(task.getId());
            messageCommand.addMessage("Tarefa removida!");
            sendRedirect(command, userPrincipal, messageCommand);
        } catch (ConstraintViolationException e) {
            messageCommand.addError(e.getMessage());
        }

        return messageCommand;
    }

    private void sendRedirect(@RequestBody TaskCommand command, UserPrincipal userPrincipal, MessageCommand messageCommand) {
        String redirectUrl = ManagementControllerURL.showKanban(userPrincipal.getCustomRoot());
        if (command != null && command.getRedirectUrl() != null)
            redirectUrl = command.getRedirectUrl();

        messageCommand.addData("redirectTo", redirectUrl);
    }

    @ResponseBody
    @GetMapping(value = "/getMonthlyGmudsModalChart/{month}")
    public List<GmudMensalModel> getMonthlyGmudsModalChart(UserPrincipal principal, @PathVariable("month") String month) {
        return gmuds.stream().filter(gmud -> month.equals(gmud.getMes())).collect(Collectors.toList());
    }

    @ResponseBody
    @GetMapping(value = "/getGmuds")
    public List<GmudMensalModel> getGmudsMensais() {
        log.info("Retornando a lista anual de issues do tipo Gmud...");

        return gmuds;
    }

    @ResponseBody
    @GetMapping(value = "/getCasualBuilds")
    public List<Buildspontuais> getCasualBuilds() {
        log.info("Retornando a lista anual de issues do tipo Gmud...");

        return buildsPontuaisService.getAll();
    }

    @ResponseBody
    @GetMapping(value = "/getReleasePassedIssues")
    public Map<LocalDate, Long> getReleasePassedIssues() {
        log.info("Retornando a lista de chamados validados da Release atual...");

        return releaseService.splitPassedIssuesByDay();
    }

    @ResponseBody
    @GetMapping(value = "/getReleaseReturnedIssues")
    public Map<LocalDate, Long> getReleaseReturnedIssues() {
        log.info("Retornando a lista de chamados retornados da Release atual...");

        return releaseService.splitReturnedIssuesByDay();
    }

    @ResponseBody
    @GetMapping(value = "/getBuildsIssues")
    public Map<LocalDate, Long> getBuildsIssues() {
        log.info("Retornando a lista de chamados do build pontual...");

        return buildsPontuaisService.splitBuildsIssuesByDay();
    }

    @ResponseBody
    @GetMapping(value = "/getBuildsImpactIssues")
    public Map<LocalDate, Long> getBuildsImpactIssues() {
        log.info("Retornando a lista de chamados de impacto do build pontual...");

        return buildsPontuaisService.splitBuildsImpactIssuesByDay();
    }

    @ResponseBody
    @GetMapping(value = "/getReleaseSpeed")
    public Long getReleaseSpeed() {
        log.info("Retornando a lista de chamados do build pontual...");

        int passedIssues = releaseService.getPassedIssues().size();
        int totalIssues = releaseService.getAllIssues().size();
        return Math.round(passedIssues * 100.0/totalIssues);
    }

    @ResponseBody
    @GetMapping(value = "/getLatestYears")
    public List<String> getLatestYears() {
        log.info("Retornando a lista de últimos anos...");

        return gmudService.getLatestYears();
    }
}