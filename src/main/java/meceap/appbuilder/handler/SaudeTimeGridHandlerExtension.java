package meceap.appbuilder.handler;

import br.com.me.ceap.web.ui.html.MEGrid;

/**
 * @author lucasns
 * @since #1.0
 */
public interface SaudeTimeGridHandlerExtension {
// -------------------------- OTHER METHODS --------------------------

    void handlerGrid(MEGrid grid);
}