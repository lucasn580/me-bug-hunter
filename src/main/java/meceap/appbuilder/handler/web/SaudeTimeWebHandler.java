package meceap.appbuilder.handler.web;

import br.com.me.appbuilder.document.saudeTime.SaudeTime;
import br.com.me.ceap.dynamic.extension.model.handler.DocumentGridParam;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.extension.handler.DocumentGridHandlerData;
import br.com.me.ceap.web.ui.html.MEGrid;
import br.com.me.ceap.web.ui.html.MEGridLinkColumn;
import br.com.me.ceap.web.ui.html.MEGridView;
import meceap.appbuilder.handler.SaudeTimeGridHandlerExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author lucasns
 * @since #1.0
 */
@Component
public class SaudeTimeWebHandler implements DocumentGridHandlerData<SaudeTime> {
// ------------------------------ FIELDS ------------------------------

    private SaudeTimeGridHandlerExtension handlerExtension;

// --------------------- GETTER / SETTER METHODS ---------------------

    @Autowired(required = false)
    public void setHandlerExtension(SaudeTimeGridHandlerExtension handlerExtension) {
        this.handlerExtension = handlerExtension;
    }

// --------------------- Interface DocumentGridHandlerData ---------------------

    @Override
    public void handle(DocumentGridParam<SaudeTime> documentGridParam) {
        MEGrid grid = documentGridParam.getMeGrid();
        grid.setPageSizeOptions(20, 40, 60, 100, 200);
        grid.setBindingRoot(documentGridParam.getUserCustomRoot());
        grid.getViews().clear();
        grid.addField("atributoSaudeTime");
        grid.setFieldRequest(true);
        grid.addViews(
                new MEGridView("table").addColumns(
                        MEGrid.linkColumn("id", String.format("/%s/document/SaudeTime/{id}", documentGridParam.getUserCustomRoot().getName()))
                                .setTarget(MEGridLinkColumn.Target.BLANK)
                                .setTitle("ID")
                                .setDataField("id")
                                .setDataFieldType("integer"),
                        MEGrid.column("owner")
                                .setTitle("Criador")
                                .setDataField("owner.name")
                                .setDataFieldType("owner"),
                        MEGrid.column("created")
                                .setTitle("Data de criação")
                                .setDataField("created")
                                .setDataFieldType("datetime"),
                        MEGrid.column("updated")
                                .setTitle("Data de Alteração")
                                .setDataField("updated")
                                .setDataFieldType("datetime"),
                        MEGrid.column("sectionOne")
                                .setTitle(UserCatalog.getMessage("saudetime.section.first"))
                                .setToolTip(UserCatalog.getMessage("saudetime.section.description"))
                                .setFnRender("StatusHealthColorGridRenderer"),
                        MEGrid.column("sectionTwo")
                                .setTitle(UserCatalog.getMessage("saudetime.section.second"))
                                .setToolTip(UserCatalog.getMessage("saudetime.section.description"))
                                .setFnRender("StatusHealthColorGridRenderer"),
                        MEGrid.column("sectionThree")
                                .setTitle(UserCatalog.getMessage("saudetime.section.third"))
                                .setToolTip(UserCatalog.getMessage("saudetime.section.description"))
                                .setFnRender("StatusHealthColorGridRenderer"),
                        MEGrid.column("sectionFour")
                                .setTitle(UserCatalog.getMessage("saudetime.section.fourth"))
                                .setToolTip(UserCatalog.getMessage("saudetime.section.description"))
                                .setFnRender("StatusHealthColorGridRenderer")
                )
        );

        if (this.handlerExtension != null)
            this.handlerExtension.handlerGrid(grid);
    }
}