package meceap.appbuilder.handler.web;

import br.com.me.ceap.dynamic.extension.model.base.UserCustomRoot;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.web.extension.handler.UserPortalShowHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Component
public class RedirectHandler implements UserPortalShowHandler {
// ------------------------ INTERFACE METHODS ------------------------

// --------------------- Interface UserPortalShowHandler ---------------------
    @Override
    public ModelAndView showUserPortal(UserHumanBeing userHumanBeing, UserCustomRoot customRoot) {
        return new ModelAndView(new RedirectView(String.format("/%s/do/Home", customRoot.getName())));
    }
}