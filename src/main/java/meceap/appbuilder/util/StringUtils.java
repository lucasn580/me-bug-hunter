package meceap.appbuilder.util;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author lucasns
 * @since #1.0
 */
public class StringUtils {

    /**
     * Remove Html tags
     *
     * @param text Text to be treated
     * @return Content treated
     */
    @NotNull
    public static String retirarTagsHtml(String text) {
        text = text.replaceAll("<br>", "");
        text = text.replaceAll("<p>", "");
        text = text.replaceAll("</p>", "");
        text = text.replaceAll("&nbsp;", " ");
        return text.trim();
    }

    /**
     * Format Date and convert to String
     *
     * @param date Date to be treated
     * @return date treated
     */
    @NotNull
    public static String formatarData(Date date, boolean showHours, Logger log) {
        String pattern = showHours ? "dd/MM/yyyy HH:mm:ss" : "dd/MM/yyyy";
        DateFormat converter = new SimpleDateFormat(pattern);
        converter.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));

        String strdate = "";
        try {
            strdate = converter.format(date);
        } catch (Exception e){
            log.error("Erro ao converter data");
            log.error(e.getMessage());
        }
        return strdate;
    }

    public static String formatarMesData(Date date){
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale("pt", "BR"));
        DateFormat format = new SimpleDateFormat("MMMM", symbols);
        return format.format(date);
    }

    public static String formatarDecimalData(double valor){
        DecimalFormat mFormat= new DecimalFormat("00");
        return mFormat.format(valor);
    }

    public static String formatarDecimal(double valor){
        DecimalFormat mFormat= new DecimalFormat("0.00");
        return mFormat.format(valor);
    }

    public static String formatarComEspacos(String valor){
        List<String> newValue = Arrays.asList(valor.split(","));
        return String.join(", ", newValue);
    }
}