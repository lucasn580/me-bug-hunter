package meceap.appbuilder.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class CustomCriteria {

    private static final Logger log = LoggerFactory.getLogger(CustomCriteria.class);

    /**
     * Monta um json com base em criterias e operator
     *
     * @param criterias Lista de critérios para query.
     * @param operator Condição da query.
     * @return json formatado.
     */
    public static String jsonRequest(List<String[]> criterias, String operator){
        String json = "{\n" +
                "  \"criterionGroups\": [\n" +
                "    {\n" +
                "      \"criteriaOperatorGroup\": \""+operator+"\",\n" +
                "      \"criteria\": [\n";

        for(String[] cri : criterias) {
            json += "        \n{\n" +
                    "          \"operator\": \""+cri[0]+"\",\n" +
                    "          \"value\": \""+cri[1]+"\",\n" +
                    "          \"fieldName\": \""+cri[2]+"\"\n" +
                    "        },";
        }
        json = json.substring(0, json.length()-1);
        json +=
                "      \n]\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}";

        log.info(json);
        return json;
    }

    /**
     * Monta um json com criterias nos operators OR e AND
     *
     * @param andCriterias Lista de critérios para query com condição AND.
     * @param orCriterias Lista de critérios para query com condição OR.
     * @return json formatado.
     */
    public static String jsonRequest(List<String[]> andCriterias, Collection<String> orCriterias){
        String json = "{\n" +
                "  \"criterionGroups\": [\n" +
                "    {\n" +
                "      \"criteriaOperatorGroup\": \"AND\",\n" +
                "      \"criteria\": [\n";

        for(String[] cri : andCriterias) {
            json += "        {\n" +
                    "          \"operator\": \""+cri[0]+"\",\n" +
                    "          \"value\": \""+cri[1]+"\",\n" +
                    "          \"fieldName\": \""+cri[2]+"\"\n" +
                    "        },";
        }
        json = json.substring(0, json.length()-1);
        json +=
                "      \n]\n" +
                        "    },{\n" +
                        "      \"criteriaOperatorGroup\": \"OR\",\n" +
                        "      \"criteria\": [\n";

        for(String m : orCriterias) {
            json += "        {\n" +
                    "          \"operator\": \"is\",\n" +
                    "          \"value\": \""+m+"\",\n" +
                    "          \"fieldName\": \"header.metricaDocId\"\n" +
                    "        },";
        }
        json = json.substring(0, json.length()-1);
        json +="      \n]\n" +
                "    }\n" +
                "  ],\n" +
                "\"sortFields\": [\n" +
                "    {\n" +
                "      \"fieldName\": \"header.updated\",\n" +
                "      \"direction\": \"DESC\"\n" +
                "    }\n" +
                "  ]" +
                "}";

        log.info(json);
        return json;
    }

    /**
     * Monta um json com criterias para POST de resultado métricas
     *
     * @param metric ID da métrica a ser atualizada
     * @return json formatado.
     */
    public static String jsonResultMetric(Calendar cal, String metric, String value){
        int mesPassado = cal.get(Calendar.MONTH) +1;
        int ano = cal.get(Calendar.YEAR);

        String json = "{\n" +
                "  \"header\": {\n" +
                "    \"metricaDocId\": \""+ metric +"\",  \n" +
                "    \"mes\": "+ mesPassado +",\n" +
                "    \"ano\": "+ ano +"\n" +
                "  },\n" +
                "  \"resultadoList\": [\n" +
                "    {\n" +
                "      \"resultadoMetrica\": "+ value +",\n" +
                "      \"dataResultado\": "+ cal.getTimeInMillis() +"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        log.info(json);
        return json;
    }
}