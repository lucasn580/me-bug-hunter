package meceap.appbuilder.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class RestTemplateFactory {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
// -------------------------- STATIC METHODS --------------------------

    public ClientHttpRequestFactory getClientHttpRequestFactory() {
        int timeout = 10000;
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
                = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(timeout);

        return clientHttpRequestFactory;
    }

    public <T> T postForObject(String url, Class<T> object, RestTemplate restTemplate, String json) {
        log.info("Posting on: " + url);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        HttpEntity<String> request = new HttpEntity<>(json, headers);

        restTemplate.getInterceptors().add(
                new BasicAuthorizationInterceptor("lucas.santos@me.com.br", "Qualidade@123"));
        return restTemplate.postForObject(url, request, object);
    }

    public <T> T postForObject(String url, Class<T> object) throws IllegalAccessException, InstantiationException {
        ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        HttpEntity<T> request = new HttpEntity<>(object.newInstance());
        return restTemplate.postForObject (url, request, object);
    }
}
