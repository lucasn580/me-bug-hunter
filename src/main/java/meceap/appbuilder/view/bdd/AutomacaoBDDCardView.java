package meceap.appbuilder.view.bdd;

import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.appbuilder.document.bDD.PassosItemInBDD;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.HtmlElement;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.ui.html.Span;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoBDDCardView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final BDD bdd;

    public AutomacaoBDDCardView(UserPrincipal principal, BDD bdd) {
        this.principal = principal;
        this.bdd = bdd;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view com informações do fluxo automatizado do cliente...");

        String descricao = StringUtils.retirarTagsHtml(bdd.getBDD().getNome());

        mainDiv.addChildTag(new Label(UserCatalog.getMessage("bdd.field.nome")));
        mainDiv.addChildTag(new DivLayout().addChildTag(new Span(descricao)));
        mainDiv.addChildTag(new HtmlElement("br"));
        mainDiv.addChildTag(new Label(UserCatalog.getMessage("bdd.field.acoes")));

        List<String> acoes = new ArrayList<>();

        for (PassosItemInBDD passo : bdd.getBDD().getPassosList()) {
            String pagina = passo.getAcao();
            if(!acoes.contains(pagina)){
                acoes.add(pagina);
                mainDiv.addChildTag(new DivLayout().addChildTag(new Span(pagina)));
            }
        }
    }
}