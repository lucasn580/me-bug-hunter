package meceap.appbuilder.view.bdd;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.HtmlElement;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.EnvironmentCommand;
import meceap.appbuilder.model.ExecutionCommand;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoBDDRunTestView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final ExecutionCommand customersNames;

    public AutomacaoBDDRunTestView(UserPrincipal principal, ExecutionCommand customersNames) {
        this.principal = principal;
        this.customersNames = customersNames;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view de execução do fluxo automatizado do cliente...");

        mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.flows")));
        mainDiv.addChildTag(new HtmlElement("br"));
        mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.customers") + StringUtils.formatarComEspacos(customersNames.getIds())));
        addFooter(mainDiv);
    }

    private void addFooter(DivLayout div) {
        div.addChildTag(new HtmlElement("br"));
        div.addChildTag(new HtmlElement("br"));
        div.addChildTag(new Button().setJsClick("StartBuildViewCoffee.next", Collections.singletonMap("obj", customersNames))
                .setDisableAfterClick(true)
                .setButtonStyle(Button.ButtonStyle.Warning)
                .setTextValue(UserCatalog.getMessage("button.executar")));
    }
}