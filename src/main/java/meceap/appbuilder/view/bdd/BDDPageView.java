package meceap.appbuilder.view.bdd;

import br.com.me.ceap.dynamic.extension.menu.SideBar;
import br.com.me.ceap.dynamic.extension.menu.SideBarMenu;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;

import java.util.Collections;

/**
 * @author lucasns
 * @since #1.0
 */
public class BDDPageView extends AbstractTitledPageView implements SideBarMenu {

    private final UserPrincipal principal;

    public BDDPageView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return UserCatalog.getMessage("title.bdd");
    }

    @Override
    protected void prepareDiv(final DivLayout divLayout) {

        DivLayout div = new DivLayout().setStyleClass("bckgdMenuDefault");
        DivLayout divLeft = new DivLayout().setStyleClass("divTips");
        DivLayout divMid = new DivLayout().setStyleClass("divBtns").setId("divBtns");
        DivLayout divRig = new DivLayout().setStyleClass("divBddLbl");

        // =================================== GUIA BDD ===================================
        DivLayout divTxt1 = new DivLayout().setAttribute("class", "container-text-home");
        DivLayout divTxt2 = new DivLayout().setAttribute("class", "row-home justify-content-home");
        DivLayout divTxt3 = new DivLayout().setAttribute("class", "col-md-8-home");
        DivLayout divTxt4 = new DivLayout().setAttribute("class", "tips-home-content");
        DivLayout divTxt5 = new DivLayout().setAttribute("class", "texto-home");
        DivLayout divTxt6 = new DivLayout().setAttribute("class", "texto-home-inner");

        divTxt6.addChildTag(new HtmlElement("h2").setTextValue(UserCatalog.getMessage("bdd.guide.title")));
        divTxt6.addChildTag(new HtmlElement("br"));
        divTxt6.addChildTag(new HtmlElement("h4").setTextValue(UserCatalog.getMessage("bdd.guide.use")));
        divTxt6.addChildTag(new HtmlElement("p").setTextValue("BDD serve para criar testes e integrar regras de negócios " +
                "com a linguagem  de programação, focando no comportamento do software. Além disso, ainda melhora a comunicação " +
                "entre as equipes de desenvolvimento e testes, aumentando o compartilhamento de conhecimento entre elas."));
        divTxt6.addChildTag(new HtmlElement("br"));
        divTxt6.addChildTag(new HtmlElement("h4").setTextValue(UserCatalog.getMessage("bdd.guide.utility")));
        divTxt6.addChildTag(new HtmlElement("p").setTextValue("Esta metodologia é útil em projetos de software ágeis, que " +
                "são construídos em várias iterações e estão sofrendo alterações ao longo do seu ciclo de vida. Quanto maior o " +
                "projeto, mais difícil será a comunicação. Entretanto, BDD propõe uma forma eficaz de resolver estes problemas."));
        divTxt6.addChildTag(new HtmlElement("br"));
        divTxt6.addChildTag(new HtmlElement("br"));
        divTxt6.addChildTag(new HrefLinkAnchor("https://www.devmedia.com.br/desenvolvimento-orientado-por-comportamento-bdd/21127")
                .setTarget(HrefLinkAnchor.Target.BLANK)
                .setTextValue(UserCatalog.getMessage("bdd.guide.link"))
                .setAttribute("style", "color: blue;"));
        divTxt5.addChildTag(divTxt6);
        divTxt4.addChildTag(divTxt5);
        divTxt3.addChildTag(divTxt4);
        divTxt2.addChildTag(divTxt3);
        divTxt1.addChildTag(divTxt2);
        divLeft.addChildTag(divTxt1);

        // =================================== Botões ===================================
        divMid.addChildTag(new DivLayout().setStyleClass("divBtn")
                .addChildTag(new Button()
                        .setUrlAction(String.format("/%s/search/BDD", principal.getCustomRoot().getName()))
                        .setButtonStyle(Button.ButtonStyle.Outline)
                        .setTextValue(UserCatalog.getMessage("button.doc.bdd"))
                        .setStyleClass("btnCustomMenu")
                        .setAttribute("data-text", UserCatalog.getMessage("data.text.bdd.novo"))));
        divMid.addChildTag(new DivLayout().setStyleClass("divBtn")
                .addChildTag(new Button()
                        .setUrlAction(AutomacaoControllerURL.showBDDFlows(principal.getCustomRoot()))
                        .setButtonStyle(Button.ButtonStyle.Outline)
                        .setTextValue(UserCatalog.getMessage("button.execucao"))
                        .setStyleClass("btnCustomMenu")
                        .setAttribute("data-text", UserCatalog.getMessage("data.text.execucao"))));
        divMid.addChildTag(new DivLayout().setStyleClass("divBtn")
                .addChildTag(new Button()
                        .setJsClick("ExecutionsViewCoffee.next", Collections.singletonMap("type", "bdd"))
                        .setButtonStyle(Button.ButtonStyle.Outline)
                        .setTextValue(UserCatalog.getMessage("button.report"))
                        .setStyleClass("btnCustomMenu")
                        .setAttribute("data-text", UserCatalog.getMessage("data.text.reports"))));

        // =================================== Label ===================================
        Label label = new Label().setId("lblMenuOpc");
        divRig.addChildTag(label);

        div.addChildTag(divLeft);
        div.addChildTag(divMid);
        div.addChildTag(divRig);

        // =================================== FOOTER ===================================
        DivLayout footer = new DivLayout().setStyleClass("board-lego");

        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 6; j++) {
                footer.addChildTag(new DivLayout().setStyleClass("brick b-" + i + "x" + j));
            }
        }

        divLayout.addChildTag(div);
        divLayout.addChildTag(footer);
    }

    @Override
    public String getMenu() {
        return SideBar.HOME;
    }
}