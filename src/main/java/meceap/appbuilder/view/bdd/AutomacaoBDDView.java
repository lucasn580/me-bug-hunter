package meceap.appbuilder.view.bdd;

import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.model.ClienteModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoBDDView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<ClienteModel> clientes;

    public AutomacaoBDDView(final List<ClienteModel> clientes, UserPrincipal principal) {
        this.principal = principal;
        this.clientes = clientes;
    }

    @Override
    protected String getTitle() { return ""; }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a lista de automações BDD...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");
        DivLayout div1 = new DivLayout().setStyleClass("dd");

        mainDiv.setId("customersBoard");
        createPageTitle(mainDiv);
        createHeaderButtons(mainDiv);
        createCustomerCard(div1);

        mainDiv.addChildTag(div1);
    }

    private void createPageTitle(DivLayout div) {
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.automation")))));
    }

    private void createHeaderButtons(DivLayout div) {
        div.addChildTag(new DivLayout().setStyleClass("div-buttons")
                .addChildTag(new Label(UserCatalog.getMessage("label.ambiente")))
                .addChildTag(new SelectInput()
                        .setOptionValue("nome")
                        .setOptionText("nome")
                        .setOptions(AutomacaoControllerURL.getActivesEnvironments(principal.getCustomRoot()))
                        .setId("environment"))
                .addChildTag(new Button()
                        .setJsClick("EnvironmentViewCoffee.show").setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-ballot-check")))
                .addChildTag(new DivLayout().setStyleClass("separator-line"))
                .addChildTag(new Button().setJsClick("ExecutionsViewCoffee.next", Collections.singletonMap("type", "bdd"))
                        .setId("btnVerExecucoes").setTextValue(UserCatalog.getMessage("button.ver.execucoes")))
                .addChildTag(new Button().setJsClick("RunTestViewCoffee.performAllBDD")
                        .setId("btnExecutarBDD").setTextValue(UserCatalog.getMessage("button.executar.tudo"))));
    }

    private void createCustomerCard(DivLayout div) {
        for (ClienteModel cli : clientes) {

            Map<String, Object> params = new HashMap<>();
            params.put("id", cli.getCliente());
            params.put("type", "bdd");

            HtmlElement ol = new HtmlElement("ol").setStyleClass("kanban top-bar");
            div.addChildTag(ol
                    .addChildTag(new DivLayout()
                            .addChildTag(new Heading(Heading.HeadingType.h2)
                                    .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-user-cog"))
                                    .addChildTag(new Span(cli.getCliente()))
                                    .addChildTag(new Button(null)
                                    .setJsClick("RunTestViewCoffee.unitTest", params)
                                            .setStyleClass("btn-icon")
                                            .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-caret-circle-right"))))));

            createCustomerCardItems(cli, ol);

            ol.addChildTag(new DivLayout().setStyleClass("actions")
                    .addChildTag(new Button(null).setUrlAction(String.format("/%s/document/BDD/create", principal.getCustomRoot().getName()))
                            .setStyleClass("addbutt").setTextValue(UserCatalog.getMessage("button.add.new"))));
        }
    }

    private void createCustomerCardItems(ClienteModel cli, HtmlElement ol) {

        for (BDD bdd : cli.getBdd()) {

            HtmlElement li = new HtmlElement("li").setStyleClass("dd-item");

            ol.addChildTag(li
                    .addChildTag(new Heading(Heading.HeadingType.h3).setStyleClass("title")
                            .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-clone dd-handle"))
                            .addChildTag(new Span(String.format("%s - %s", bdd.getId(), bdd.getBDD().getFluxo().getNome())))));

            li.addChildTag(new DivLayout().setStyleClass("text").setAttribute("contenteditable", "false")
                    .setTextValue(bdd.getBDD().getNome()));

            createCustomerActionButtons(li, bdd);
        }
    }

    private void createCustomerActionButtons(HtmlElement li, BDD bdd) {

        li.addChildTag(new DivLayout().setStyleClass("actions")
                .addChildTag(new Button(null)
                        .setUrlAction(String.format("/%s/document/BDD/" + bdd.getId(), principal.getCustomRoot().getName()))
                        .setStyleClass("btn-icon").setAttribute("target", "_blank")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-edit")))

                .addChildTag(new Button(null)
                        .setJsClick("InformationsFlowViewCoffee.bdd", Collections.singletonMap("id", bdd.getId()))
                        .setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-info"))));
    }
}