package meceap.appbuilder.view.home;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserHumanBeingPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractComponentView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.controller.url.HomeControllerURL;

/**
 * @author lucasns
 * @since #1.0
 */
public class HomeView extends AbstractComponentView {

    private final UserHumanBeingPrincipal principal;

    public HomeView(UserHumanBeingPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected HtmlElement getComponent() {
        return new DivLayout().addChildTag(
                createTitle(),
                addMenu(),
                addFooter()
        );
    }

    private HtmlElement createTitle(){
        DivLayout div = new DivLayout();
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h2)
                        .addChildTag(new ImageInput().setImageURL("/bughunter/resource/?name=resources/img/logo-m.png").setStyleClass("imgME"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.home")))));
        return div;
    }

    private HtmlElement addMenu() {

        DivLayout div = new DivLayout().setStyleClass("bckgdMenuDefault");
        DivLayout divLeft = new DivLayout().setAttribute("style", "width: 100%; min-height: 650px;");

        // =================================== MENU ===================================
        DivLayout divMenu = new DivLayout();
        HtmlElement nav = new HtmlElement("nav")
                .setAttribute("style", "list-style: none; font-size: 1.5em; font-weight: 300;")
                .setStyleClass("navDiv");
        HtmlElement ul = new HtmlElement("ul");

        createMenuButton(ul,
                UserCatalog.getMessage("button.cenarios"),
                UserCatalog.getMessage("data.text.cenarios"),
                "superman.png",
                String.format("/%s/search/CasodeTeste", principal.getCustomRoot().getName()),
                "icon-ballot-check");

        createMenuButton(ul,
                UserCatalog.getMessage("button.bdd"),
                UserCatalog.getMessage("data.text.bdd"),
                "spiderman.png",
                HomeControllerURL.showBDDPage(principal.getCustomRoot()),
                "icon-list-ol");

        createMenuButton(ul,
                UserCatalog.getMessage("button.suites"),
                UserCatalog.getMessage("data.text.suites"),
                "batman.png",
                HomeControllerURL.showSuites(principal.getCustomRoot()),
                "icon-apps");

        createMenuButton(ul,
                UserCatalog.getMessage("button.clientes"),
                UserCatalog.getMessage("data.text.clientes"),
                "flash.png",
                HomeControllerURL.showErrorsByCustomer(principal.getCustomRoot()),
                "icon-smile-beam");

        createMenuButton(ul,
                UserCatalog.getMessage("button.automacao"),
                UserCatalog.getMessage("data.text.automacao"),
                "batgirl.png",
                AutomacaoControllerURL.index(principal.getCustomRoot()),
                "icon-sun");

        createMenuButton(ul,
                UserCatalog.getMessage("button.builds"),
                UserCatalog.getMessage("data.text.builds"),
                "captain.png",
                AutomacaoControllerURL.showBuildsPage(principal.getCustomRoot()),
                "icon-temperature-hot");

        createMenuButton(ul,
                UserCatalog.getMessage("button.gestao"),
                UserCatalog.getMessage("data.text.gestao"),
                "wonderwoman.png",
                HomeControllerURL.showManagerPage(principal.getCustomRoot()),
                "icon-analytics");

        createMenuButton(ul,
                UserCatalog.getMessage("button.wiki"),
                UserCatalog.getMessage("data.text.wiki"),
                "blackwidow.png",
                String.format("/%s/search/Wiki", principal.getCustomRoot().getName()),
                "icon-book-open");

        createMenuButton(ul,
                UserCatalog.getMessage("button.saude"),
                UserCatalog.getMessage("data.text.saude"),
                "hulk.png",
                String.format("/%s/search/SaudeTime", principal.getCustomRoot().getName()),
                "icon-heartbeat");

        nav.addChildTag(ul);
        divMenu.addChildTag(nav);

        DivLayout divDesc = new DivLayout();
        Label label = new Label().setId("lblMenuOpc");
        ImageInput imgLego = new ImageInput().setAttribute("id", "imgLego");

        // =================================== DIVS ===================================
        divDesc.addChildTag(label);
        divDesc.addChildTag(imgLego);
        divLeft.addChildTag(divMenu);
        divLeft.addChildTag(divDesc);
        div.addChildTag(divLeft);
        return div;
    }

    private void createMenuButton(HtmlElement ul, String title, String dataText, String dataImg, String href, String iconClass) {
        ul.addChildTag(new HtmlElement("li")
                .setAttribute("data-text", dataText)
                .setAttribute("data-img", "/bughunter/resource/?name=resources/img/" + dataImg)
                .addChildTag(new HrefLinkAnchor().setHref(href)
                        .addChildTag(new Span().setStyleClass("icon")
                                .addChildTag(new DivLayout().setStyleClass("lines-row")
                                        .addChildTag(new DivLayout().setStyleClass("dp")
                                                .addChildTag(new DivLayout().setStyleClass("dp-arc-inner"))
                                                .addChildTag(new DivLayout().setStyleClass("dp-arc-outer"))
                                                .addChildTag(new Icon().setStyleClass("me-icon " + iconClass))))
                                .addChildTag(new Span(title).setStyleClass("spanTitle")))));
    }

    private HtmlElement addFooter() {
        DivLayout div = new DivLayout();
        DivLayout legoFooter = new DivLayout().setStyleClass("board-lego");

        for(int i = 1; i<=5; i++){
            for(int j = 1; j<=6; j++)
                legoFooter.addChildTag(new DivLayout().setStyleClass("brick b-"+i+"x"+j));
        }

        DivLayout iconCreator = new DivLayout().setId("blip-container")
                .setAttribute("data-text", UserCatalog.getMessage("data.text.footer"))
                .addChildTag(new DivLayout().setId("blip-open-iframe")
                        .addChildTag(new ImageInput().setImageURL("/bughunter/resource/?name=resources/img/ironman.png")));

        DivLayout balloon = new DivLayout().setId("talk-bubble")
                .setStyleClass("talk-bubble tri-right border-balloon round-balloon btm-right-in")
                .addChildTag(new DivLayout().setStyleClass("talktext"));

        div.addChildTag(legoFooter);
        div.addChildTag(iconCreator);
        div.addChildTag(balloon);
        return div;
    }
}