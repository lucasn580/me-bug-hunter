package meceap.appbuilder.view.home;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.TestCaseModel;
import meceap.appbuilder.controller.url.HomeControllerURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class SuitesView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public SuitesView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações do cliente...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        createTitle(mainDiv);

        mainDiv.addChildTag(new MEGrid("customgrid")
                .setTableStyle(MEGrid.GridTableStyle.Bordered, MEGrid.GridTableStyle.Striped)
                .autoAddColumns(TestCaseModel.class)
                .setBaseURLData(HomeControllerURL.getSuitesInfo(principal.getCustomRoot())));
    }

    private void createTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.suites")))));
    }
}