package meceap.appbuilder.view.home;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.HtmlElement;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class ModalView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public ModalView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando modal view...");

        mainDiv.addChildTag(new Label("Acesso negado!"));
        mainDiv.addChildTag(new HtmlElement("br"));
        mainDiv.addChildTag(new Label("Você não tem permissão de acesso à esta página."));
    }
}