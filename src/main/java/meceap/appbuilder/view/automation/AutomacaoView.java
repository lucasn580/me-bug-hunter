package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.controller.url.HomeControllerURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public AutomacaoView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a lista de casos de teste automatizados...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");
        DivLayout div1 = new DivLayout().setStyleClass("dd");

        mainDiv.setId("customersBoard");
        createPageTitle(mainDiv);
        createHeaderButtons(mainDiv);
        createGrid(div1);

        mainDiv.addChildTag(div1);
    }

    private void createPageTitle(DivLayout div) {
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.automation")))));
    }

    private void createHeaderButtons(DivLayout div) {
        div.addChildTag(new DivLayout().setStyleClass("div-buttons")
                .addChildTag(new Label(UserCatalog.getMessage("label.ambiente")))
                .addChildTag(new SelectInput()
                        .setOptionValue("nome")
                        .setOptionText("nome")
                        .setOptions(AutomacaoControllerURL.getActivesEnvironments(principal.getCustomRoot()))
                        .setId("environment"))
                .addChildTag(new Button()
                        .setJsClick("EnvironmentViewCoffee.show").setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-ballot-check")))
                .addChildTag(new Button().setUrlAction(AutomacaoControllerURL.customBuilder(principal.getCustomRoot()))
                        .setId("btnConstruir").setTextValue(UserCatalog.getMessage("button.contruir")))
                .addChildTag(new DivLayout().setStyleClass("separator-line"))
                .addChildTag(new Button().setJsClick("ExecutionsViewCoffee.next", Collections.singletonMap("type", "testCase"))
                        .setId("btnVerExecucoes").setTextValue(UserCatalog.getMessage("button.ver.execucoes")))
                .addChildTag(new Button().setJsClick("RunTestViewCoffee.performAllCts")
                        .setId("btnExecutarTudo").setTextValue(UserCatalog.getMessage("button.executar.tudo")))
                .addChildTag(new Button().setJsClick("RunTestViewCoffee.onlyChecked")
                        .setId("btnExecutar").setTextValue(UserCatalog.getMessage("button.executar"))));
    }

    private void createGrid(DivLayout div) {
        MEGrid grid = new MEGrid("customgrid");
        grid.setBaseURLData(AutomacaoControllerURL.getActivesTestCases(principal.getCustomRoot()));
        grid.setColumnChooser(false);
        grid.setMultiSelect(true);
        grid.setTableStyle(MEGrid.GridTableStyle.Bordered, MEGrid.GridTableStyle.Hover);
        grid.addColumns(
                MEGrid.column("id")
                        .setTitle(UserCatalog.getMessage("testcase.field.id"))
                        .setDataField("id"),
                MEGrid.column("casodeTeste.ordem")
                        .setTitle(UserCatalog.getMessage("testcase.field.ordem"))
                        .setDataField("casodeTeste.ordem"),
                MEGrid.column("casodeTeste.cliente.nome")
                        .setTitle(UserCatalog.getMessage("testcase.field.customer"))
                        .setDataField("casodeTeste.cliente.nome"),
                MEGrid.column("casodeTeste.fluxo.nome")
                        .setTitle(UserCatalog.getMessage("testcase.field.flow"))
                        .setDataField("casodeTeste.fluxo.nome"),
                MEGrid.column("casodeTeste.nome")
                        .setTitle(UserCatalog.getMessage("testcase.field.scenario.name"))
                        .setDataField("casodeTeste.nome")
        );
        div.addChildTag(grid);
    }
}