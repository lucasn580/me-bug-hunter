package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserHumanBeingPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.ExecutionCommand;
import meceap.appbuilder.model.ExecutorModel;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author lucasns
 * @since #1.0
 */
public class BuildRunsView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final UserHumanBeingPrincipal principal;
    private boolean isAdmin = false;
    private List<ExecutionCommand> execucoes;
    private List<ExecutorModel> executores;
    private Map<String,List<Double>> medias;

    public BuildRunsView(UserHumanBeingPrincipal principal, List<ExecutionCommand> execucoes) {
        this.principal = principal;
        this.execucoes = execucoes;
        this.executores = new ArrayList<>();
    }

    public BuildRunsView(UserHumanBeingPrincipal principal, boolean isAdmin, List<ExecutorModel> executores) {
        this.principal = principal;
        this.isAdmin = isAdmin;
        this.executores = executores;
        this.execucoes = new ArrayList<>();
        this.medias = new HashMap<>();
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações de execução automatizada de acordo com o perfil...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        DivLayout div = new DivLayout().setId("divBuilds");
        HtmlElement selectMedias = new HtmlElement("select");

        if(isAdmin){
            createTitleAdmin(div, selectMedias);

            if (executores.isEmpty()){
                div.addChildTag(new Heading(Heading.HeadingType.h3)
                        .addChildTag(new Span(UserCatalog.getMessage("label.execution.none.run"))));
            } else {
                for(int i = 0; i< executores.size(); i++){

                    ExecutorModel executor = executores.get(i);
                    if(!executor.getExecucoes().isEmpty()){
                        createUserHeader(div, executor.getAnalistaEmail());
                    }
                    createSelectVisibleRows(div, i);

                    DivLayout divExcs = new DivLayout().setId("table-id"+i);
                    for (ExecutionCommand execucao : executor.getExecucoes()){
                        createExecutionDiv(divExcs, execucao);

                        double rating = (execucao.getResultCommand().getRating() > 0 ? execucao.getResultCommand().getRating() : 0);
                        medias.computeIfAbsent(StringUtils.formatarMesData(execucao.getDate()), k -> new ArrayList<>()).add(rating);
                    }
                    div.addChildTag(divExcs);
                    createExecutionDivPagination(div, String.valueOf(i));
                }
                populateAverageMonthDiv(selectMedias);
                createUserBuildsChart(div);
            }
        } else {
            createTitleNonAdmin(div);
            for(ExecutionCommand execucao : execucoes)
                createExecutionDiv(div, execucao);
        }
        mainDiv.addChildTag(div);
    }

    private void createTitleAdmin(DivLayout div, HtmlElement select){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.build.running")))));
        div.addChildTag(new Label("User admin: " + principal.getHumanBeing().getName()));
        select.setId("average").setStyleClass("maxRows average");
        div.addChildTag(select);
        div.addChildTag(new Label().setId("lblAverage").setStyleClass("lblShowRows average"));
    }

    private void createTitleNonAdmin(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1).setAttribute("style", "margin-bottom: 50px;")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span("Suas execuções, " + principal.getHumanBeing().getName() + "!"))));
    }

    private void populateAverageMonthDiv(HtmlElement select){
        select.addChildTag(new HtmlElement("option").setTextValue(UserCatalog.getMessage("label.execution.average")));
        medias.forEach((mes, media) -> {
            double total = media.stream().mapToDouble(a -> a).sum();
            String mediaTotal = StringUtils.formatarDecimal(total/media.size());
            select.addChildTag(new HtmlElement("option").setTextValue(mes).setValue(mediaTotal));
        });
    }

    private void createUserHeader(DivLayout div, String user){
        DivLayout divUser = new DivLayout().setStyleClass("userDiv brick");
        divUser.addChildTag(new Heading(Heading.HeadingType.h3)
                .addChildTag(new Span(user).setStyleClass("spanUsername")));
        divUser.addChildTag(new HtmlElement("br"));
        div.addChildTag(divUser);
    }

    private void createSelectVisibleRows(DivLayout div, int item){
        div.addChildTag(new Label(UserCatalog.getMessage("label.execution.rows.record")).setStyleClass("lblShowRows"));
        HtmlElement select = new HtmlElement("select").setStyleClass("maxRows").setId("maxRows"+item);
        select.addChildTag(new HtmlElement("option").setTextValue(UserCatalog.getMessage("label.execution.rows.showAll")).setValue("5000"));
        select.addChildTag(new HtmlElement("option").setTextValue("5").setValue("5"));
        select.addChildTag(new HtmlElement("option").setTextValue("10").setValue("10"));
        select.addChildTag(new HtmlElement("option").setTextValue("15").setValue("15"));
        select.addChildTag(new HtmlElement("option").setTextValue("20").setValue("20"));
        select.addChildTag(new HtmlElement("option").setTextValue("50").setValue("50"));
        select.addChildTag(new HtmlElement("option").setTextValue("70").setValue("70"));
        select.addChildTag(new HtmlElement("option").setTextValue("100").setValue("100"));
        div.addChildTag(select);
    }

    private void createExecutionDiv(DivLayout div, ExecutionCommand execucao){
        DivLayout divExc = new DivLayout().setStyleClass("excUserDiv");
        divExc.addChildTag(new Label("Data: " + StringUtils.formatarData(execucao.getDate(), true, log)).setStyleClass("lblDateExec"));
        divExc.addChildTag(new Label(UserCatalog.getMessage("label.execution.flows.runned")).setStyleClass("lblExec"));
        divExc.addChildTag( new Button(null)
                .setJsClick("ScenariosRunnedViewCoffee.next", Collections.singletonMap("ids", execucao.getIds()))
                .setStyleClass("btn-icon btnExec")
                .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-info")));

        createResultInfos(divExc, UserCatalog.getMessage("label.execution.rows.passed"), "lblPassed", execucao.getResultCommand().getPassed());
        createResultInfos(divExc, UserCatalog.getMessage("label.execution.rows.failed"), "lblFailed", execucao.getResultCommand().getFailed(), execucao.getResultCommand().getFailures());
        createResultInfos(divExc, UserCatalog.getMessage("label.execution.rows.ignored"), "lblExec", execucao.getResultCommand().getIgnored());

        divExc.addChildTag(new Label(String.format("Rating (%%): %s", execucao.getResultCommand().getResult())).setStyleClass("lblPassed"));
        divExc.addChildTag(new HtmlElement("br"));
        div.addChildTag(divExc);
    }

    private void createExecutionDivPagination(DivLayout div, String id){
        DivLayout divPag = new DivLayout().setStyleClass("pagination-container");
        HtmlElement nav = new HtmlElement("nav");
        HtmlElement ul = new HtmlElement("ul").setStyleClass("pagination").setId("pagination" + id);
        ul.addChildTag(new HtmlElement("li").setAttribute("data-page", "prev").setId("next")
                .addChildTag(new Span().addChildTag(new Span().setStyleClass("sr-only"))));
        ul.addChildTag(new HtmlElement("li").setAttribute("data-page", "next").setId("prev")
                .addChildTag(new Span().addChildTag(new Span().setStyleClass("sr-only"))));

        nav.addChildTag(ul);
        divPag.addChildTag(nav);
        div.addChildTag(divPag);
    }

    @SafeVarargs
    private final void createResultInfos(DivLayout div, String result, String styleClass, List<String>... list){
        if(result.equals("Failed")){
            if(list[0].size() > 0) {
                div.addChildTag(new Button(null)
                        .setJsClick("FailuresViewCoffee.next", Collections.singletonMap("id", formatFailures(list[1])))
                        .setTextValue(result + ": " + list[0].size())
                        .setStyleClass(styleClass));
            } else {
                div.addChildTag(new Label(result + ": N/A").setStyleClass(styleClass));
            }
        } else
            div.addChildTag(new Label(result + ": " + (list[0].size() > 0 ? list[0].size() : "N/A")).setStyleClass(styleClass));
    }

    private String formatFailures(List<String> list){
        StringJoiner joiner = new StringJoiner("£");
        for(String err : list)
            joiner.add(err.replace("/","-"));
        return joiner.toString();
    }

    private void createUserBuildsChart(DivLayout div){
        div.addChildTag(new DivLayout().setId("buildsChart"));
        div.addChildTag(new Widget()
                .setRenderer("BuildsChartWidgetRenderer.renderGraph"));
    }
}