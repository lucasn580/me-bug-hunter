package meceap.appbuilder.view.automation;

import br.com.me.appbuilder.PageObjects;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.model.ClienteModel;
import meceap.appbuilder.model.TestCaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoOldView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<ClienteModel> clientes;

    public AutomacaoOldView(final List<ClienteModel> clientes, UserPrincipal principal) {
        this.principal = principal;
        this.clientes = clientes;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a lista de casos de teste automatizados...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");
        DivLayout div1 = new DivLayout().setStyleClass("dd");

        mainDiv.setId("customersBoard");
        createPageTitle(mainDiv);
        createHeaderButtons(mainDiv);
        createCustomerCard(div1);

        mainDiv.addChildTag(div1);
    }

    private void createPageTitle(DivLayout div) {
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.automation")))));
    }

    private void createHeaderButtons(DivLayout div) {
        div.addChildTag(new DivLayout().setStyleClass("div-buttons")
                .addChildTag(new Label(UserCatalog.getMessage("label.ambiente")))
                .addChildTag(new SelectInput()
                        .setOptionValue("nome")
                        .setOptionText("nome")
                        .setOptions(AutomacaoControllerURL.getActivesEnvironments(principal.getCustomRoot()))
                        .setId("environment"))
                .addChildTag(new Button()
                        .setJsClick("EnvironmentViewCoffee.show").setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-ballot-check")))
                .addChildTag(new Button().setUrlAction(AutomacaoControllerURL.customBuilder(principal.getCustomRoot()))
                        .setId("btnConstruir").setTextValue(UserCatalog.getMessage("button.contruir")))
                .addChildTag(new DivLayout().setStyleClass("separator-line"))
                .addChildTag(new Button().setJsClick("ExecutionsViewCoffee.next", Collections.singletonMap("type", "testCase"))
                        .setId("btnVerExecucoes").setTextValue(UserCatalog.getMessage("button.ver.execucoes")))
                .addChildTag(new Button().setJsClick("RunTestViewCoffee.performAllCts")
                        .setId("btnExecutarTudo").setTextValue(UserCatalog.getMessage("button.executar.tudo")))
                .addChildTag(new Button().setJsClick("RunTestViewCoffee.onlyChecked")
                        .setId("btnExecutar").setTextValue(UserCatalog.getMessage("button.executar"))));
    }

    private void createCustomerCard(DivLayout div) {
        for (ClienteModel cli : clientes) {

            HtmlElement ol = new HtmlElement("ol").setStyleClass("kanban top-bar");
            div.addChildTag(ol
                    .addChildTag(new DivLayout()
                            .addChildTag(new Heading(Heading.HeadingType.h2)
                                    .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-user-cog"))
                                    .addChildTag(new Span(cli.getCliente())))));

            createCustomerCardItems(cli, ol);

            ol.addChildTag(new DivLayout().setStyleClass("actions")
                    .addChildTag(new Button(null).setUrlAction(String.format("/%s/document/CasodeTeste/create", principal.getCustomRoot().getName()))
                            .setStyleClass("addbutt").setTextValue(UserCatalog.getMessage("button.add.new"))));
        }
    }

    private void createCustomerCardItems(ClienteModel cli, HtmlElement ol) {

        for (TestCaseModel tc : cli.getCasosDeTeste()) {

            HtmlElement li = new HtmlElement("li").setStyleClass("dd-item");

            ol.addChildTag(li
                    .addChildTag(new Heading(Heading.HeadingType.h3).setStyleClass("title")
                            .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-clone dd-handle"))
                            .addChildTag(new Span(String.format("%s - %s", tc.getId(), tc.getFluxo())))
                            .addChildTag(new CheckBox().setId(tc.getId()))));

            List<String> telasMapeadas = new ArrayList<>();

            for (PageObjects po : tc.getPageObjects()) {
                String pagina = po.getPagina();
                if (!telasMapeadas.contains(pagina))
                    telasMapeadas.add(pagina);
            }
            li.addChildTag(new DivLayout().setStyleClass("text").setAttribute("contenteditable", "false")
                    .setTextValue(telasMapeadas.size() + (telasMapeadas.size() > 1 ? " páginas mapeadas." : " página mapeada.")));

            li.addChildTag(new DivLayout().setStyleClass("text").setAttribute("contenteditable", "false")
                    .setTextValue(tc.getPageObjects().size() + (tc.getPageObjects().size() > 1 ? " ações criadas." : " ação criada.")));

            createCustomerActionButtons(li, tc);
        }
    }

    private void createCustomerActionButtons(HtmlElement li, TestCaseModel tc) {

        li.addChildTag(new DivLayout().setStyleClass("actions")
                .addChildTag(new Button(null)
                        .setJsClick("RunTestViewCoffee.unitTest", Collections.singletonMap("id", tc.getId()))
                        .setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-caret-circle-right")))

                .addChildTag(new Button(null)
                        .setUrlAction(String.format("/%s/document/CasodeTeste/" + tc.getId(), principal.getCustomRoot().getName()))
                        .setStyleClass("btn-icon").setAttribute("target", "_blank")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-edit")))

                .addChildTag(new Button(null).setStyleClass("btn-icon")
                        .setAttribute("onclick", "window.open('https://tcqa.me.com.br/viewType.html?buildTypeId=MeWebClientes_" + tc.getCliente() + "_" + tc.getCliente() + "')")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-ballot-check")))

                .addChildTag(new Button(null)
                        .setJsClick("InformationsFlowViewCoffee.testCase", Collections.singletonMap("id", tc.getId()))
                        .setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-info"))));
    }
}