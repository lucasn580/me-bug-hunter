package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractComponentView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.model.EnvironmentCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lucasns
 * @since #1.0
 */
public class EnvironmentView extends AbstractComponentView {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final UserPrincipal principal;
    private final EnvironmentCommand ambientes;
    private HttpServletRequest request;

    public EnvironmentView(UserPrincipal principal, EnvironmentCommand ambientes, HttpServletRequest request) {
        this.principal = principal;
        this.ambientes = ambientes;
        this.request = request;
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    protected HtmlElement getComponent() {
        Form form = new Form(Form.FormOrientation.HORIZONTAL);
        form.setModel(ambientes);
        form.setAction(AutomacaoControllerURL.registerEnvironmentPOST(principal.getCustomRoot()));
        form.setUseDefaultSubmitButton(false);
        form.setDisableSubmitAfterClick(true);
        form.setId("formEnvironments");
        form.setAutoFields(false);
        form.addChildTag(
                createFields()
        );

        return new DivLayout().addChildTag(
                form,
                createGrid()
        );
    }

    private DivLayout createFields() {
        DivLayout divLayout = new DivLayout();
        divLayout.addChildTag(new DivLayout(DivLayout.DivStyle.Row).addChildTag(
                new DivLayout(DivLayout.DivStyle.Col_lg_5).addChildTag(
                        new Label(UserCatalog.getMessage("environment.name")),
                        new Paragraph().addChildTag(
                                new TextInput()
                                        .setModel(ambientes)
                                        .setFieldName("name")
                        )
                ),
                new DivLayout(DivLayout.DivStyle.Col_lg_5).addChildTag(
                        new Label(UserCatalog.getMessage("environment.active")),
                        new Paragraph().addChildTag(
                                new CheckBox()
                                        .setModel(ambientes)
                                        .setFieldName("active")
                        )
                ),
                new DivLayout(DivLayout.DivStyle.Col_lg_2).addChildTag(
                        new HrefLinkAnchor().addChildTag(
                                new HtmlElement("i")
                                        .setStyleClass("me-icon icon-plus-circle btn-icon-circle")
                                        .bindEvent("EnvironmentViewCoffee.addNewEnv", "click")
                        )

                )
                ).mergeAttribute("style", "margin-bottom:20px")
        );

        return divLayout;
    }

    private MEGrid createGrid() {
        MEGrid grid = new MEGrid("grid");
        grid.setSerializedData(request, ambientes.getEnvironments());
        grid.setColumnChooser(false);
        grid.setShowExportButton(false);
        grid.setTableStyle(MEGrid.GridTableStyle.Striped, MEGrid.GridTableStyle.Hover);
        grid.addColumns(
                MEGrid.column("name").setTitle(UserCatalog.getMessage("environment.name")).setDataField("nome"),
                MEGrid.column("active").setTitle(UserCatalog.getMessage("environment.active")).setDataField("ativo").setDataFieldType("boolean"),
                MEGrid.column("name").setDataField("nome").setFnRender("EnvironmentViewCoffee.renderRemoveEnvironmentColumn")
        );

        return grid;
    }
}