package meceap.appbuilder.view.automation;

import br.com.me.appbuilder.document.casodeTeste.CasodeTeste;
import br.com.me.appbuilder.document.casodeTeste.PageObjectsItemInCasodeTeste;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.HtmlElement;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.ui.html.Span;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoKanbanCardView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final CasodeTeste casodeTeste;

    public AutomacaoKanbanCardView(UserPrincipal principal, CasodeTeste casodeTeste) {
        this.principal = principal;
        this.casodeTeste = casodeTeste;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view com informações do fluxo automatizado do cliente...");

        String descricao = StringUtils.retirarTagsHtml(casodeTeste.getCasodeTeste().getDescricao());
        String preCondicao = StringUtils.retirarTagsHtml(casodeTeste.getCasodeTeste().getPrecondicao());

        mainDiv.addChildTag(new Label(UserCatalog.getMessage("testcase.field.goal")));
        mainDiv.addChildTag(new DivLayout().addChildTag(new Span(descricao)));
        mainDiv.addChildTag(new HtmlElement("br"));
        mainDiv.addChildTag(new Label(UserCatalog.getMessage("testcase.field.precondition")));
        mainDiv.addChildTag(new DivLayout().addChildTag(new Span(preCondicao)));
        mainDiv.addChildTag(new HtmlElement("br"));
        mainDiv.addChildTag(new Label(UserCatalog.getMessage("testcase.field.automated.page")));

        List<String> telasMapeadas = new ArrayList<>();

        for (PageObjectsItemInCasodeTeste po : casodeTeste.getCasodeTeste().getPageObjectsList()) {
            String pagina = po.getPagina();
            if(!telasMapeadas.contains(pagina)){
                telasMapeadas.add(pagina);
                mainDiv.addChildTag(new DivLayout().addChildTag(new Span(pagina)));
            }
        }
    }
}