package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.MEGrid;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class ExecutionsModalView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private final UserPrincipal principal;
    private final String type;

    public ExecutionsModalView(UserPrincipal principal, String type) {
        this.principal = principal;
        this.type = type;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a lista de builds...");

        mainDiv.addChildTag(new MEGrid("gridBuilds")
                .setTableStyle(MEGrid.GridTableStyle.Bordered, MEGrid.GridTableStyle.Striped)
                .setBaseURLData(AutomacaoControllerURL.getBuilds(principal.getCustomRoot(), type))
                .addColumns(
                        MEGrid.column("id")
                                .setTitle(UserCatalog.getMessage("execution.field.id"))
                                .setDataField("id"),
                        MEGrid.column("date")
                                .setTitle(UserCatalog.getMessage("execution.field.date"))
                                .setDataField("date")
                                .setDataFieldType("datetime"),
                        MEGrid.column("user")
                                .setTitle(UserCatalog.getMessage("execution.field.user"))
                                .setDataField("user"),
                        MEGrid.column("ids")
                                .setTitle(UserCatalog.getMessage("execution.field.ids"))
                                .setDataField("ids"),
                        MEGrid.column("result")
                                .setTitle(UserCatalog.getMessage("execution.field.result"))
                                .setDataField("resultCommand.result"),
                        MEGrid.column("report")
                                .setTitle(UserCatalog.getMessage("execution.field.report"))
                                .setFnRender("GridRenderers.renderLink")
                                .setDataField("report")
                ));
    }
}