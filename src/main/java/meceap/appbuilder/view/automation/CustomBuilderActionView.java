package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.Input;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class CustomBuilderActionView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public CustomBuilderActionView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando modal de edição da ação...");

        mainDiv.addChildTag(new DivLayout().setAttribute("style", "margin:20px")
                .addChildTag(new Label("Elemento:"))
                .addChildTag(new Input().setStyleClass("form-control")));

        mainDiv.addChildTag(new DivLayout().setAttribute("style", "margin:20px")
                .addChildTag(new Label("Texto:"))
                .addChildTag(new Input().setStyleClass("form-control")));

        mainDiv.addChildTag(new DivLayout().setAttribute("style", "margin:20px")
                .addChildTag(new Button().setButtonStyle(Button.ButtonStyle.Primary).setTextValue("Ok")));
    }
}