package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.ui.html.ListItem;
import br.com.me.ceap.web.ui.html.UnorderedList;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.ClienteModel;
import meceap.appbuilder.model.TestCaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class ScenariosRunnedView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<ClienteModel> clientes;

    public ScenariosRunnedView(UserPrincipal principal, List<ClienteModel> clientes) {
        this.principal = principal;
        this.clientes = clientes;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view com cenários executados dos clientes...");

        for(ClienteModel cli : clientes){
            mainDiv.addChildTag(new Label(cli.getCliente()));

            UnorderedList list = new UnorderedList();
            for(TestCaseModel test : cli.getCasosDeTeste()){
                list.addItem(new ListItem(test.getNomeCenario()));
            }
            mainDiv.addChildTag(list);
        }
    }
}