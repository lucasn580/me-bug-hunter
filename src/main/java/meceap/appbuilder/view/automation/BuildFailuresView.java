package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.Span;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class BuildFailuresView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final String failures;

    public BuildFailuresView(UserPrincipal principal, String failures) {
        this.principal = principal;
        this.failures = failures;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando modal view de falhas do build selecionado...");

        String[] falhas = failures.split("£");

        for(String st : falhas){
            String[] falha = st.split("-");

            DivLayout div = new DivLayout().setAttribute("style", "margin:20px; display: flex;");
            DivLayout div1 = new DivLayout().setAttribute("style", "width: 80%");
            div1.addChildTag(new Span("Cenário: " + falha[0] + " - " + falha[1]));
            div.addChildTag(div1);

            div.addChildTag(new Button()
//                .setJsClick("")
                    .setButtonStyle(Button.ButtonStyle.Danger)
                    .setStyleClass("newIssue")
                    .setTextValue(UserCatalog.getMessage("button.abrir.miisy")));


            mainDiv.addChildTag(div);
        }
    }
}