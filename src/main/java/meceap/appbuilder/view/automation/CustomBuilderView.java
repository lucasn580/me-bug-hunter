package meceap.appbuilder.view.automation;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class CustomBuilderView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public CustomBuilderView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a tela de construção de CasodeTeste...");

        mainDiv.setStyleClass("bckgdDefault");
        mainDiv.setId("builder");
        createPageTitle(mainDiv);

        DivLayout wrapper = new DivLayout().setId("wrapper");

        createActionOptions(wrapper);
        createBuilderFrame(wrapper);

        mainDiv.addChildTag(wrapper);
    }

    private void createPageTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span("Em desenvolvimento..."))));
    }

    private void createActionOptions(DivLayout div){
        div.addChildTag(new DivLayout().setId("options")
                .addChildTag(
                        createActionOption("drag1", "Clicar"),
                        createActionOption("drag2", "Preencher"),
                        createActionOption("drag3", "Iframe"),
                        createActionOption("drag4", "Popup"),
                        createActionOption("drag5", "Comparar"),
                        createActionOption("drag6", "Limpar")
                ));
    }

    private DivLayout createActionOption(String id, String value){
        return new DivLayout().setId(id).setStyleClass("drag")
                .addChildTag(new Span(value))
                .addChildTag(new Button(null)
                        .setJsClick("EditBuilderActionViewCoffee.next")
                        .setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-edit")));
    }

    private void createBuilderFrame(DivLayout div){
        div.addChildTag(new DivLayout().setId("frame")
                .addChildTag(new Heading(Heading.HeadingType.h3).setTextValue("Crie seu fluxo personalizado!")));
    }
}