package meceap.appbuilder.view.automation;

import br.com.me.appbuilder.document.casodeTeste.CasodeTeste;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.HtmlElement;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.ExecutionCommand;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * @author lucasns
 * @since #1.0
 */
public class AutomacaoKanbanRunTestView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final CasodeTeste casodeTeste;
    private final ExecutionCommand command;

    public AutomacaoKanbanRunTestView(UserPrincipal principal, ExecutionCommand command, CasodeTeste casodeTeste) {
        this.principal = principal;
        this.command = command;
        this.casodeTeste = casodeTeste;
    }

    public AutomacaoKanbanRunTestView(UserPrincipal principal, ExecutionCommand command) {
        this.principal = principal;
        this.command = command;
        this.casodeTeste = null;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view de execução do fluxo automatizado do cliente...");

        if(casodeTeste != null){
            mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.flow")));
            mainDiv.addChildTag(new HtmlElement("br"));
            mainDiv.addChildTag(new DivLayout().addChildTag(new Label(casodeTeste.getCasodeTeste().getCliente().getNome())));
            mainDiv.addChildTag(new DivLayout().addChildTag(new Label("ID do caso de teste: " + casodeTeste.getId())));
            mainDiv.addChildTag(new DivLayout().addChildTag(new Label("Fluxo: " + casodeTeste.getCasodeTeste().getFluxo().getNome())));
            addFooter(mainDiv);

        } else if (command != null && !command.getIds().equals("vazio")) {
                mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.flows")));
                mainDiv.addChildTag(new HtmlElement("br"));
                mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.ids") + StringUtils.formatarComEspacos(command.getIds())));
                addFooter(mainDiv);
        } else
            mainDiv.addChildTag(new Label(UserCatalog.getMessage("label.execution.warn")));
    }

    private void addFooter(DivLayout div) {
        div.addChildTag(new HtmlElement("br"));
        div.addChildTag(new HtmlElement("br"));
        div.addChildTag(new Button().setJsClick("StartBuildViewCoffee.next", Collections.singletonMap("obj", command))
                .setDisableAfterClick(true)
                .setButtonStyle(Button.ButtonStyle.Warning)
                .setTextValue(UserCatalog.getMessage("button.executar")));
    }
}