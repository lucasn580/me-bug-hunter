package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.external.DashboardCommand;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lucasns
 * @since #1.0
 */
public class DashboardView extends AbstractTitledPageView {

    private final UserPrincipal principal;
    private final DashboardCommand command;

    public DashboardView(UserPrincipal principal, DashboardCommand command) {
        this.principal = principal;
        this.command = command;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(DivLayout div) {
        createTitle(div);
        addDashboard(div);
    }

    private void createTitle(DivLayout div) {
        div.addChildTag(new DivLayout()
                .addChildTag(new ImageInput().setImageURL("/bughunter/resource/?name=resources/img/logo-me.png")
                        .setStyleClass("small-logo-me"))
                .addChildTag(new Heading(Heading.HeadingType.h2).setStyleClass("dashTitle")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-chart-bar"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.dashboard")))));
    }

    private void addDashboard(DivLayout div) {
        div.setStyleClass("bckgdDashboard");
        DivLayout divDash = new DivLayout().setStyleClass("divDashboard");
        DivLayout divLeft = new DivLayout().setStyleClass("divDashLeft");
        DivLayout divMid = new DivLayout().setStyleClass("divDashGraphics");
        DivLayout divRig = new DivLayout().setStyleClass("divDashRight");

        // =================================== Indicadores Esquerda ===================================
        String urlIssues = command.getUrlIssueFilterBase();
        divLeft.addChildTag(new DivLayout().setStyleClass("divPartition")
                .addChildTag(new DivLayout().setStyleClass("items").addChildTag(
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.open"),urlIssues + "4453", command.getOpenIssues().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.inprogress"),urlIssues + "4474", command.getInProgressIssues().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.developing"),urlIssues + "4471", command.getDevelopingIssues().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.testing"),urlIssues + "4465", command.getTestingIssues().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.waiting.build"),urlIssues + "4462", command.getWaitingBuildProdIssues().toString())
                )));

        String urlBuilds = command.getUrlBuildFilterBase();
        divLeft.addChildTag(new DivLayout().setStyleClass("divPartition")
                .addChildTag(new DivLayout().setStyleClass("items").addChildTag(
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.open"),urlBuilds + "4453", command.getOpenBuilds().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.inprogress"),urlBuilds + "4474", command.getInProgressBuilds().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.developing"),urlBuilds + "4471", command.getDevelopingBuilds().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.testing"),urlBuilds + "4465", command.getTestingBuilds().toString()),
                        createIndicatorDiv(UserCatalog.getMessage("label.dashboard.waiting.build"),urlBuilds + "4462", command.getWaitingBuildProdBuilds().toString())
                )));

        // =================================== Gráficos ===================================
        divMid.addChildTag(renderColumnGraphic(new DivLayout().setId("chamadosValidados"), "passedIssues", "Chamados validados no dia"));
        divMid.addChildTag(renderColumnGraphic(new DivLayout().setId("qtdRetornos"), "returnedIssues", "Chamados retornados no dia"));
        divMid.addChildTag(renderColumnGraphic(new DivLayout().setId("buildTotal"), "buildIssues", "Build Pontual"));
        divMid.addChildTag(renderColumnGraphic(new DivLayout().setId("buildImpactTotal"), "buildImpactIssues", "Build Pontual"));

        // =================================== Indicadores Direita ===================================
        divRig.addChildTag(new DivLayout().setStyleClass("divPartition")
                .addChildTag(new DivLayout().setStyleClass("items")
                        .addChildTag(new Label(UserCatalog.getMessage("label.dashboard.release")).setStyleClass("lblRelease"))
                        .addChildTag(new DivLayout().setStyleClass("indicatorSmall dashBorder")
                                .addChildTag(new Label(command.getCurrentRelease()).setStyleClass("lblIndicator b-amber")))
                        .addChildTag(new DivLayout().setStyleClass("graphicSmall")
                                .addChildTag(renderSolidGraphic(new DivLayout().setId("solidRelease"))))
                        .addChildTag(new Label(UserCatalog.getMessage("label.dashboard.therm.quality")))
                        .addChildTag(new DivLayout().setStyleClass("indicator dashBorder")
                                .addChildTag(addQualityIcon(command.getAllReleaseIssues().size(), command.getReturnedReleaseIssues())))));

        divRig.addChildTag(new DivLayout().setStyleClass("divPartition")
                .addChildTag(new DivLayout().setStyleClass("items")
                        .addChildTag(new Label(UserCatalog.getMessage("label.dashboard.total")))
                        .addChildTag(new DivLayout().setStyleClass("indicatorSmall dashBorder")
                                .addChildTag(new Label(String.valueOf(command.getAllBuildsIssues().size())).setStyleClass("lblIndicator")))
                        .addChildTag(new Label(UserCatalog.getMessage("label.dashboard.total.impact")))
                        .addChildTag(new DivLayout().setStyleClass("indicatorSmall dashBorder")
                                .addChildTag(new Label(command.getImpactsBuildsIssues().toString()).setStyleClass("lblIndicator")))
                        .addChildTag(new Label(UserCatalog.getMessage("label.dashboard.therm.builds")))
                        .addChildTag(new DivLayout().setStyleClass("indicator dashBorder")
                                .addChildTag(addQualityIcon(command.getAllReleaseIssues().size(), command.getReturnedReleaseIssues())))));

        divDash.addChildTag(divLeft);
        divDash.addChildTag(divMid);
        divDash.addChildTag(divRig);
        div.addChildTag(divDash);
    }

    private DivLayout createIndicatorDiv(String label, String url, String value){
        return new DivLayout()
                .addChildTag(new Label(label))
                .addChildTag(new HrefLinkAnchor(url)
                        .setTarget(HrefLinkAnchor.Target.BLANK)
                        .setStyleClass("lblAmount")
                        .setTextValue(value));
    }

    private Icon addQualityIcon(int total, int section) {
        Icon icon = new Icon();

        long average = Math.round(section * 100.0 / total);
        icon.setAttribute("title", String.format("Percentual: %s", average));
        if (average <= 2)
            icon.setStyleClass("iconTherm b-green me-icon icon-thermometer-full");
        else if (average <= 4)
            icon.setStyleClass("iconTherm b-orange me-icon icon-thermometer-half");
        else
            icon.setStyleClass("iconTherm b-red me-icon icon-thermometer-empty");
        return icon;
    }

    private DivLayout renderColumnGraphic(DivLayout div, String type, String title) {
        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("title", title);
        params.put("divId", div.getId());
        div.setStyleClass("graphics");
        div.addChildTag(new Widget()
                .setRenderer("DashboardColumnChartWidgetRenderer")
                .setData(params));
        return div;
    }

    private DivLayout renderSolidGraphic(DivLayout div) {
        div.setStyleClass("graphics");
        div.addChildTag(new Widget()
                .setRenderer("DashboardSolidChartWidgetRenderer"));
        return div;
    }
}