package meceap.appbuilder.view.management;

import br.com.me.appbuilder.Tarefa;
import br.com.me.ceap.dynamic.extension.model.base.UserCustomRoot;
import br.com.me.ceap.web.ui.html.Toolbar;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import meceap.appbuilder.model.TaskCommand;

/**
 * @author carloso
 * @since 2.4
 */
public class EditTaskPageView extends AbstractTasksView {
// ------------------------------ FIELDS ------------------------------

    private final Tarefa tarefa;

// --------------------------- CONSTRUCTORS ---------------------------

    public EditTaskPageView(UserCustomRoot customRoot, TaskCommand command, Tarefa tarefa) {
        super(customRoot, command);
        this.tarefa = tarefa;
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    protected String getFormActionUrl() {
        return ManagementControllerURL.editTaskPOST(customRoot, tarefa);
    }

    @Override
    protected void setToolbar(Toolbar toolbar) {

    }
}