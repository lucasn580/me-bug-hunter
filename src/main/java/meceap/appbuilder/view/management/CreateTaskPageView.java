package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.UserCustomRoot;
import br.com.me.ceap.web.ui.html.Toolbar;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import meceap.appbuilder.model.TaskCommand;

/**
 * @author lucasns
 * @since #1.0
 */
public class CreateTaskPageView extends AbstractTasksView {
// --------------------------- CONSTRUCTORS ---------------------------

    public CreateTaskPageView(UserCustomRoot customRoot, TaskCommand command) {
        super(customRoot, command);
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    protected String getFormActionUrl() {
        return ManagementControllerURL.createTask(customRoot);
    }

    @Override
    protected void setToolbar(Toolbar toolbar) {

    }
}