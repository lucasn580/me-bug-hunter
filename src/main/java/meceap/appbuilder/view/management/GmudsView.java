package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.AutomacaoControllerURL;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import meceap.appbuilder.model.GmudMensalModel;
import meceap.appbuilder.model.external.misytracker.DocumentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.Collections;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class GmudsView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<GmudMensalModel> gmuds;

    public GmudsView(UserPrincipal principal, List<GmudMensalModel> gmuds) {
        this.principal = principal;
        this.gmuds = gmuds;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações de Gmuds...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        createTitle(mainDiv);
        createSelectYear(mainDiv);

        HtmlElement table = new HtmlElement("table").setId("customgrid");
        createTableHeader(table);
        populateTable(table);
        mainDiv.addChildTag(table);

        createCharts(mainDiv);
    }

    private void createTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.gmuds")))
                        .addChildTag(new Button(null)
                                .setUrlAction(String.format("/%s/search/DocumentoGmud", principal.getCustomRoot().getName()))
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setStyleClass("btnActionTop btnActionTopLeft")
                                .setTextValue(UserCatalog.getMessage("gmuds.button.comite")))
                        .addChildTag(new Button(null)
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setStyleClass("btnActionTop btnActionTopRight")
                                .setAttribute("onclick", "window.open('https://issues.miisy.com/Misy/search/filter?dt=1312&favorite=false&followed=false&archived=false&publishFilter=false&analytics=false&system=false&filter=26808697')")
                                .setTextValue(UserCatalog.getMessage("gmuds.button.approve")))));
    }

    private void createSelectYear(DivLayout div){
        SelectInput yearSelect = new SelectInput();
        if(gmuds != null && !gmuds.isEmpty())
            yearSelect.setPlaceHolderText(gmuds.get(0).getAno());

        div.addChildTag(new DivLayout().setStyleClass("divYear")
                .addChildTag(new Span(UserCatalog.getMessage("gmuds.select.year")).setStyleClass("lblYear"))
                .addChildTag(yearSelect
                        .setOptions(ManagementControllerURL.getLatestYears(principal.getCustomRoot()))
                        .setId("years")
                        .bindEvent("GmudsLinkCoffee.changeYear", "change")));
    }

    private void createTableHeader(HtmlElement table){
        HtmlElement thead = new HtmlElement("thead");
        HtmlElement tr = new HtmlElement("tr");
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.month")));
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.qtd.issues")));
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.qtd.emerg")));
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.qtd.planned")));
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.link")));
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2").setTextValue(UserCatalog.getMessage("gmuds.table.rootCause")));
        tr.addChildTag(new HtmlElement("th").setAttribute("colspan", "4").setTextValue(UserCatalog.getMessage("gmuds.table.status")));

        HtmlElement tr1 = new HtmlElement("tr");
        tr1.addChildTag(new HtmlElement("th").setTextValue(UserCatalog.getMessage("gmuds.table.status.open")));
        tr1.addChildTag(new HtmlElement("th").setTextValue(UserCatalog.getMessage("gmuds.table.status.closed")));
        tr1.addChildTag(new HtmlElement("th").setTextValue(UserCatalog.getMessage("gmuds.table.status.testingProd")));
        tr1.addChildTag(new HtmlElement("th").setTextValue(UserCatalog.getMessage("gmuds.table.status.testing")));

        thead.addChildTag(tr);
        thead.addChildTag(tr1);
        table.addChildTag(thead);
    }

    private void populateTable(HtmlElement table){
        for(GmudMensalModel gmud : gmuds){
            HtmlElement line = new HtmlElement("tr");

            line.addChildTag(new HtmlElement("td")
                    .addChildTag(new Button(null)
                            .setJsClick("GmudsLinkCoffee.monthColumn", Collections.singletonMap("month", gmud.getMes()))
                            .setStyleClass("btnLink")
                            .setTextValue(gmud.getMes())));

            addCellToLine(line, String.valueOf(gmud.getIssues().getTotalHits()));
            addCellToLine(line, gmud.getEmergenciais().toString());
            addCellToLine(line, gmud.getPlanejados().toString());

            line.addChildTag(new HtmlElement("td")
                    .addChildTag(new Button(null)
                            .setJsClick("GmudsLinkCoffee.linkColumn", Collections.singletonMap("month", gmud.getMes()))
                            .setStyleClass("btn-icon")
                            .addChildTag(new Icon().setStyleClass("me-icon icon-search icon-check"))));

            List<DocumentModel> docs = gmud.getIssues().getDocument();
            LocalDate date = (!docs.isEmpty() ? docs.get(0).getCreated() : LocalDate.now());
            String[] dates = getStringDate(date);

            line.addChildTag(new HtmlElement("td")
                    .addChildTag(new Button().setTextValue("Causa")
                            .setAttribute("onclick", String.format("window.open('https://issues.miisy.com/Misy/search/filter?filterTargetId=IssueDoc&tag=26858996" +
                                    "&items[0].fieldName=created&items[0].operator=greaterThanEqual&items[0].value=%1$s" +
                                    "&items[1].fieldName=created&items[1].operator=lesserThanEqual&items[1].value=%2$s" +
                                    "&items[2].fieldName=header.department&items[2].operator=is&items[2].value=2768')", dates[0], dates[1]))));

            addCellToLine(line, String.valueOf(gmud.getQtdPorStatus("aberto")));
            addCellToLine(line, String.valueOf(gmud.getQtdPorStatus("fechado")));
            addCellToLine(line, String.valueOf(gmud.getQtdPorStatus("em testes em produção")));
            addCellToLine(line, String.valueOf(gmud.getQtdPorStatus("em testes")));

            table.addChildTag(line);
        }
    }

    private void addCellToLine(HtmlElement line, String text) {
        line.addChildTag(new HtmlElement("td")
                .addChildTag(new Span(text)));
    }

    private void createCharts(DivLayout div){
        DivLayout divCharts = new DivLayout().setAttribute("style", "display: flex;");
        divCharts.addChildTag(new DivLayout().setId("gmudpiechart"));
        divCharts.addChildTag(new Widget()
                .setRenderer("GmudTypeChartWidgetRenderer.renderGraph"));

        divCharts.addChildTag(new DivLayout().setId("gmudbarchart"));
        divCharts.addChildTag(new Widget()
                .setRenderer("GmudBarChartWidgetRenderer.renderGraph"));

        div.addChildTag(divCharts);
    }

    private String[] getStringDate(LocalDate date) {

        String[] dates = new String[2];

        YearMonth month = YearMonth.from(date);
        LocalDate start = month.atDay(1);
        LocalDate end   = month.atEndOfMonth();

        dates[0] = start.toString();
        dates[1] = end.toString();
        return dates;
    }
}