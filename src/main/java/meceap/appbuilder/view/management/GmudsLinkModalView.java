package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.GmudMensalModel;
import meceap.appbuilder.model.external.misytracker.DocumentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class GmudsLinkModalView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<GmudMensalModel> gmuds;
    private final String mes;
    private String urlFiltroMisy = "https://issues.miisy.com/Misy/search/filter?favorite=false&followed=false&archived=false&publishFilter=false&analytics=false&system=false&_=undefined&filterTargetId=IssueDoc&filterTargetType=Document&items%5B0%5D.fieldName=id&items%5B0%5D.operator=in&items%5B0%5D.value=";

    public GmudsLinkModalView(UserPrincipal principal, final List<GmudMensalModel> gmuds, String mes) {
        this.principal = principal;
        this.gmuds = gmuds;
        this.mes = mes;
    }


    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando view de Gmuds emergenciais...");

        DivLayout div = new DivLayout();
        mainDiv.addChildTag(div);
        mainDiv.addChildTag(new HtmlElement("br"));

        HrefLinkAnchor iconMisy = new HrefLinkAnchor().setTarget(HrefLinkAnchor.Target.BLANK).setStyleClass("melink");
        div.addChildTag(new Heading(Heading.HeadingType.h2)
                .addChildTag(new Span(mes + ": "))
                .addChildTag(iconMisy
                        .addChildTag(new Icon().setStyleClass("me-icon icon-misy icon-check"))));
        div.addChildTag(new HtmlElement("br"));

        addMarkerLabel(div);

        for(GmudMensalModel gmud : gmuds){
            for(DocumentModel doc : gmud.getIssues().getDocument()){

                Label issueNumber = new Label(doc.getId()+": ");
                String categoria = doc.getHeader().getCategory().getDescription();

                if(categoria.equals("Mudança Emergencial"))
                    issueNumber.setStyleClass("lblEmerg");

                div.addChildTag(new HrefLinkAnchor()
                        .setHref("https://issues.miisy.com/Misy/do/issue/show/"+doc.getId())
                        .setTarget(HrefLinkAnchor.Target.BLANK)
                        .addChildTag(issueNumber));
                div.addChildTag(new Span(doc.getHeader().getSummary()));
                div.addChildTag(new HtmlElement("br"));

                urlFiltroMisy += (doc.getId() + "%2C");
            }
        }
        iconMisy.setHref(urlFiltroMisy);
    }

    private void addMarkerLabel(DivLayout div){
        DivLayout marker = new DivLayout().setId("marker");
        marker.addChildTag(new Label("Legenda: "));
        marker.addChildTag(new Span("❑").setStyleClass("square-red"));
        marker.addChildTag(new Label(" Emergencial"));
        marker.addChildTag(new Span("❑").setStyleClass("square-black"));
        marker.addChildTag(new Label(" Planejada"));
        marker.addChildTag(new HtmlElement("br"));
        div.addChildTag(marker);
    }
}