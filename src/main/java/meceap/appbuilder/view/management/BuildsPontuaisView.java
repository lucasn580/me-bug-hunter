package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class BuildsPontuaisView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<String> productVersions;

    public BuildsPontuaisView(UserPrincipal principal, List<String> productVersions) {
        this.principal = principal;
        this.productVersions = productVersions;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações de Releases...");

        Label label = new Label();
        mainDiv.setId("metrics");
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        createTitle(mainDiv);
        createVersionsGrid(mainDiv);
        mainDiv.addChildTag(createGrid());
    }

    private void createTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.casual.builds")))
                        .addChildTag(new Button(null)
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setUrlAction(String.format("/%s/document/Buildspontuais/create", principal.getCustomRoot().getName()))
                                .setStyleClass("btnActionTop btnActionTopLeft")
                                .setTextValue("Novo Build Pontual"))));
    }

    private void createVersionsGrid(DivLayout div){
        DivLayout versionDiv = new DivLayout().setId("versionDiv");
        for(String version : productVersions)
            versionDiv.addChildTag(new DivLayout()
                    .addChildTag(new Span(version)));
        div.addChildTag(versionDiv);
    }

    private MEGrid createGrid(){
        return new MEGrid("customgrid")
                .setBaseURLData(ManagementControllerURL.getCasualBuilds(principal.getCustomRoot()))
                .addColumns(
                        MEGrid.column("release")
                                .setTitle(UserCatalog.getMessage("builds.field.release"))
                                .setDataField("buildsPontuais.release"),
//                                .setGroupHeader("Teste"),
                        MEGrid.column("ano")
                                .setTitle(UserCatalog.getMessage("builds.field.year"))
                                .setDataField("buildsPontuais.ano"),
                        MEGrid.column("mes")
                                .setTitle(UserCatalog.getMessage("builds.field.month"))
                                .setDataField("buildsPontuais.mes.value"),
                        MEGrid.column("qtdChamados")
                                .setTitle(UserCatalog.getMessage("builds.field.qtdIssues"))
                                .setDataField("buildsPontuais.qtdChamados"),
                        MEGrid.column("chamados")
                                .setTitle(UserCatalog.getMessage("builds.field.issues"))
                                .setDataField("buildsPontuais.chamados"),
                        MEGrid.column("build")
                                .setTitle(UserCatalog.getMessage("builds.field.build"))
                                .setDataField("buildsPontuais.build"),
                        MEGrid.column("nroMiisy")
                                .setTitle(UserCatalog.getMessage("builds.field.miisy"))
                                .setDataField("buildsPontuais.nroMiisy"),
                        MEGrid.column("dataProd")
                                .setTitle(UserCatalog.getMessage("builds.field.prodDate"))
                                .setDataField("buildsPontuais.dataProd")
                                .setDataFieldType("datetime"),
                        MEGrid.column("qtdImpactos")
                                .setTitle(UserCatalog.getMessage("builds.field.qtdImpacts"))
                                .setDataField("buildsPontuais.qtdImpactos"),
                        MEGrid.column("chamadoImpacto")
                                .setTitle(UserCatalog.getMessage("builds.field.impact.issue"))
                                .setDataField("buildsPontuais.chamadoImpacto"),
                        MEGrid.column("metaEquipe")
                                .setTitle(UserCatalog.getMessage("builds.field.goal"))
                                .setFnRender("GridRenderers.statusRenderer")
                                .setDataField("buildsPontuais.metaEquipe.colorHex"),
                        MEGrid.column("observacao")
                                .setTitle(UserCatalog.getMessage("builds.field.obs"))
                                .setDataField("buildsPontuais.observacao")
                );
    }
}