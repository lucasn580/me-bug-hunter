package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.MetricaMensalModel;
import meceap.appbuilder.model.external.metrics.DocumentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class MetricsView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<MetricaMensalModel> metrics;
    private String lastMonth;

    public MetricsView(UserPrincipal principal, List<MetricaMensalModel> metrics) {
        this.principal = principal;
        this.metrics = metrics;
        this.lastMonth = metrics.get(metrics.size()-1).getMes();
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações de Métricas...");

        Label label = new Label();
        mainDiv.setId("metrics");
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        createTitle(mainDiv);

        HtmlElement tabela = new HtmlElement("table").setId("customgrid");
        HtmlElement thead = new HtmlElement("thead");
        HtmlElement tr = new HtmlElement("tr");
        tr.addChildTag(new HtmlElement("th").setTextValue("Mês"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Não Conformidades em Produção"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Não Conformidades Detectadas nos Testes"));
        tr.addChildTag(new HtmlElement("th").setTextValue("% Testes Automatizados de Regressão com sucesso"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Índice de Reteste"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Qtde de Builds pontuais"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Métrica de Relacionamento"));

        thead.addChildTag(tr);
        tabela.addChildTag(thead);

        for(MetricaMensalModel metric : metrics){

            String NAO_CONFORM_PROD = getMetric(metric.getMetrics().getDocuments().get(0));
            String NAO_CONFORM_DETECT_TESTS = getMetric(metric.getMetrics().getDocuments().get(1));
            String TESTS_AUTO_REGR_SUC = getMetric(metric.getMetrics().getDocuments().get(2));
            String INDICE_RETESTE = getMetric(metric.getMetrics().getDocuments().get(3));
            String QTDE_BUILDS_PONT = getMetric(metric.getMetrics().getDocuments().get(4));
            String METRICA_RELACIO = getMetric(metric.getMetrics().getDocuments().get(5));

            HtmlElement linha = new HtmlElement("tr");

            addCellElement(linha, metric.getMes());
            addCellElement(linha, NAO_CONFORM_PROD);
            addCellElement(linha, NAO_CONFORM_DETECT_TESTS);
            addCellElement(linha, TESTS_AUTO_REGR_SUC);
            addCellElement(linha, INDICE_RETESTE);
            addCellElement(linha, QTDE_BUILDS_PONT);
            addCellElement(linha, METRICA_RELACIO);

            tabela.addChildTag(linha);
        }
        mainDiv.addChildTag(tabela);
    }

    private void createTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.metric")))
                        .addChildTag(new Button(null)
                                .setJsClick("RegisterMetricsViewCoffee.next", Collections.singletonMap("month", lastMonth))
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setStyleClass("btnActionTop btnActionTopRight")
                                .setTextValue("Registrar métrica"))));
    }

    private void addCellElement(HtmlElement linha, String metric){
        linha.addChildTag(new HtmlElement("td")
                .addChildTag(new Span(metric)));
    }

    private String getMetric(DocumentModel doc){
        if(doc.getResultadoList() != null)
            return String.valueOf(doc.getResultadoList().get(0).getResultadoMetrica());
        else return "Não registrado";
    }
}