package meceap.appbuilder.view.management;

import br.com.me.appbuilder.Tarefa;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class KanbanView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<List<Tarefa>> tarefas;

    public KanbanView(final List<List<Tarefa>> tarefas, UserPrincipal principal) {
        this.principal = principal;
        this.tarefas = tarefas;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar a lista de tarefas...");

        Label label = new Label();
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        mainDiv.setId("customersBoard");
        createTitle(mainDiv);

        DivLayout div1 = new DivLayout().setStyleClass("dd");
        createCustomerCard(div1);

        mainDiv.addChildTag(div1);
    }

    private void createTitle(DivLayout div){
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.kanban")))
                        .addChildTag(new Button(null)
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setJsClick("KanbanTaskCoffee.create")
                                .setStyleClass("btnActionTop btnActionTopRight")
                                .setTextValue(UserCatalog.getMessage("button.kanban.new")))));
    }

    private void createCustomerCard(DivLayout div) {
        for (List<Tarefa> userTasks : tarefas) {

            HtmlElement ol = new HtmlElement("ol").setStyleClass("kanban top-bar");
            div.addChildTag(ol
                    .addChildTag(new DivLayout()
                            .addChildTag(new Heading(Heading.HeadingType.h2)
                                    .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-user-check"))
                                    .addChildTag(new Span(userTasks.get(0).getUsuario().getName())))));

            createCustomerCardItems(userTasks, ol);

            ol.addChildTag(new DivLayout().setStyleClass("actions")
                    .addChildTag(new Button(null)
                            .setJsClick("KanbanTaskCoffee.create")
                            .setStyleClass("addbutt")
                            .setTextValue(UserCatalog.getMessage("button.add.new"))));
        }
    }

    private void createCustomerCardItems(List<Tarefa> tasks, HtmlElement ol) {

        for (Tarefa task : tasks) {

            HtmlElement li = new HtmlElement("li").setStyleClass("dd-item");
            Span userTitle = new Span(task.getDescricao());
            if(task.getConcluido() != null)
                userTitle.setAttribute("style", "text-decoration: line-through;");

            ol.addChildTag(li
                    .addChildTag(new Heading(Heading.HeadingType.h3).setStyleClass("title")
                            .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-clone dd-handle"))
                            .addChildTag(userTitle)
                            .addChildTag(new CheckBox()
                                    .setChecked(task.getConcluido() != null)
                                    .setId(task.getId()))));

            li.addChildTag(new DivLayout().setStyleClass("text").setAttribute("contenteditable", "false")
                    .setTextValue(StringUtils.formatarData(task.getDataInicio().toDate(), true, log)));

            li.addChildTag(new DivLayout().setStyleClass("text").setAttribute("contenteditable", "false")
                    .setAttribute("style", "color: #e53935;")
                    .setTextValue(StringUtils.formatarData(task.getDataFim().toDate(), true, log)));

            createUserActionButtons(li, task);
        }
    }

    private void createUserActionButtons(HtmlElement li, Tarefa tarefa) {

        li.addChildTag(new DivLayout().setStyleClass("actions")
                .addChildTag(new Button(null)
                        .setJsClick("KanbanTaskCoffee.edit", Collections.singletonMap("id", tarefa.getId()))
                        .setStyleClass("btn-icon").setAttribute("target", "_blank")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-edit")))
                .addChildTag(new Button(null)
                        .setJsClick("KanbanTaskCoffee.remove", Collections.singletonMap("id", tarefa.getId()))
                        .setStyleClass("btn-icon")
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-trash-alt"))));
    }
}