package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.Widget;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.GmudMensalModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lucasns
 * @since #1.0
 */
public class MonthlyGmudsChartView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<GmudMensalModel> gmuds;

    public MonthlyGmudsChartView(UserPrincipal principal, List<GmudMensalModel> gmuds) {
        this.principal = principal;
        this.gmuds = gmuds;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar os charts das Gmuds mensais...");

        DivLayout divCharts = new DivLayout().setAttribute("style", "display: flex;");

        Map<String, String> params = new HashMap<>();
        params.put("month", gmuds.get(0).getMes());
        params.put("type", "customer");
        params.put("title", "Clientes por GMUDs");
        divCharts.addChildTag(new DivLayout().setId("monthlycustbarchart")
                .addChildTag(new Widget()
                .setRenderer("MonthlyGmudBarChartWidgetRenderer")
                .setData(params)));

        Map<String, Long> params1 = new HashMap<>();
        params1.put("planejadas", gmuds.get(0).getPlanejados());
        params1.put("emergenciais", gmuds.get(0).getEmergenciais());
        divCharts.addChildTag(new DivLayout().setId("monthlypiechart")
                .addChildTag(new Widget()
                .setRenderer("MonthlyGmudPieChartWidgetRenderer")
                .setData(params1)));

        Map<String, String> params2 = new HashMap<>();
        params2.put("month", gmuds.get(0).getMes());
        params2.put("type", "classification");
        params2.put("title", "GMUDs por classificação");
        divCharts.addChildTag(new DivLayout().setId("monthlybarchart")
                .addChildTag(new Widget()
                .setRenderer("MonthlyGmudBarChartWidgetRenderer")
                .setData(params2)));

        mainDiv.addChildTag(divCharts);
    }
}