package meceap.appbuilder.view.management;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.BuildResultCommand;
import meceap.appbuilder.model.ExecutionResultCommand;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.DoubleStream;

/**
 * @author lucasns
 * @since #1.0
 */
public class ReleasesView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final List<Release> releases;
    private final BuildResultCommand hunterResults;
    private final BuildResultCommand bddResults;

    public ReleasesView(UserPrincipal principal, List<Release> releases, BuildResultCommand hunterResults, BuildResultCommand bddResults) {
        this.principal = principal;
        this.releases = releases;
        this.hunterResults = hunterResults;
        this.bddResults = bddResults;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Tentando preparar as informações de Releases...");

        Label label = new Label();
        mainDiv.setId("metrics");
        mainDiv.addChildTag(label);
        mainDiv.setStyleClass("bckgdDefault");

        createTitle(mainDiv);
        createReleasesTable(mainDiv);
        createBuildResultsTable(mainDiv);
    }

    private void createTitle(DivLayout div) {
        div.addChildTag(new DivLayout()
                .addChildTag(new Heading(Heading.HeadingType.h1)
                        .addChildTag(new Icon().setStyleClass("material-icons me-icon icon-check"))
                        .addChildTag(new Span(UserCatalog.getMessage("title.releases")))
                        .addChildTag(new Button(null)
                                .setButtonStyle(Button.ButtonStyle.Outline)
                                .setUrlAction(String.format("/%s/document/Release/create", principal.getCustomRoot().getName()))
                                .setStyleClass("btnActionTop btnActionTopRight")
                                .setTextValue("Nova Release"))));
    }

    private void createReleasesTable(DivLayout div) {
        HtmlElement table = new HtmlElement("table").setId("customgrid");
        HtmlElement thead = new HtmlElement("thead");
        HtmlElement tr = new HtmlElement("tr");
        tr.addChildTag(new HtmlElement("th").setTextValue("Release"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Ano"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Mês"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Qtd. Chamados"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Qtd. Chamados Validados QA"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Qtd. Devolução"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Índice de Reteste"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Chamado da GMUD"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Qtd. Impactos"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Chamado(s) de Impacto"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Subida para Produção"));
        tr.addChildTag(new HtmlElement("th").setTextValue("Sinalizador de Qualidade"));

        thead.addChildTag(tr);
        table.addChildTag(thead);
        fillReleasesTable(table);
        div.addChildTag(table);
    }

    private void fillReleasesTable(HtmlElement table) {
        for (Release release : releases) {

            HtmlElement linha = new HtmlElement("tr");

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getRelease())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getAno().toString())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getMes().getValue())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getQtdChamados().toString())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getQtdChamadosValQA().toString())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getQtdDevolucao().toString())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getIndicereteste())));

            Integer chamado = release.getRelease().getChamadoGmud();
            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new HrefLinkAnchor()
                            .setHref("https://issues.miisy.com/Misy/do/issue/show/" + chamado)
                            .setTarget(HrefLinkAnchor.Target.BLANK)
                            .setTextValue(chamado != null ? chamado.toString() : "")));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getQtdImpactos().toString())));

            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(release.getRelease().getChamadoImpacto())));

            String date = release.getRelease().getDataProd() != null ?
                    StringUtils.formatarData(release.getRelease().getDataProd().toDate(), false, log) : "";
            linha.addChildTag(new HtmlElement("td")
                    .addChildTag(new Span(date)));

            addQualityCell(linha,
                    release.getRelease().getQtdChamados(),
                    release.getRelease().getQtdImpactos());

            table.addChildTag(linha);
        }
    }

    private void addQualityCell(HtmlElement linha, int total, int impactos) {
        HtmlElement td = new HtmlElement("td").setAttribute("style", "text-align: center;");
        Icon icon = new Icon().setAttribute("style", "font-size: 2em");

        long mediaImpacto = Math.round(impactos * 100.0 / total);
        if (mediaImpacto <= 2)
            linha.addChildTag(td.addChildTag(icon.setStyleClass("b-green me-icon icon-thermometer-full")));
        else if (mediaImpacto <= 4)
            linha.addChildTag(td.addChildTag(icon.setStyleClass("b-orange me-icon icon-thermometer-half")));
        else
            linha.addChildTag(td.addChildTag(icon.setStyleClass("b-red me-icon icon-thermometer-empty")));
    }

    private void createBuildResultsTable(DivLayout div) {
        HtmlElement table = new HtmlElement("table").setStyleClass("buildsAverage");
        HtmlElement tr = new HtmlElement("tr");
        tr.addChildTag(new HtmlElement("th").setAttribute("rowspan", "2")
                .addChildTag(new Span("Mês atual"))
                .addChildTag(new HtmlElement("th").setAttribute("colspan", "2")
                        .addChildTag(new Span("Resultados")))
                .addChildTag(new HtmlElement("th").setAttribute("rowspan", "2")
                        .addChildTag(new Span("Média")).addChildTag(new HtmlElement("br")).addChildTag(new Span("Total"))));

        HtmlElement tr1 = new HtmlElement("tr");
        tr1.addChildTag(new HtmlElement("th")
                .addChildTag(new Span("Hunter")))
                .addChildTag(new HtmlElement("th")
                        .addChildTag(new Span("BDD")));

        table.addChildTag(tr);
        table.addChildTag(tr1);

        for(int i = 0; i < hunterResults.getDays().size(); ){
            ExecutionResultCommand hunterResult = hunterResults.getExecutionCommandList().get(i).getResultCommand();
            Double hunterValue = hunterResult != null ? hunterResult.getRating() : 0.0;
            ExecutionResultCommand bddResult = bddResults.getExecutionCommandList().get(i).getResultCommand();
            Double bddValue = bddResult != null ? bddResult.getRating() : 0.0;

            fillTable(table, hunterResults.getDays().get(i), ++i, hunterValue, bddValue);
        }
        div.addChildTag(table);
    }

    private void fillTable(HtmlElement table, int day, int index, Double hunterValue, Double bddValue){
        DoubleStream stream = DoubleStream.of(hunterValue, bddValue);
        OptionalDouble obj = stream.average();
        double average = obj.isPresent() ? obj.getAsDouble() : 0.0;
        HtmlElement tr = new HtmlElement("tr");
        tr.addChildTag(new HtmlElement("th").setAttribute("style", "align:left")
                .addChildTag(new Span(String.format("%sª sexta-feira (%s)", index, day)))
                .addChildTag(new HtmlElement("td")
                        .addChildTag(new Span(String.format("%s %%", StringUtils.formatarDecimal(hunterValue)))))
                .addChildTag(new HtmlElement("td")
                        .addChildTag(new Span(String.format("%s %%", StringUtils.formatarDecimal(bddValue)))))
                .addChildTag(new HtmlElement("td")
                        .addChildTag(new Span(String.format("%s %%", StringUtils.formatarDecimal(average))))));
        table.addChildTag(tr);
    }
}