package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.ImageInput;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.controller.url.ManagementControllerURL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

/**
 * @author lucasns
 * @since #1.0
 */
public class ManagementView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;

    public ManagementView(UserPrincipal principal) {
        this.principal = principal;
    }

    @Override
    protected String getTitle() {
        return "";
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        mainDiv.setStyleClass("manageBackg");
        mainDiv.addChildTag(new ImageInput().setImageURL("/bughunter/resource/?name=resources/img/logo-me.png")
                .setStyleClass("big-logo-me"));

        DivLayout div = new DivLayout().setStyleClass("divAllBtns");

        DivLayout div1 = new DivLayout().setStyleClass("divBtnsInline");
        createMenuButton(div1, UserCatalog.getMessage("button.metricas"), ManagementControllerURL.showMetrics(principal.getCustomRoot()));
        createMenuButton(div1, UserCatalog.getMessage("button.gmud"), ManagementControllerURL.showGmuds(principal.getCustomRoot(),
                String.valueOf(Calendar.getInstance().get(Calendar.YEAR))));
        createMenuButton(div1, UserCatalog.getMessage("button.releases"), ManagementControllerURL.showReleases(principal.getCustomRoot()));
        createMenuButton(div1, UserCatalog.getMessage("button.branches"), String.format("/%s/search/Branch", principal.getCustomRoot().getName()));

        DivLayout div2 = new DivLayout().setStyleClass("divBtnsInline");
        createMenuButton(div2, UserCatalog.getMessage("button.refresh"), String.format("/%s/search/Refresh", principal.getCustomRoot().getName()));
        createMenuButton(div2, UserCatalog.getMessage("button.controle"), String.format("/%s/search/Controleacessos", principal.getCustomRoot().getName()));
        createMenuButton(div2, UserCatalog.getMessage("button.builds.pontuais"), ManagementControllerURL.showBuildsPontuais(principal.getCustomRoot()));
        createMenuButton(div2, UserCatalog.getMessage("button.dashboard"), ManagementControllerURL.showDashboard(principal.getCustomRoot()));

        DivLayout div3 = new DivLayout().setStyleClass("divBtnsInline");
        createMenuButton(div3, UserCatalog.getMessage("button.branch.meceap"), String.format("/%s/search/BranchMECEAP", principal.getCustomRoot().getName()));
        createMenuButton(div3, UserCatalog.getMessage("button.branch.apps"), String.format("/%s/search/BranchApps", principal.getCustomRoot().getName()));
        createMenuButton(div3, UserCatalog.getMessage("button.kanban"), ManagementControllerURL.showKanban(principal.getCustomRoot()));

        div.addChildTag(div1);
        div.addChildTag(div2);
        div.addChildTag(div3);
        mainDiv.addChildTag(div);
    }

    private void createMenuButton(DivLayout div, String text, String urlAction){
        div.addChildTag(new Button()
                .setUrlAction(urlAction)
                .setTextValue(text)
                .setStyleClass("btnCustomMenu btnManager"));
    }
}