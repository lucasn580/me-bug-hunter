package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.model.base.UserCustomRoot;
import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.web.ui.html.*;
import br.com.me.ceap.web.view.page.AbstractHeaderMenuPageView;
import meceap.appbuilder.controller.url.HomeControllerURL;
import meceap.appbuilder.model.TaskCommand;

/**
 * @author lucasns
 * @since #1.0
 */
public abstract class AbstractTasksView extends AbstractHeaderMenuPageView {
// ------------------------------ FIELDS ------------------------------

    protected final UserCustomRoot customRoot;
    protected final TaskCommand command;

// --------------------------- CONSTRUCTORS ---------------------------

    public AbstractTasksView(UserCustomRoot customRoot, TaskCommand command) {
        this.customRoot = customRoot;
        this.command = command;
    }

// -------------------------- OTHER METHODS --------------------------

    protected abstract String getFormActionUrl();

    @Override
    protected void setContent(DivLayout div) {
        div.setStyleClass("me-card");

        DivLayout divProductVersion = new DivLayout();

        Form form = new Form().setModel(command)
                .setUseDefaultSubmitButton(false)
                .setAction(this.getFormActionUrl())
                .setId("formTask")
                .addChildTag(new FieldSet()
                        .setLegend(UserCatalog.getMessage("tarefa.nova"))
                        .addChildTag(
                                new Input(Input.InputType.hidden).setModel(command).setFieldName("redirectUrl"),
                                Form.createFormField("userHumanBeing", UserCatalog.getMessage("tarefa.usuario"), UserCatalog.getMessage("tarefa.usuario.descricao"),
                                        new SelectInput(SelectInput.Selection.simple)
                                                .setOptions(HomeControllerURL.getUsersAdmin(customRoot))
                                                .setOptionText("name")
                                                .setOptionValue("id")
                                                .setReadOnly(true)
                                                .setRequired(true)
                                                .setFieldName("userHumanBeing.id")
                                                .setModel(command)
                                ),
                                Form.createFormField("descricao", UserCatalog.getMessage("tarefa.descricao"), UserCatalog.getMessage("tarefa.descricao.descricao"),
                                        new TextInput()
                                                .setFieldName("descricao")
                                                .setModel(command)
                                ),
                                Form.createFormField("dataInicio", UserCatalog.getMessage("tarefa.data.inicio"), UserCatalog.getMessage("tarefa.data.inicio.descricao"),
                                        new DatetimePicker()
                                                .setFieldName("dataInicio")
                                                .setModel(command)
                                ),
                                Form.createFormField("dataFim", UserCatalog.getMessage("tarefa.data.fim"), UserCatalog.getMessage("tarefa.data.fim.descricao"),
                                        new DatetimePicker()
                                                .setFieldName("dataFim")
                                                .setModel(command)
                                ),
                                Form.createFormField("dependencias", UserCatalog.getMessage("tarefa.dependencias"), UserCatalog.getMessage("tarefa.dependencias.descricao"),
                                        new TextInput()
                                                .setFieldName("dependencias")
                                                .setModel(command)
                                ),
                                Form.createFormField("concluido", UserCatalog.getMessage("tarefa.concluido"), UserCatalog.getMessage("tarefa.concluido.descricao"),
                                        new CheckBox()
                                                .setFieldName("concluido")
                                                .setModel(command))));

        divProductVersion.addChildTag(
                form,
                new HorizontalRule(),
                new Button()
                        .setJsClick("TaskPage.saveTask")
                        .setButtonStyle(Button.ButtonStyle.Primary)
                        .setDisableAfterClick(true)
                        .setId("btnSave")
                        .setTextValue(UserCatalog.getMessage("button.save")));
        div.addChildTag(divProductVersion);
    }
}