package meceap.appbuilder.view.management;

import br.com.me.ceap.dynamic.extension.util.UserCatalog;
import br.com.me.ceap.dynamic.extension.model.base.principal.UserPrincipal;
import br.com.me.ceap.web.ui.html.Button;
import br.com.me.ceap.web.ui.html.DivLayout;
import br.com.me.ceap.web.ui.html.Input;
import br.com.me.ceap.web.ui.html.Label;
import br.com.me.ceap.web.view.page.AbstractTitledPageView;
import meceap.appbuilder.model.external.CountersCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author lucasns
 * @since #1.0
 */
public class RegisterMetricView extends AbstractTitledPageView {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final UserPrincipal principal;
    private final CountersCommand counters;
    private final String month;

    public RegisterMetricView(UserPrincipal principal, CountersCommand counters, String month) {
        this.principal = principal;
        this.counters = counters;
        this.month = month;
    }

    @Override
    protected String getTitle() {
        return UserCatalog.getMessage("title.metric.register");
    }

    @Override
    protected void prepareDiv(final DivLayout mainDiv) {
        log.info("Preparando modal de gravação de métricas...");

        mainDiv.setId("newMetric");
        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_4)
                        .addChildTag(new Label("Mês:"))
                        .addChildTag(new Input().setStyleClass("form-control")
                                .setValue(month)
                                .setAttribute("disabled", "true"))));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("Não Conformidades em Produção"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getNonCompliantProd().toString())
                        .setAttribute("disabled", "true")));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("Não Conformidades Detectadas nos Testes"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getNonCompliantDetectedTest().toString())
                        .setAttribute("disabled", "true")));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("% Testes Automatizados de Regressão com sucesso"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getAutomRegressionTestsSuccess().toString())));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("Índice de Reteste"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getRetestIndex().toString())));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("Qtde de Builds pontuais"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getAmountPointBuild().toString())));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Label("Métrica de Relacionamento"))
                .addChildTag(new Input().setStyleClass("form-control")
                        .setValue(counters.getRelationshipMetric().toString())
                        .setAttribute("disabled", "true")));

        mainDiv.addChildTag(new DivLayout(DivLayout.DivStyle.Col_lg_12)
                .addChildTag(new Button()
                        .setJsClick("RegisterMetricsViewCoffee.newMetrics")
                        .setDisableAfterClick(true)
                        .setButtonStyle(Button.ButtonStyle.Primary)
                        .setTextValue("Gravar")));
    }
}