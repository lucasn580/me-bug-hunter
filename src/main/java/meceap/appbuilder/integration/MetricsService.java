package meceap.appbuilder.integration;

import meceap.appbuilder.model.MetricaMensalModel;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public interface MetricsService {
// -------------------------- OTHER METHODS --------------------------

    List<MetricaMensalModel> getMetrics() throws RuntimeException;

    Integer getAmountMetricFilterOne() throws RuntimeException;

    Integer getAmountMetricFilterTwo() throws RuntimeException;

    Integer getAmountRelationshipMetric();

    void postMetrics(String[] values) throws RuntimeException;
}