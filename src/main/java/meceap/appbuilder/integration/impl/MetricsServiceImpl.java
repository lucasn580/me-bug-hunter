package meceap.appbuilder.integration.impl;

import meceap.appbuilder.integration.MetricsService;
import meceap.appbuilder.integration.MisyTrackerService;
import meceap.appbuilder.model.MetricaMensalModel;
import meceap.appbuilder.model.external.metrics.DocumentModel;
import meceap.appbuilder.model.external.metrics.MetricType;
import meceap.appbuilder.model.external.metrics.MetricsResultModel;
import meceap.appbuilder.model.external.misytracker.IssueResultModel;
import meceap.appbuilder.service.AppConfigurationService;
import meceap.appbuilder.util.CustomCriteria;
import meceap.appbuilder.util.RestTemplateFactory;
import meceap.appbuilder.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.text.DateFormatSymbols;
import java.util.*;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class MetricsServiceImpl implements MetricsService {
// ------------------------------ FIELDS ------------------------------

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private AppConfigurationService appConfigurationService;
    private RestTemplateFactory restTemplateFactory;
    private MisyTrackerService misyTrackerService;
    private Map<Integer, String> metrics;
    private Calendar cal;
    private int relationshipResult;

// --------------------- GETTER / SETTER METHODS ---------------------

    @Inject
    public void setAppConfigurationService(AppConfigurationService appConfigurationService) {
        this.appConfigurationService = appConfigurationService;
    }

    @Inject
    public void setRestTemplateFactory(RestTemplateFactory restTemplateFactory) {
        this.restTemplateFactory = restTemplateFactory;
    }

    @Inject
    public void setMisyTrackerService(MisyTrackerService misyTrackerService) {
        this.misyTrackerService = misyTrackerService;
    }

    @Override
    public Integer getAmountRelationshipMetric(){
        return relationshipResult;
    }

// ------------------------ INTERFACE METHODS ------------------------


// --------------------- Interface MetricsService ---------------------

    @Override
    public List<MetricaMensalModel> getMetrics(){
        cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);

        int ano = cal.get(Calendar.YEAR);
        int mesPassado = cal.get(Calendar.MONTH) +1;

        List<String[]> criteria = getCriteriaMensalBase(mesPassado, ano);

        // Métricas
        metrics = new HashMap<>();
        metrics.put(0, MetricType.NOT_CONFORM_PROD.getMetricaDocId());
        metrics.put(1, MetricType.NOT_CONFORM_DETECTED_TESTS.getMetricaDocId());
        metrics.put(2, MetricType.AUTOM_REGRESSION_TESTS_SUCCESS.getMetricaDocId());
        metrics.put(3, MetricType.RETEST_INDEX.getMetricaDocId());
        metrics.put(4, MetricType.AMOUNT_POINT_BUILDS.getMetricaDocId());
        metrics.put(5, MetricType.RELATIONSHIP_METRIC.getMetricaDocId());

        MetricsResultModel result = getMetrics(CustomCriteria.jsonRequest(criteria, metrics.values()));
        List<MetricaMensalModel> metricaList = new ArrayList<>();

        for(int i = 1; i <= mesPassado; i++){

            MetricaMensalModel metrica = new MetricaMensalModel();
            MetricsResultModel emptyResult = new MetricsResultModel(6);

            if(result != null){
                for(DocumentModel doc : result.getDocuments()) {
                    if(doc.getHeader()!=null && doc.getHeader().getMes() == i)
                        metrics.entrySet().stream()
                                .filter(m -> doc.getHeader().getMetricaDocId().equals(m.getValue()))
                                .forEach(m -> emptyResult.setDocument(m.getKey(), doc));
                }
            }
            metrica.setMes(new DateFormatSymbols(new Locale("pt", "BR")).getMonths()[i-1]);
            metrica.setMetrics(emptyResult);
            metricaList.add(metrica);

            if(i == mesPassado){
                DocumentModel r = metricaList.get(i-1).getMetrics().getDocuments().get(5);
                if (r.getResultadoList() != null )
                    relationshipResult = r.getResultadoList().get(0).getResultadoMetrica();
                else
                    relationshipResult = 0;
            }
        }
        return metricaList;
    }

    @Override
    public Integer getAmountMetricFilterOne(){
        log.info("Recuperando issues mês passado referente a métrica 'Não Conformidades em Produção'...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"is", "Central de Serviços SaaS", "header.department.name"};
        String[] criteria1 = {"is", "NC - ANÁLISE - VERIFICAÇÃO", "header.departmentCategory.category.description"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.addAll(getCriteriaForLastMonth());

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        return issues.getTotalHits();
    }

    @Override
    public Integer getAmountMetricFilterTwo(){
        log.info("Recuperando issues mês passado referente a métrica 'Não Conformidades Detectadas nos Testes'...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"is", "QA TI", "header.department.name"};
        String[] criteria1 = {"in", "Não Conformidade Exploratória,Falha na execução,Não Conformidade", "header.departmentCategory.category.description"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.addAll(getCriteriaForLastMonth());
        IssueResultModel filtro = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        int totalHits = filtro.getTotalHits();

        criterias.clear();

        criterias.addAll(getCriteriaForLastMonth());
        String[] criteria2 = {"is", "Miisy", "header.department.name"};
        String[] criteria3 = {"is", "Não Conformidade", "header.departmentCategory.category.description"};
        String[] criteria4 = {"is", "QA TI", "header.reporter.department.name"};
        criterias.add(criteria2);
        criterias.add(criteria3);
        criterias.add(criteria4);
        filtro = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        totalHits += filtro.getTotalHits();

        return totalHits;
    }

    @Override
    public void postMetrics(String[] values) {
        String url = appConfigurationService.getAppManagerByPK("urlApiMetricsResults");
        ClientHttpRequestFactory requestFactory = restTemplateFactory.getClientHttpRequestFactory();
        metrics.forEach((k,v) ->
                restTemplateFactory.postForObject(url, MetricsResultModel.class, new RestTemplate(requestFactory), CustomCriteria.jsonResultMetric(cal, v, values[k])));
    }

// -------------------------- OTHER METHODS --------------------------

    private List<String[]> getCriteriaMensalBase(Integer mes, Integer ano){
        String[] criteria = {"greaterThanEqual", "1", "header.mes"};
        String[] criteria1 = {"lesserThanEqual", mes.toString(), "header.mes"};
        String[] criteria2 = {"is", ano.toString(), "header.ano"};

        List<String[]> criterias = new ArrayList<>();
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.add(criteria2);

        return criterias;
    }

    private List<String[]> getCriteriaForLastMonth(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));

        int anoAtual = cal.get(Calendar.YEAR);
        String mesPassado = StringUtils.formatarDecimalData(cal.get(Calendar.MONTH) + 1);
        String ultimoDiaMesPassado =  StringUtils.formatarDecimalData(cal.get(Calendar.DAY_OF_MONTH));

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"greaterThanEqual", anoAtual+"-"+mesPassado+"-01", "created"};
        String[] criteria1 = {"lesserThanEqual", anoAtual+"-"+mesPassado+"-"+ultimoDiaMesPassado, "created"};
        criterias.add(criteria);
        criterias.add(criteria1);
        return criterias;
    }

    private MetricsResultModel getMetrics(String json) {
        log.info("Recuperando metrics...");

        String url = appConfigurationService.getAppManagerByPK("urlApiMetricsResults");
        ClientHttpRequestFactory requestFactory = restTemplateFactory.getClientHttpRequestFactory();
        MetricsResultModel issues = null;
        try {
            issues = restTemplateFactory.postForObject(url+"/getbycriteria", MetricsResultModel.class, new RestTemplate(requestFactory), json);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return issues;
    }
}