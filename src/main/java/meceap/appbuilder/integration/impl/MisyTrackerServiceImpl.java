package meceap.appbuilder.integration.impl;

import meceap.appbuilder.integration.MisyTrackerService;
import meceap.appbuilder.model.external.misytracker.IssueResultModel;
import meceap.appbuilder.model.external.misytracker.RoadmapReleasesModel;
import meceap.appbuilder.service.AppConfigurationService;
import meceap.appbuilder.util.RestTemplateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class MisyTrackerServiceImpl implements MisyTrackerService {
// ------------------------------ FIELDS ------------------------------

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private AppConfigurationService appConfigurationService;
    private RestTemplateFactory restTemplateFactory;

// --------------------- GETTER / SETTER METHODS ---------------------

    @Inject
    public void setAppConfigurationService(AppConfigurationService appConfigurationService) {
        this.appConfigurationService = appConfigurationService;
    }

    @Inject
    public void setRestTemplateFactory(RestTemplateFactory restTemplateFactory) {
        this.restTemplateFactory = restTemplateFactory;
    }

// ------------------------ INTERFACE METHODS ------------------------


// --------------------- Interface MisyTrackerService ---------------------

    @Override
    public IssueResultModel getIssues(String json) {
        log.info("Recuperando issues...");

        String url = appConfigurationService.getAppManagerByPK("urlApiMiisyIssues");
        ClientHttpRequestFactory requestFactory = restTemplateFactory.getClientHttpRequestFactory();
        IssueResultModel issues = null;
        try {
            issues = restTemplateFactory.postForObject(url, IssueResultModel.class, new RestTemplate(requestFactory), json);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return issues;
    }

    @Override
    public RoadmapReleasesModel getProductVersions(String json) {
        log.info("Recuperando ProductVersions...");

        String url = appConfigurationService.getAppManagerByPK("urlApiMiisyProductVersion");
        ClientHttpRequestFactory requestFactory = restTemplateFactory.getClientHttpRequestFactory();
        RoadmapReleasesModel productVersions = null;
        try {
            productVersions = restTemplateFactory.postForObject(url, RoadmapReleasesModel.class, new RestTemplate(requestFactory), json);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

        return productVersions;
    }
}