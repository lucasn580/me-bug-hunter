package meceap.appbuilder.integration;

import meceap.appbuilder.model.external.misytracker.IssueResultModel;
import meceap.appbuilder.model.external.misytracker.RoadmapReleasesModel;

/**
 * @author lucasns
 * @since #1.0
 */
public interface MisyTrackerService {
// -------------------------- OTHER METHODS --------------------------

    IssueResultModel getIssues(String json) throws RuntimeException;

    RoadmapReleasesModel getProductVersions(String json) throws RuntimeException;
}