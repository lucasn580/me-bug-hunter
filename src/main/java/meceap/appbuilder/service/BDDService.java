package meceap.appbuilder.service;

import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.appbuilder.document.bDD.manager.BDDDocumentManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import br.com.me.ceap.util.criteria.operators.Operator;
import meceap.appbuilder.model.ClienteModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class BDDService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private BDDDocumentManager bddDocumentManager;
    private List<ClienteModel> clientes = new ArrayList<>();

    @Inject
    public void setBddDocumentManager(BDDDocumentManager bddDocumentManager) {
        this.bddDocumentManager = bddDocumentManager;
    }

    public List<ClienteModel> getCustomersBDD() {
        log.info("Agrupando por cliente todos os casos de teste registrados...");

        List<BDD> bddList = new ArrayList<>(bddDocumentManager.getAll());
        clientes.clear();

        for(BDD bdd : bddList) {
            if(bdd.getBDD().getCliente() != null && !verificarSeListaContemCliente(bdd))
                clientes.add(new ClienteModel(bdd));
        }
        return clientes;
    }

    public BDD getByCustomer(String customer) {
        FilterCriteria criteria = new FilterCriteria();
        criteria.and("bDD.cliente.nome", Operator.is, customer);
        return bddDocumentManager.getByCriteria(criteria).get(0);
    }

//    public List<ClienteModel> getScenariosRunned(String idsCts) {
//        List<ClienteModel> cli = getCustomersTestCases();
//        String[] ids = idsCts.split(",");
//        List<String> cenarios = Arrays.asList(ids);
//
//        for (ClienteModel cliente : cli){
//
//            List<TestCaseModel> toRemove = new ArrayList<>();
//
//            for (TestCaseModel test : cliente.getCasosDeTeste()) {
//                if(!cenarios.contains(test.getId()))
//                    toRemove.add(test);
//            }
//            cliente.getCasosDeTeste().removeAll(toRemove);
//        }
//        return cli;
//    }

    private boolean verificarSeListaContemCliente(final BDD bdd){
        return clientes.stream().anyMatch(input ->
                (input.getCliente().equals(bdd.getBDD().getCliente().getNome()) && input.addBDD(bdd))
        );
    }
}