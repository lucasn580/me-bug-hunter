package meceap.appbuilder.service;

import br.com.me.appbuilder.document.buildspontuais.Buildspontuais;
import br.com.me.appbuilder.document.buildspontuais.manager.BuildspontuaisDocumentManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import br.com.me.ceap.web.ui.html.Span;
import meceap.appbuilder.integration.MisyTrackerService;
import meceap.appbuilder.model.external.misytracker.DocumentModel;
import meceap.appbuilder.model.external.misytracker.IssueResultModel;
import meceap.appbuilder.util.CustomCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class BuildsPontuaisService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private AppConfigurationService appConfigurationService;
    private BuildspontuaisDocumentManager buildsManager;
    private MisyTrackerService misyTrackerService;

    @Inject
    public void setAppConfigurationService(AppConfigurationService appConfigurationService) {
        this.appConfigurationService = appConfigurationService;
    }

    @Inject
    public void setBuildsManager(BuildspontuaisDocumentManager buildsManager) {
        this.buildsManager = buildsManager;
    }

    @Inject
    public void setMisyTrackerService(MisyTrackerService misyTrackerService) {
        this.misyTrackerService = misyTrackerService;
    }

    public List<Buildspontuais> getAll() {
        log.info("Retornando a lista de Releases...");
        FilterCriteria criteria = new FilterCriteria();
        criteria.getSearchParams().addSortField("created", "asc");

        List<Buildspontuais> lista = buildsManager.getByCriteria(criteria);
        for(Buildspontuais rel : lista){
            if(rel.getBuildsPontuais().getQtdChamados() == null){
                rel.getBuildsPontuais().setQtdChamados(0);
            }
            if(rel.getBuildsPontuais().getChamados() == null){
                rel.getBuildsPontuais().setChamados("");
            }
            if(rel.getBuildsPontuais().getBuild() == null){
                rel.getBuildsPontuais().setBuild("");
            }
            if(rel.getBuildsPontuais().getNroMiisy() == null){
                rel.getBuildsPontuais().setNroMiisy("");
            }
            if(rel.getBuildsPontuais().getQtdImpactos() == null){
                rel.getBuildsPontuais().setQtdImpactos(0);
            }
            if(rel.getBuildsPontuais().getChamadoImpacto() == null){
                rel.getBuildsPontuais().setChamadoImpacto("");
            }
            if(rel.getBuildsPontuais().getObservacao() == null){
                rel.getBuildsPontuais().setObservacao("");
            }
        }
        return lista;
    }

    public List<DocumentModel> getBuildsIssues(){
        log.info("Recuperando builds pontuais deste mês...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"is", "Central de Serviços SaaS", "header.department.name"};
        String[] criteria1 = {"is", "NC - ANÁLISE - VERIFICAÇÃO", "header.departmentCategory.category.description"};
        String[] criteria2 = {"thisMonth", "", "updated"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.add(criteria2);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        List<DocumentModel> docs = new ArrayList<>();
        if (issues.getDocument() != null)
            docs.addAll(issues.getDocument());
        return docs;
    }

    public List<DocumentModel> getBuildsImpactIssues(){
        log.info("Recuperando impactos em builds pontuais deste mês...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"is", "Central de Serviços SaaS", "header.department.name"};
        String[] criteria1 = {"in", "aberto,em andamento,em análise", "header.status.name"};
        String[] criteria2 = {"isNotNull", "", "header.knownErrors"};
        String[] criteria3 = {"thisMonth", "", "updated"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.add(criteria2);
        criterias.add(criteria3);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        List<DocumentModel> docs = new ArrayList<>();
        if (issues.getDocument() != null)
            docs.addAll(issues.getDocument());
        return docs;
    }

    public String getFilterBuildURL() {
        return appConfigurationService.getAppManagerByPK("urlMisyFilterBuild");
    }

    public Map<LocalDate, Long> splitBuildsIssuesByDay() {
        return getBuildsIssues().stream().collect(Collectors.groupingBy(DocumentModel::getUpdated,
                Collectors.counting()));
    }

    public Map<LocalDate, Long> splitBuildsImpactIssuesByDay() {
        return getBuildsImpactIssues().stream().collect(Collectors.groupingBy(DocumentModel::getUpdated,
                Collectors.counting()));
    }

    public List<String> getAllProductVersions() {
        List<String> productVersions = new ArrayList<>();
        List<String> environments = new ArrayList<>();
        environments.add("trunk.");
        environments.add("qa.");
        environments.add("uat.");
        environments.add("stg.");
        environments.add("");

        URLConnection conn;
        try {
            for(String env : environments){
                URL obj = new URL(String.format("http://%sme.com.br", env));
                conn = obj.openConnection();
                productVersions.add(conn.getHeaderField("x-powered-by").replace("_", "."));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return productVersions;
    }
}