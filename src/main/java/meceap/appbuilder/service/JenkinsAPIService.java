package meceap.appbuilder.service;

import br.com.me.appbuilder.Execucao;
import meceap.appbuilder.model.ExecutionCommand;
import meceap.appbuilder.model.ExecutionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
class JenkinsAPIService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private AppConfigurationService appConfigurationService;

    @Inject
    public void setAppConfigurationService(AppConfigurationService appConfigurationService) {
        this.appConfigurationService = appConfigurationService;
    }

    protected String connect(ExecutionCommand execution, String suiteName){

        String response = null;

        log.info("Abrindo a conexão...");
        try {
            String user = appConfigurationService.getAppManagerByPK("userJenkins");
            String pass = appConfigurationService.getAppManagerByPK("passJenkins");
            String crumb = appConfigurationService.getAppManagerByPK("crumbJenkins");
            String urlJenkins;
            String urlParams = String.format("id=%s", execution.getId());

            if (execution.getType().equals(ExecutionType.TEST_CASE.getType()))
                urlJenkins = appConfigurationService.getAppManagerByPK("urlJenkins");
            else
                urlJenkins = String.format(appConfigurationService.getAppManagerByPK("urlJenkinsBDD"), execution.getProject(), suiteName, execution.getEnvironmentCommand().getName());

            log.info(String.format("Conectando na URL: %s", urlJenkins));
            URL url = new URL (urlJenkins);
            String authStr = user +":"+  pass;
            String encoding = DatatypeConverter.printBase64Binary(authStr.getBytes(StandardCharsets.UTF_8));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Jenkins-Crumb", crumb);

            byte[] postData = urlParams.getBytes(StandardCharsets.UTF_8);
            try(DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                wr.write(postData);
            }

            // Get the response code
            int statusCode = connection.getResponseCode();

            InputStream is;

            if (statusCode >= 200 && statusCode < 400)
                is = connection.getInputStream();
            else
                is = connection.getErrorStream();

            log.info("Salvando o resultado encontrado...");
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = in.readLine()) != null) {
                log.info(line);
                response += line;
            }
        } catch(Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return response;
    }
}