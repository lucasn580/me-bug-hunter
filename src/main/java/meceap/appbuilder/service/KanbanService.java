package meceap.appbuilder.service;

import br.com.me.appbuilder.Tarefa;
import br.com.me.appbuilder.manager.TarefaManager;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class KanbanService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private TarefaManager tarefaManager;

    @Inject
    public void setTarefaManager(TarefaManager tarefaManager) {
        this.tarefaManager = tarefaManager;
    }

    private List<Tarefa> getAll() {
        log.info("Retornando a lista de Releases...");

        List<Tarefa> lista = tarefaManager.getAll();
        for (Tarefa tarefa : lista) {
            if(tarefa.getDataInicio() == null)
                tarefa.setDataInicio(new DateTime());
            if(tarefa.getDataFim() == null)
                tarefa.setDataFim(new DateTime().plusHours(1));
            if(tarefa.getDependencias() == null)
                tarefa.setDependencias("");
        }
        return lista;
    }

    public List<List<Tarefa>> getUsersTasks() {
        List<Tarefa> taskListModel = getAll();

        Map<UserHumanBeing, List<Tarefa>> tasks = taskListModel.stream().collect(
                Collectors.groupingBy(Tarefa::getUsuario, Collectors.toList()));

        return new ArrayList<>(tasks.values());
    }

    public Tarefa save(Tarefa loadTaskFromCommand) {
        return tarefaManager.save(loadTaskFromCommand);
    }

    public void remove(String id) {
        tarefaManager.remove(id);
    }
}