package meceap.appbuilder.service;

import br.com.me.appbuilder.document.casodeTeste.CasodeTeste;
import br.com.me.appbuilder.document.casodeTeste.CasodeTesteItemInCasodeTeste;
import br.com.me.appbuilder.document.casodeTeste.manager.CasodeTesteDocumentManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import br.com.me.ceap.util.criteria.operators.Operator;
import meceap.appbuilder.model.ClienteModel;
import meceap.appbuilder.model.TestCaseModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class TestCasesService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private CasodeTesteDocumentManager tcDocManager;
    private List<ClienteModel> clientes = new ArrayList<>();

    @Inject
    public void setCasodeTesteDocumentManager(CasodeTesteDocumentManager tcDocManager) {
        this.tcDocManager = tcDocManager;
    }

    public List<ClienteModel> getCustomersTestCases() {
        log.info("Agrupando por cliente todos os casos de teste registrados...");

        List<CasodeTeste> casos = new ArrayList<>(tcDocManager.getAll());
        clientes.clear();

        for(CasodeTeste caso : casos) {
            if(!verificarSeListaContemCliente(caso))
                clientes.add(new ClienteModel(caso));
        }
        return clientes;
    }

    public List<TestCaseModel> getTcsDetails() {
        log.info("Agrupando por cliente todos os casos de teste registrados...");

        List<TestCaseModel> tcs = new ArrayList<>();
        List<ClienteModel> cli = getCustomersTestCases();

        for (ClienteModel cliente : cli)
            tcs.addAll(cliente.getCasosDeTeste());
        return tcs;
    }

    public List<ClienteModel> getScenariosRunned(String idsCts) {
        List<ClienteModel> cli = getCustomersTestCases();
        String[] ids = idsCts.split(",");
        List<String> cenarios = Arrays.asList(ids);

        for (ClienteModel cliente : cli){

            List<TestCaseModel> toRemove = new ArrayList<>();

            for (TestCaseModel test : cliente.getCasosDeTeste()) {
                if(!cenarios.contains(test.getId()))
                    toRemove.add(test);
            }
            cliente.getCasosDeTeste().removeAll(toRemove);
        }
        return cli;
    }

    private boolean verificarSeListaContemCliente(final CasodeTeste ct){
        return clientes.stream().anyMatch(input ->
                (input.getCliente().equals(ct.getCasodeTeste().getCliente().getNome()) && input.addCasosDeTeste(ct))
        );
    }

    public CasodeTeste getById(String id){
        return tcDocManager.get(id);
    }

    public List<CasodeTeste> getActivesTestCases(){
        FilterCriteria criteria = new FilterCriteria();
        criteria.and("casodeTeste.status.status", Operator.notIn, "Bloqueado");
        List<CasodeTeste> testCases = tcDocManager.getByCriteria(criteria);

        Comparator<CasodeTeste> comparator = Comparator.comparing(a -> a.getCasodeTeste().getCliente().getNome());
        comparator = comparator.thenComparingInt(s-> s.getCasodeTeste().getOrdem());
        return testCases.stream()
                .filter(s-> s.getCasodeTeste().getOrdem() != null)
                .sorted(comparator)
                .collect(Collectors.toList());
    }
}