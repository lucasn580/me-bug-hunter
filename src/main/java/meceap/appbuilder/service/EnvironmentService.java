package meceap.appbuilder.service;

import br.com.me.appbuilder.Ambiente;
import br.com.me.appbuilder.manager.AmbienteManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import br.com.me.ceap.util.criteria.operators.Operator;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class EnvironmentService {

    private AmbienteManager ambienteManager;

    @Inject
    public void setAmbienteManager(AmbienteManager ambienteManager) {
        this.ambienteManager = ambienteManager;
    }

    public List<Ambiente> getActivesEnvironments(){
        FilterCriteria criteria = new FilterCriteria();
        criteria.and("ativo", Operator.is, "true");
        return ambienteManager.getByCriteria(criteria);
    }

    public void registerEnvironments(List<Ambiente> ambientes) {
        List<Ambiente> fullList = getAll();

        ambientes.forEach(env -> {
            if(fullList.stream().noneMatch(e -> e.getNome().equals(env.getNome())))
                ambienteManager.save(env);
        });

        fullList.forEach(envOld -> {
            if(ambientes.stream().noneMatch(e -> e.getNome().equals(envOld.getNome())))
                ambienteManager.remove(envOld.getId());
        });
    }

    public List<Ambiente> getAll(){
        return ambienteManager.getAll();
    }
}