package meceap.appbuilder.service;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.appbuilder.document.release.manager.ReleaseDocumentManager;
import meceap.appbuilder.model.calendar.DueDateCalendarEvent;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Created by luiza on 27/01/2015.
 */
@Service
public class ReleaseCalendarService extends DueDateCalendarService {

    public static String CALENDAR = "Releases";

    public static String COLOR = "#ef9a4a";
    public static String COLOR_PROD = "#8e55ef";

// ------------------------------ FIELDS ------------------------------

    @Inject
    private ReleaseDocumentManager releaseDocumentManager;

// -------------------------- OTHER METHODS --------------------------

    @Override
    public void changeDueDate(DueDateCalendarEvent event) {
        Release release = releaseDocumentManager.get(event.getDocumentId());

        DateTime startDate = event.getStart().withZone(DateTimeZone.UTC);
        DateTime expectedDate = release.getRelease().getDataProd().withZone(DateTimeZone.UTC);

        if (startDate.equals(expectedDate))
            return;

        release.getRelease().setDataProd(event.getStart());
        releaseDocumentManager.save(release);
    }
}