package meceap.appbuilder.service;

import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.model.base.UserRole;
import br.com.me.ceap.dynamic.extension.service.RoleService;
import meceap.appbuilder.security.RolePermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private RoleService roleService;

    @Inject
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public boolean userIsAdmin(UserHumanBeing userHumanBeing) {
        log.info("Verificando se o usuário é admin...");
        List<UserRole> roles = roleService.getRoles(userHumanBeing);

        if(roles != null && !roles.isEmpty())
            for(UserRole role : roles)
                if(RolePermission.ROLE_ADMIN.equals(role.getName()))
                    return true;

        return false;
    }

    public List<UserHumanBeing> getUsersAdmin(){
        UserRole role = roleService.getRole(RolePermission.ROLE_ADMIN);
        return roleService.getHumanBeings(role);
    }
}