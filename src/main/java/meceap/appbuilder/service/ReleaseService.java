package meceap.appbuilder.service;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.appbuilder.document.release.manager.ReleaseDocumentManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import meceap.appbuilder.integration.MisyTrackerService;
import meceap.appbuilder.model.external.misytracker.DocumentModel;
import meceap.appbuilder.model.external.misytracker.IssueResultModel;
import meceap.appbuilder.model.external.misytracker.ProductVersionModel;
import meceap.appbuilder.model.external.misytracker.RoadmapReleasesModel;
import meceap.appbuilder.util.CustomCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class ReleaseService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private AppConfigurationService appConfigurationService;
    private ReleaseDocumentManager releaseManager;
    private MisyTrackerService misyTrackerService;

    @Inject
    public void setAppConfigurationService(AppConfigurationService appConfigurationService) {
        this.appConfigurationService = appConfigurationService;
    }

    @Inject
    public void setReleaseManager(ReleaseDocumentManager releaseManager) {
        this.releaseManager = releaseManager;
    }

    @Inject
    public void setMisyTrackerService(MisyTrackerService misyTrackerService) {
        this.misyTrackerService = misyTrackerService;
    }

    public List<Release> getAll() {
        log.info("Retornando a lista de Releases...");
        FilterCriteria criteria = new FilterCriteria();

        List<Release> lista = releaseManager.getByCriteria(criteria);
        for (Release rel : lista) {
            if (rel.getRelease().getQtdChamadosValQA() == null) {
                rel.getRelease().setQtdChamadosValQA(0);
            }
            if (rel.getRelease().getQtdDevolucao() == null) {
                rel.getRelease().setQtdDevolucao(0);
            }
            if (rel.getRelease().getChamadoImpacto() == null) {
                rel.getRelease().setChamadoImpacto("");
            }
        }
        return lista;
    }

    public List<DocumentModel> getAllIssues() {
        log.info("Recuperando issues validadas neste mês...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"in", getThisMonthReleasesNames(), "header.targetVersion.name"};
        criterias.add(criteria);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        List<DocumentModel> docs = new ArrayList<>();
        if (issues.getDocument() != null)
            docs.addAll(issues.getDocument());
        return docs;
    }

    public List<DocumentModel> getPassedIssues() {
        log.info("Recuperando issues validadas neste mês...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"in", "aguardando build para Produção,fechado", "header.status.name"};
        String[] criteria1 = {"in", getThisMonthReleasesNames(), "header.targetVersion.name"};
        criterias.add(criteria);
        criterias.add(criteria1);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        List<DocumentModel> docs = new ArrayList<>();
        if (issues.getDocument() != null)
            docs.addAll(issues.getDocument());
        return docs;
    }

    private List<DocumentModel> getReturnedIssues() {
        log.info("Recuperando issues retornadas neste mês...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"thisMonth", "", "updated"};
        String[] criteria1 = {"in", String.join(",", getListOfReturnStates()), "header.devStatus.name"};
        String[] criteria2 = {"in", getThisMonthReleasesNames(), "header.targetVersion.name"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.add(criteria2);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));
        List<DocumentModel> docs = new ArrayList<>();
        if (issues.getDocument() != null)
            docs.addAll(issues.getDocument());
        return docs;
    }

    private List<ProductVersionModel> getAllReleases() {
        log.info("Buscando todo o roadmap de Releases (ProductVersion)...");

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"is", "MEWeb", "product.name"};
        String[] criteria1 = {"isNot", "Fechado", "status.name"};
        criterias.add(criteria);
        criterias.add(criteria1);

        RoadmapReleasesModel roadmap = misyTrackerService.getProductVersions(CustomCriteria.jsonRequest(criterias, "AND"));

        if (roadmap.getTotalHits() > 0)
            return roadmap.getProductVersionModels();
        else
            return null;
    }

    private List<ProductVersionModel> getThisMonthReleases() {
        log.info("Buscando as Releases do mês atual...");

        List<ProductVersionModel> releases = getAllReleases();

        Calendar cal = Calendar.getInstance();
        int mesAtual = cal.get(Calendar.MONTH)+1;

        assert releases != null;
        return releases.stream().filter(pvm -> pvm.getDueDate().getMonthValue() == mesAtual).collect(Collectors.toList());
    }

    private String getThisMonthReleasesNames() {
        log.info("Buscando as Releases do mês atual...");

        List<ProductVersionModel> releases = getThisMonthReleases();
        return releases.stream()
                .map(ProductVersionModel::getName)
                .collect(Collectors.joining(","));
    }

    private List<String> getListOfReturnStates() {
        List<String> states = new ArrayList<>();
        states.add("Falha: 01 Retorno");
        states.add("Falha: 02 Retornos");
        states.add("Falha: 03 Retornos");
        states.add("Falha: 04 Retornos");
        states.add("Falha: Mais de 04 Retornos");
        return states;
    }

    public String getCurrentReleaseVersion() {
        log.info("Buscando a versão atual (Em testes) do roadmap de Releases (ProductVersion)...");

        List<ProductVersionModel> releases = getAllReleases();

        if (releases != null)
            return releases.stream().filter(e -> e.getStatusProductVersionModel().getName().equals("Em testes")).findFirst().get().getName();
        else
            return "";
    }

    public String formatFilterIssueURL() {
        String urlBase = appConfigurationService.getAppManagerByPK("urlMisyFilterIssue");

        List<ProductVersionModel> releases = getThisMonthReleases();
        return String.format(urlBase, releases.stream()
                .map(ProductVersionModel::getId)
                .collect(Collectors.joining(",")));
    }

    public Map<LocalDate, Long> splitReturnedIssuesByDay() {
        List<DocumentModel> issues = getReturnedIssues();
        List<String> states = getListOfReturnStates();

        for (DocumentModel doc : issues) {
            for (int i = 0; i < states.size(); i++)
                if (doc.getHeader().getDevStatus() != null && doc.getHeader().getDevStatus().getName().equals(states.get(i)))
                    doc.getHeader().getDevStatus().setCount(i + 1);
        }

        Map<LocalDate, List<DocumentModel>> listMap = issues.stream().collect(Collectors.groupingBy(DocumentModel::getUpdated,
                Collectors.toList()));
        Map<LocalDate, Long> map = new HashMap<>();

        for (Map.Entry<LocalDate, List<DocumentModel>> listEntry : listMap.entrySet()) {
            long sum = 0;
            for (DocumentModel doc : listEntry.getValue()) {
                sum += doc.getHeader().getDevStatus().getCount();
            }
            map.put(listEntry.getKey(), sum);
        }
        return map;
    }

    public int getReturnedIssuesCount() {
        int count = 0;
        Map<LocalDate, Long> map = splitReturnedIssuesByDay();
        for (Map.Entry<LocalDate, Long> m : map.entrySet()) {
            count += m.getValue();
        }
        return count;
    }

    public Map<LocalDate, Long> splitPassedIssuesByDay() {
        return getPassedIssues().stream().collect(Collectors.groupingBy(DocumentModel::getUpdated,
                Collectors.counting()));
    }
}