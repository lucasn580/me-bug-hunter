package meceap.appbuilder.service;

import br.com.me.appbuilder.Execucao;
import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.appbuilder.manager.ExecucaoManager;
import br.com.me.ceap.util.criteria.FilterCriteria;
import br.com.me.ceap.util.criteria.operators.Operator;
import meceap.appbuilder.model.BuildResultCommand;
import meceap.appbuilder.model.ExecutionCommand;
import meceap.appbuilder.model.ExecutionType;
import meceap.appbuilder.model.ExecutorModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class ExecutionService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private ExecucaoManager execucaoManager;
    private BDDService bddService;
    private List<ExecutorModel> executores;
    private JenkinsAPIService jenkinsAPIService;

    @Inject
    public void setExecucaoManager(ExecucaoManager execucaoManager) {
        this.execucaoManager = execucaoManager;
    }

    @Inject
    public void setBddService(BDDService bddService) {
        this.bddService = bddService;
    }

    @Inject
    public void setJenkinsAPIService(JenkinsAPIService jenkinsAPIService) {
        this.jenkinsAPIService = jenkinsAPIService;
    }

    public void run(ExecutionCommand exec) {
        log.info("Criando um registro de Execucao dos cenários passados...");

        if (exec.getType().equals(ExecutionType.TEST_CASE.getType())) {
            Execucao execucao = ExecutionCommand.mapFrom(exec);
            execucaoManager.save(execucao);

            log.info("EXECUCAO: " + execucao.getId());
            exec.setId(execucao.getId());
            log.info(jenkinsAPIService.connect(exec, null));

        } else if (exec.getType().equals(ExecutionType.BDD.getType())) {
            String[] result = exec.getIds().split(",");
            for (String suite : result) {
                Execucao execucao = ExecutionCommand.mapFrom(exec);
                execucaoManager.save(execucao);
                log.info("EXECUCAO: " + execucao.getId());

                BDD bdd = bddService.getByCustomer(suite);
                exec.setId(execucao.getId());
                exec.setProject(bdd.getBDD().getProjeto().getNome());

                log.info(jenkinsAPIService.connect(exec, suite));
            }
        }
    }

    public List<ExecutionCommand> getBuilds(String type) {
        log.info("Recuperando todos os builds por tipo...");
        FilterCriteria criteria = new FilterCriteria();
        criteria.and("tipo", Operator.is, type);

        return getExecutionsModel(execucaoManager.getByCriteria(criteria));
    }

    public List<ExecutionCommand> getUserBuilds(String user) {
        log.info("Recuperando os builds do usuário...");

        FilterCriteria criteria = new FilterCriteria();
        criteria.and("usuario", Operator.is, user);

        List<Execucao> execs = execucaoManager.getByCriteria(criteria);
        return getExecutionsModel(execs);
    }

    public List<ExecutorModel> getExecutores() {
        log.info("Recuperando os usuários que já geraram execuções...");
        executores = new ArrayList<>();
        for (Execucao execucao : execucaoManager.getAll()) {
            if (execucao.getUsuario() != null && !execucao.getUsuario().equals("")) {
                if (!verificarSeListaContemExecutor(execucao))
                    executores.add(new ExecutorModel(execucao));
            }
        }
        return executores;
    }

    private boolean verificarSeListaContemExecutor(final Execucao ex) {
        return executores.stream().anyMatch(input ->
                (input.getAnalistaEmail().equals(ex.getUsuario()) && input.addExecucao(ex))
        );
    }

    private List<ExecutionCommand> getExecutionsModel(List<Execucao> lista) {
        log.info("Gerando lista de modelo de Execuções...");
        List<ExecutionCommand> execs = new ArrayList<>();
        for (Execucao ex : lista)
            execs.add(ExecutionCommand.mapFrom(ex));

        execs.sort(Collections.reverseOrder());
        return execs;
    }

    public BuildResultCommand getMonthlyBuildsResults(String type) {
        List<ExecutionCommand> resultList = new ArrayList<>();
        List<ExecutionCommand> executions = getBuilds(type);

        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH);
        List<Integer> fridayList = getExecutionDays(cal, Calendar.FRIDAY);

        fridayList.forEach(f -> {
            AtomicReference<ExecutionCommand> exec = new AtomicReference<>(new ExecutionCommand());
            executions.stream()
                    .sorted(Comparator.comparing(e -> e.getDate().getTime()))
                    .forEach(e -> {
                        Calendar cal1 = Calendar.getInstance();
                        cal1.setTime(e.getDate());
                        int execMonth = cal1.get(Calendar.MONTH);
                        if (month == execMonth && f == cal1.get(Calendar.DAY_OF_MONTH))
                            exec.set(e);
                    });
            resultList.add(exec.get());
        });

        BuildResultCommand resultCommand = new BuildResultCommand();
        resultCommand.setDays(fridayList);
        resultCommand.setExecutionCommandList(resultList);

        return resultCommand;
    }

    private List<Integer> getExecutionDays(Calendar cal, int weekDay){
        List<Integer> days = new ArrayList<>();
        int month = cal.get(Calendar.MONTH);
        do {
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek == weekDay)
                days.add(cal.get(Calendar.DAY_OF_MONTH));
            cal.add(Calendar.DAY_OF_MONTH, 1);
        } while (cal.get(Calendar.MONTH) == month);
        return days;
    }
}