package meceap.appbuilder.service;

import br.com.me.appbuilder.document.release.Release;
import br.com.me.ceap.dynamic.extension.base.MetaDocument;
import br.com.me.ceap.dynamic.extension.model.base.UserCalendar;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.service.CalendarService;
import meceap.appbuilder.model.calendar.DueDateCalendarEvent;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public abstract class DueDateCalendarService extends BaseCalendarService {
// ------------------------------ FIELDS ------------------------------

    @Inject
    private CalendarService calendarService;

// -------------------------- OTHER METHODS --------------------------

    public void appendToCalendar(MetaDocument metaDocument, DateTime start, DateTime end, String summary, UserHumanBeing userHumanBeing, String calendarName, String color) {
        UserCalendar userCalendar = calendarService.get(userHumanBeing, calendarName);
        if (userCalendar == null)
            userCalendar = createCalendar(userHumanBeing, calendarName, color);

        if (start == null || end == null) {
            clearEvents(userCalendar, metaDocument);
            return;
        }

        List<DueDateCalendarEvent> event = calendarService.getEvents(userCalendar, metaDocument, DueDateCalendarEvent.class);
        DueDateCalendarEvent releaseEvent = null;
        DueDateCalendarEvent prodEvent = null;

        if(!event.isEmpty()) {
            releaseEvent = event.get(0);
            if(event.size()>1)
                prodEvent = event.get(1);
        }


        if (releaseEvent == null)
            releaseEvent = new DueDateCalendarEvent();

        releaseEvent.setDocumentId(metaDocument.getId());
        releaseEvent.setCalendar(userCalendar);
        releaseEvent.setTitle(String.format("(# %s) Release %s", metaDocument.getId(), summary));
        releaseEvent.setStart(start);
        releaseEvent.setEnd(end);
        releaseEvent.setDocType(metaDocument.getClass().getSimpleName());

        calendarService.save(releaseEvent, metaDocument);

        if (metaDocument instanceof Release) {
            if (prodEvent == null)
                prodEvent = new DueDateCalendarEvent();

            prodEvent.setDocumentId(metaDocument.getId() + "PRD");
            prodEvent.setCalendar(userCalendar);
            prodEvent.setTitle(String.format("Release %s PRODUÇÃO", summary));
            prodEvent.setStart(end);
            prodEvent.setEnd(end.plusHours(1));
            prodEvent.setDocType(metaDocument.getClass().getSimpleName());
            prodEvent.setColor(ReleaseCalendarService.COLOR_PROD);

            calendarService.save(prodEvent, metaDocument);
        }
    }

    public abstract void changeDueDate(DueDateCalendarEvent event);
}