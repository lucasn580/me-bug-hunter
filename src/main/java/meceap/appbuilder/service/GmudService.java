package meceap.appbuilder.service;

import meceap.appbuilder.integration.MisyTrackerService;
import meceap.appbuilder.model.GmudMensalModel;
import meceap.appbuilder.model.external.misytracker.*;
import meceap.appbuilder.util.CustomCriteria;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class GmudService {

    private MisyTrackerService misyTrackerService;

    @Inject
    public void setMisyTrackerService(MisyTrackerService misyTrackerService) {
        this.misyTrackerService = misyTrackerService;
    }

    public List<String> getLatestYears(){
        List<String> years = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        int currentYear = now.get(Calendar.YEAR);
        years.add(Integer.toString(currentYear));
        years.add(Integer.toString(currentYear - 1));

        return years;
    }

    public List<GmudMensalModel> getGmudsByYear(String year){

        String dataInicial = String.format("%s-01-01", year);
        String dataFinal = String.format("%s-12-31", year);

        List<String[]> criterias = new ArrayList<>();
        String[] criteria = {"greaterThanEqual", dataInicial, "created"};
        String[] criteria1 = {"lesserThanEqual", dataFinal, "created"};
        String[] criteria2 = {"is", "Gestão Mudança", "header.department.name"};
        criterias.add(criteria);
        criterias.add(criteria1);
        criterias.add(criteria2);

        IssueResultModel issues = misyTrackerService.getIssues(CustomCriteria.jsonRequest(criterias, "AND"));

        List<GmudMensalModel> lista = new ArrayList<>();

        if (issues != null)
            for(int i = 1; i <= 12; i++) {

                GmudMensalModel gmud = new GmudMensalModel();
                List<DocumentModel> issuesMensal = new ArrayList<>();
                IssueResultModel issuesModel = new IssueResultModel();

                for(DocumentModel doc : issues.getDocument()){
                    int mes = doc.getCreated().getMonthValue();
                    if(mes == i) {
                        String categoria = doc.getHeader().getCategory().getDescription();
                        if(categoria.equals("Mudança Emergencial") || categoria.equals("Mudança Planejada"))
                            issuesMensal.add(doc);
                    }
                }

                issuesModel.setDocuments(issuesMensal);
                issuesModel.setTotalHits(issuesMensal.size());

                gmud.setAno(year);
                gmud.setMes(new DateFormatSymbols(new Locale("pt", "BR")).getMonths()[i-1]);
                gmud.setIssues(issuesModel);

                lista.add(gmud);
            }
        return lista;
    }
}