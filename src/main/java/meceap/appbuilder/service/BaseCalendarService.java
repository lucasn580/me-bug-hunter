package meceap.appbuilder.service;

import br.com.me.ceap.dynamic.extension.base.MetaDocument;
import br.com.me.ceap.dynamic.extension.calendar.BaseCalendarEvent;
import br.com.me.ceap.dynamic.extension.model.base.UserCalendar;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.dynamic.extension.service.CalendarService;
import br.com.me.ceap.dynamic.extension.service.CompanyService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
@Service
public class BaseCalendarService {
// ------------------------------ FIELDS ------------------------------

    @Inject
    private CalendarService calendarService;

    @Inject
    private CompanyService companyService;

// -------------------------- OTHER METHODS --------------------------

    public void clearEvents(UserCalendar userCalendar, MetaDocument metaDocument) {
        List<BaseCalendarEvent> events = calendarService.getEvents(userCalendar, metaDocument);
        for (BaseCalendarEvent event : events) {
            calendarService.delete(event);
        }
    }

    public UserCalendar createCalendar(UserHumanBeing userHumanBeing, String calendarName, String color) {
        UserCalendar calendar = new UserCalendar();
        calendar.setName(calendarName);
        calendar.setDescription(calendarName);
        calendar.setOwner(userHumanBeing);
        calendar.setColor(color);
        calendar.setPrivateGroups(new ArrayList<>(companyService.get(userHumanBeing)));
        calendarService.save(calendar);
        return calendar;
    }
}