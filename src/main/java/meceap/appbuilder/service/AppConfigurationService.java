package meceap.appbuilder.service;

import br.com.me.appbuilder.AppConfig;
import br.com.me.appbuilder.manager.AppConfigManager;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class AppConfigurationService {

    private AppConfigManager manager;

    @Inject
    public void setManager(AppConfigManager manager) {
        this.manager = manager;
    }

    public String getAppManagerByPK(String key){
        return manager.getByPK(key).getValue();
    }

    public AppConfig saveManagerByPK(String key, String value){
        AppConfig configuration = manager.getByPK(key);
        configuration.setValue(value);

        return manager.save(configuration);
    }
}