package meceap.appbuilder.security;

/**
 * @author lucasns
 * @since #1.0
 */
public class RolePermission {
// ------------------------------ FIELDS ------------------------------

    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
}