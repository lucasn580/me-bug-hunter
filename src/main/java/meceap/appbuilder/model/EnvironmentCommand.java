package meceap.appbuilder.model;

import br.com.me.appbuilder.Ambiente;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class EnvironmentCommand {

// ------------------------------ FIELDS ------------------------------

    private String name;
    private boolean active;
    private List<Ambiente> environments = new ArrayList<>();

// -------------------------- STATIC METHODS --------------------------

    public EnvironmentCommand() {

    }

    public EnvironmentCommand(String environment) {
        this.name = environment;
    }

    public static EnvironmentCommand mapFrom(List<Ambiente> ambientes) {
        EnvironmentCommand command = new EnvironmentCommand();
        command.environments.addAll(ambientes);
        return command;
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ambiente> getEnvironments() {
        return environments;
    }

    public void setEnvironments(List<Ambiente> environments) {
        this.environments = environments;
    }
}