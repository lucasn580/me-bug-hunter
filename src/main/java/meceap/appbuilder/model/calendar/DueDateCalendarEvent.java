package meceap.appbuilder.model.calendar;

import br.com.me.ceap.dynamic.extension.calendar.BaseCalendarEvent;

/**
 * @author lucasns
 * @since #1.0
 */
public class DueDateCalendarEvent extends BaseCalendarEvent {
// ------------------------------ FIELDS ------------------------------

    private String documentId;

    private String docType;

// --------------------------- CONSTRUCTORS ---------------------------

    public DueDateCalendarEvent() {
//        this.setRenderer("DueDateCalendarEventRenderer");
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
}