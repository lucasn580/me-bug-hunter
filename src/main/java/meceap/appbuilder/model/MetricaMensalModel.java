package meceap.appbuilder.model;

import meceap.appbuilder.model.external.metrics.MetricsResultModel;

/**
 * @author lucasns
 * @since #1.0
 */
public class MetricaMensalModel {
// ------------------------------ FIELDS ------------------------------

    private String mes;
    private MetricsResultModel metrics;

// --------------------------- CONSTRUCTORS ---------------------------

    public MetricaMensalModel(){
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public MetricsResultModel getMetrics() {
        return metrics;
    }

    public void setMetrics(MetricsResultModel metrics) {
        this.metrics = metrics;
    }
}