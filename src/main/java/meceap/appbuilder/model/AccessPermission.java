package meceap.appbuilder.model;

import br.com.me.ceap.util.annotations.SimpleLabel;

/**
 * @author lucasns
 * @since #1.0
 */
public class AccessPermission {
// ------------------------------ FIELDS ------------------------------

    @SimpleLabel("Allow to create custom automation flows")
    public static final String PERM_BUILD_AUTOMATION_FLOW = "PERM_BUILD_AUTOMATION_FLOW";
    @SimpleLabel("Allow to execute automation flows")
    public static final String PERM_RUN_AUTOMATION_FLOW = "PERM_RUN_AUTOMATION_FLOW";
    @SimpleLabel("Allow to see management screen")
    public static final String PERM_ALLOW_SEE_MANAGEMENT = "PERM_ALLOW_SEE_MANAGEMENT";
    @SimpleLabel("Allow to admin Saudetime document")
    public static final String PERM_HEALTH_TEAM_DOCUMENT = "PERM_HEALTH_TEAM_DOCUMENT";
    @SimpleLabel("Allow to register metrics")
    public static final String PERM_REGISTER_METRIC = "PERM_REGISTER_METRIC";
    @SimpleLabel("Allow to create Release documents")
    public static final String PERM_CREATE_RELEASE = "PERM_CREATE_RELEASE";
    @SimpleLabel("Allow to admin DocumentoGmud document")
    public static final String PERM_COMMITTEE_DOCUMENT = "PERM_COMMITTEE_DOCUMENT";
    @SimpleLabel("Allow to admin Branch document")
    public static final String PERM_BRANCH_DOCUMENT = "PERM_BRANCH_DOCUMENT";
    @SimpleLabel("Allow to admin Refresh document")
    public static final String PERM_REFRESH_DOCUMENT = "PERM_REFRESH_DOCUMENT";
    @SimpleLabel("Allow to admin Controleacessos document")
    public static final String PERM_CONTROLS_DOCUMENT = "PERM_CONTROLS_DOCUMENT";
    @SimpleLabel("Allow to create Buildspontuais documentst")
    public static final String PERM_CREATE_BUILD_DOCUMENT = "PERM_CREATE_BUILD_DOCUMENT";
    @SimpleLabel("Allow to see dashboard manager")
    public static final String PERM_ALLOW_SEE_DASHBOARD = "PERM_ALLOW_SEE_DASHBOARD";
    @SimpleLabel("Allow to admin BranchMECEAP document")
    public static final String PERM_BRANCHMECEAP_DOCUMENT = "PERM_BRANCHMECEAP_DOCUMENT";
    @SimpleLabel("Allow to admin BranchApps document")
    public static final String PERM_BRANCHAPPS_DOCUMENT = "PERM_BRANCHAPPS_DOCUMENT";
    @SimpleLabel("Allow to see Kanban")
    public static final String PERM_ALLOW_SEE_KANBAN = "PERM_ALLOW_SEE_KANBAN";
}