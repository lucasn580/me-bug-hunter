package meceap.appbuilder.model.external;

/**
 * @author lucasns
 * @since #1.0
 */
public class OwnerModel {
// ------------------------------ FIELDS ------------------------------

    private int id;
    private String name;
    private String email;
    private String uniqueIdentifier;
    private String timeZone;

// --------------------------- CONSTRUCTORS ---------------------------

    private OwnerModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}