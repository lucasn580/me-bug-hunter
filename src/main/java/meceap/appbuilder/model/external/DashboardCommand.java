package meceap.appbuilder.model.external;

import meceap.appbuilder.model.external.misytracker.DocumentModel;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class DashboardCommand {
    private String currentRelease;
    private List<DocumentModel> allReleaseIssues;
    private Integer passedReleaseIssues;
    private Integer returnedReleaseIssues;
    private List<DocumentModel> allBuildsIssues;
    private Integer impactsBuildsIssues;
    private String urlIssueFilterBase;
    private String urlBuildFilterBase;

    public String getCurrentRelease() {
        return currentRelease;
    }

    public void setCurrentRelease(String currentRelease) {
        this.currentRelease = currentRelease;
    }

    public List<DocumentModel> getAllReleaseIssues() {
        return allReleaseIssues;
    }

    public void setAllReleaseIssues(List<DocumentModel> allReleaseIssues) {
        this.allReleaseIssues = allReleaseIssues;
    }

    public Integer getPassedReleaseIssues() {
        return passedReleaseIssues;
    }

    public void setPassedReleaseIssues(Integer passedReleaseIssues) {
        this.passedReleaseIssues = passedReleaseIssues;
    }

    public Integer getReturnedReleaseIssues() {
        return returnedReleaseIssues;
    }

    public void setReturnedReleaseIssues(Integer returnedReleaseIssues) {
        this.returnedReleaseIssues = returnedReleaseIssues;
    }

    public List<DocumentModel> getAllBuildsIssues() {
        return allBuildsIssues;
    }

    public void setAllBuildsIssues(List<DocumentModel> allBuildsIssues) {
        this.allBuildsIssues = allBuildsIssues;
    }

    public Integer getImpactsBuildsIssues() {
        return impactsBuildsIssues;
    }

    public void setImpactsBuildsIssues(Integer impactsBuildsIssues) {
        this.impactsBuildsIssues = impactsBuildsIssues;
    }

    public String getUrlIssueFilterBase() {
        return urlIssueFilterBase;
    }

    public void setUrlIssueFilterBase(String urlIssueFilterBase) {
        this.urlIssueFilterBase = urlIssueFilterBase;
    }

    public String getUrlBuildFilterBase() {
        return urlBuildFilterBase;
    }

    public void setUrlBuildFilterBase(String urlBuildFilterBase) {
        this.urlBuildFilterBase = urlBuildFilterBase;
    }

    public Long getOpenIssues() {
        return allReleaseIssues.stream().filter(doc ->
                "aberto".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getInProgressIssues() {
        return allReleaseIssues.stream().filter(doc ->
                "em andamento".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getDevelopingIssues() {
        return allReleaseIssues.stream().filter(doc ->
                "em desenvolvimento".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getTestingIssues() {
        return allReleaseIssues.stream().filter(doc ->
                "em testes".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getWaitingBuildProdIssues() {
        return allReleaseIssues.stream().filter(doc ->
                "aguardando build para Produção".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getOpenBuilds() {
        return allBuildsIssues.stream().filter(doc ->
                "aberto".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getInProgressBuilds() {
        return allBuildsIssues.stream().filter(doc ->
                "em andamento".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getDevelopingBuilds() {
        return allBuildsIssues.stream().filter(doc ->
                "em desenvolvimento".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getTestingBuilds() {
        return allBuildsIssues.stream().filter(doc ->
                "em testes".equals(doc.getHeader().getStatus().getName())).count();
    }

    public Long getWaitingBuildProdBuilds() {
        return allBuildsIssues.stream().filter(doc ->
                "aguardando build para Produção".equals(doc.getHeader().getStatus().getName())).count();
    }
}