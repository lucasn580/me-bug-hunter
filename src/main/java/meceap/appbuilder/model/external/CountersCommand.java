package meceap.appbuilder.model.external;

/**
 * @author lucasns
 * @since #1.0
 */
public class CountersCommand {
    private Integer nonCompliantProd;
    private Integer nonCompliantDetectedTest;
    private Integer automRegressionTestsSuccess;
    private Integer retestIndex;
    private Integer amountPointBuild;
    private Integer relationshipMetric;

    public Integer getNonCompliantProd() {
        return nonCompliantProd;
    }

    public void setNonCompliantProd(Integer nonCompliantProd) {
        this.nonCompliantProd = nonCompliantProd;
    }

    public Integer getNonCompliantDetectedTest() {
        return nonCompliantDetectedTest;
    }

    public void setNonCompliantDetectedTest(Integer nonCompliantDetectedTest) {
        this.nonCompliantDetectedTest = nonCompliantDetectedTest;
    }

    public Integer getAutomRegressionTestsSuccess() {
        return automRegressionTestsSuccess;
    }

    public void setAutomRegressionTestsSuccess(Integer automRegressionTestsSuccess) {
        this.automRegressionTestsSuccess = automRegressionTestsSuccess;
    }

    public Integer getRetestIndex() {
        return retestIndex;
    }

    public void setRetestIndex(Integer retestIndex) {
        this.retestIndex = retestIndex;
    }

    public Integer getAmountPointBuild() {
        return amountPointBuild;
    }

    public void setAmountPointBuild(Integer amountPointBuild) {
        this.amountPointBuild = amountPointBuild;
    }

    public Integer getRelationshipMetric() {
        return relationshipMetric;
    }

    public void setRelationshipMetric(Integer relationshipMetric) {
        this.relationshipMetric = relationshipMetric;
    }
}