package meceap.appbuilder.model.external.metrics;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class MetricsResultModel {
// ------------------------------ FIELDS ------------------------------

    @JsonProperty("documents")
    private List<DocumentModel> documents;
    private int totalHits;

// --------------------------- CONSTRUCTORS ---------------------------

    public MetricsResultModel(){
    }

    public MetricsResultModel(int capability) {
        documents = new ArrayList<>(capability);

        for(int i = 0; i < capability; i++)
            documents.add(new DocumentModel());
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public List<DocumentModel> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentModel> documents) {
        this.documents = documents;
    }

    public void setDocument(int index, DocumentModel document) {
        this.documents.set(index, document);
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }
}