package meceap.appbuilder.model.external.metrics;

/**
 * @author lucasns
 * @since #1.0
 */
public enum MetricType {

    NOT_CONFORM_PROD("96"),
    NOT_CONFORM_DETECTED_TESTS("97"),
    AUTOM_REGRESSION_TESTS_SUCCESS("99"),
    RETEST_INDEX("471"),
    AMOUNT_POINT_BUILDS("472"),
    RELATIONSHIP_METRIC("101");

    private String metricaDocId;

    MetricType(String metricaDocId){
        this.metricaDocId = metricaDocId;
    }

    public String getMetricaDocId() {
        return metricaDocId;
    }
}