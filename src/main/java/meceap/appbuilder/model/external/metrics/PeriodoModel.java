package meceap.appbuilder.model.external.metrics;

import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class PeriodoModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    private String descricao;
    private DateTime dataInicial;
    private DateTime dataFinal;

// --------------------------- CONSTRUCTORS ---------------------------

    public PeriodoModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataInicial() {
        return dataInicial.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public void setDataInicial(DateTime dataInicial) {
        this.dataInicial = dataInicial;
    }

    public LocalDate getDataFinal() {
        return dataFinal.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public void setDataFinal(DateTime dataFinal) {
        this.dataFinal = dataFinal;
    }
}