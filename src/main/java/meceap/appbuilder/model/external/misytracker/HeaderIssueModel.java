package meceap.appbuilder.model.external.misytracker;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class HeaderIssueModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    private DateTime closeDate;
    private DateTime startDate;
    @JsonProperty("status")
    private StatusIssueModel status;
    private String description;
    private String priorityImg;
    @JsonProperty("priority")
    private PriorityModel priority;
    @JsonProperty("handler")
    private HandlerModel handler;
    @JsonProperty("reporter")
    private ReporterModel reporter;
    @JsonProperty("demandingCustomer")
    private DemandingCustomerModel demandingCustomer;
    @JsonProperty("customer")
    private CustomerModel customer;
    @JsonProperty("department")
    private DepartmentModel department;
    @JsonProperty("category")
    private CategoryModel category;
    private String summary;
    @JsonProperty("classification")
    private ClassificationModel classification;
    @JsonProperty("devStatus")
    private DevStatusModel devStatus;
    private String knownErrors;

// --------------------------- CONSTRUCTORS ---------------------------

    private HeaderIssueModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public LocalDate getCloseDate() {
        if (closeDate != null)
            return closeDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCloseDate(DateTime closeDate) {
        this.closeDate = closeDate;
    }

    public LocalDate getStartDate() {
        if (startDate != null)
            return startDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public StatusIssueModel getStatus() {
        return status;
    }

    public void setStatus(StatusIssueModel status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriorityImg() {
        return priorityImg;
    }

    public void setPriorityImg(String priorityImg) {
        this.priorityImg = priorityImg;
    }

    public PriorityModel getPriority() {
        return priority;
    }

    public void setPriority(PriorityModel priority) {
        this.priority = priority;
    }

    public HandlerModel getHandler() {
        return handler;
    }

    public void setHandler(HandlerModel handler) {
        this.handler = handler;
    }

    public ReporterModel getReporter() {
        return reporter;
    }

    public void setReporter(ReporterModel reporter) {
        this.reporter = reporter;
    }

    public DemandingCustomerModel getDemandingCustomer() {
        return demandingCustomer;
    }

    public void setDemandingCustomer(DemandingCustomerModel demandingCustomer) {
        this.demandingCustomer = demandingCustomer;
    }

    public CustomerModel getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerModel customer) {
        this.customer = customer;
    }

    public DepartmentModel getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentModel department) {
        this.department = department;
    }

    public CategoryModel getCategory() {
        return category;
    }

    public void setCategory(CategoryModel category) {
        this.category = category;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public ClassificationModel getClassification() {
        return classification;
    }

    public void setClassification(ClassificationModel classification) {
        this.classification = classification;
    }

    public DevStatusModel getDevStatus() {
        return devStatus;
    }

    public void setDevStatus(DevStatusModel devStatus) {
        this.devStatus = devStatus;
    }

    public String getKnownErrors() {
        return knownErrors;
    }

    public void setKnownErrors(String knownErrors) {
        this.knownErrors = knownErrors;
    }
}