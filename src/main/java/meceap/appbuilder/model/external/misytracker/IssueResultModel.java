package meceap.appbuilder.model.external.misytracker;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class IssueResultModel {
// ------------------------------ FIELDS ------------------------------

    @JsonProperty("documents")
    private List<DocumentModel> documents = new ArrayList<>();
    private int totalHits;

// --------------------------- CONSTRUCTORS ---------------------------

    public IssueResultModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public List<DocumentModel> getDocument() {
        return documents;
    }

    public void setDocuments(List<DocumentModel> documents) {
        this.documents = documents;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }
}