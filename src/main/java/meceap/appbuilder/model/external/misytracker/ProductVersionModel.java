package meceap.appbuilder.model.external.misytracker;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class ProductVersionModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    @JsonProperty("product")
    private ProductModel productModel;
    private String name;
    private String description;
    @JsonProperty("status")
    private StatusProductVersionModel statusProductVersionModel;
    private DateTime qaDate;
    private DateTime dueDate;
    private String vcsBranch;

// --------------------------- CONSTRUCTORS ---------------------------

    public ProductVersionModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public ProductModel getProductModel() {
        return productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StatusProductVersionModel getStatusProductVersionModel() {
        return statusProductVersionModel;
    }

    public void setStatusProductVersionModel(StatusProductVersionModel statusProductVersionModel) {
        this.statusProductVersionModel = statusProductVersionModel;
    }

    public LocalDate getQaDate() {
        if (qaDate != null)
            return qaDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setQaDate(DateTime qaDate) {
        this.qaDate = qaDate;
    }

    public LocalDate getDueDate() {
        if (dueDate != null)
            return dueDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getVcsBranch() {
        return vcsBranch;
    }

    public void setVcsBranch(String vcsBranch) {
        this.vcsBranch = vcsBranch;
    }
}