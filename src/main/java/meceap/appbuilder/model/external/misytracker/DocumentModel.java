package meceap.appbuilder.model.external.misytracker;

import com.fasterxml.jackson.annotation.JsonProperty;
import meceap.appbuilder.model.external.OwnerModel;
import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class DocumentModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    private long internalId;
    @JsonProperty("owner")
    private OwnerModel owner;
    private String parent;
    private boolean privateShare;
    private boolean archived;
    private int documentTypeId;
    @JsonProperty("header")
    private HeaderIssueModel header;

// --------------------------- CONSTRUCTORS ---------------------------

    private DocumentModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public long getInternalId() {
        return internalId;
    }

    public void setInternalId(long internalId) {
        this.internalId = internalId;
    }

    public OwnerModel getOwner() {
        return owner;
    }

    public void setOwner(OwnerModel owner) {
        this.owner = owner;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public boolean isPrivateShare() {
        return privateShare;
    }

    public void setPrivateShare(boolean privateShare) {
        this.privateShare = privateShare;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int getDocumentTypeId() {
        return documentTypeId;
    }

    public void setDocumentTypeId(int documentTypeId) {
        this.documentTypeId = documentTypeId;
    }

    public HeaderIssueModel getHeader() {
        return header;
    }

    public void setHeader(HeaderIssueModel header) {
        this.header = header;
    }
}