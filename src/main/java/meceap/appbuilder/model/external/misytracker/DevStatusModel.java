package meceap.appbuilder.model.external.misytracker;

import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class DevStatusModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    private String name;
    private int count;

// --------------------------- CONSTRUCTORS ---------------------------

    private DevStatusModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}