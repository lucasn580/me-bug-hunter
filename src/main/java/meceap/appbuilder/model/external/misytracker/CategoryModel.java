package meceap.appbuilder.model.external.misytracker;

import org.joda.time.DateTime;

import java.time.LocalDate;
import java.time.ZoneId;

/**
 * @author lucasns
 * @since #1.0
 */
public class CategoryModel {
// ------------------------------ FIELDS ------------------------------

    private String id;
    private int version;
    private DateTime created;
    private DateTime updated;
    private String description;

// --------------------------- CONSTRUCTORS ---------------------------

    private CategoryModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LocalDate getCreated() {
        if (created != null)
            return created.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public LocalDate getUpdated() {
        if (updated != null)
            return updated.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        else return null;
    }

    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}