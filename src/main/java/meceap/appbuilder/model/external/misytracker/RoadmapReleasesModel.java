package meceap.appbuilder.model.external.misytracker;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class RoadmapReleasesModel {
// ------------------------------ FIELDS ------------------------------

    @JsonProperty("objects")
    private List<ProductVersionModel> productVersionModels = new ArrayList<>();
    private int totalHits;

// --------------------------- CONSTRUCTORS ---------------------------

    public RoadmapReleasesModel() {
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public List<ProductVersionModel> getProductVersionModels() {
        return productVersionModels;
    }

    public void setProductVersionModels(List<ProductVersionModel> productVersionModels) {
        this.productVersionModels = productVersionModels;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }
}