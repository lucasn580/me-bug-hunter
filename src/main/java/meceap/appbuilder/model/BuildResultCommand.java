package meceap.appbuilder.model;

import java.util.List;

/**
 * @author lucasns
 * @since #1.6
 */
public class BuildResultCommand {
// ------------------------------ FIELDS ------------------------------

    private List<Integer> dayList;
    private List<ExecutionCommand> executionCommandList;

// --------------------------- CONSTRUCTORS ---------------------------

    public BuildResultCommand(){

    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public List<Integer> getDays() {
        return dayList;
    }

    public void setDays(List<Integer> days) {
        this.dayList = days;
    }

    public List<ExecutionCommand> getExecutionCommandList() {
        return executionCommandList;
    }

    public void setExecutionCommandList(List<ExecutionCommand> executionCommandList) {
        this.executionCommandList = executionCommandList;
    }
}