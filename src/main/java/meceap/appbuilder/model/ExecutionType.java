package meceap.appbuilder.model;

/**
 * @author lucasns
 * @since #1.0
 */
public enum ExecutionType {

    TEST_CASE("testCase"),
    BDD("bdd");

    private String type;

    ExecutionType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }
}