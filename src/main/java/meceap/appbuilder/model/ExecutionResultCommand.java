package meceap.appbuilder.model;

import meceap.appbuilder.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class ExecutionResultCommand {
// ------------------------------ FIELDS ------------------------------

    private String result;
    private double rating;
    private List<String> passed;
    private List<String> failed;
    private List<String> failures;
    private List<String> ignored;

// --------------------------- CONSTRUCTORS ---------------------------

    public ExecutionResultCommand(){
    }

    public static ExecutionResultCommand mapFrom(String result) {
        ExecutionResultCommand command = new ExecutionResultCommand();
        command.passed = new ArrayList<>();
        command.failed = new ArrayList<>();
        command.ignored = new ArrayList<>();
        command.failures = new ArrayList<>();
        command.tratarResultado(command, result);
        return command;
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public List<String> getPassed() {
        return passed;
    }

    public void setPassed(List<String> passed) {
        this.passed = passed;
    }

    public List<String> getFailed() {
        return failed;
    }

    public void setFailed(List<String> failed) {
        this.failed = failed;
    }

    public List<String> getIgnored() {
        return ignored;
    }

    public void setIgnored(List<String> ignored) {
        this.ignored = ignored;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<String> getFailures() {
        return failures;
    }

    public void setFailures(List<String> failures) {
        this.failures = failures;
    }

    private void tratarResultado(ExecutionResultCommand command, String resultado){
        if (resultado != null && resultado.length() > 0) {
            resultado = resultado.substring(0, resultado.length() - 1);
            String[] cts = resultado.split("§");

            for(String ct : cts){
                String[] result = ct.split("£");
                switch (result[1]) {
                    case "passed":
                        command.passed.add(result[0]);
                        break;
                    case "failed":
                        command.failed.add(result[0]);
                        break;
                    case "ignored":
                        command.ignored.add(result[0]);
                        break;
                }
                if(result.length > 2)
                    command.failures.add(result[0]+"-"+result[2]);
            }
            double total = getPassed().size() + getFailed().size() + getIgnored().size();
            command.rating = (getPassed().size() / total) * 100;
            command.result = String.format("%s %%", StringUtils.formatarDecimal(rating));
        } else
            command.result = "Pendente";
    }
}