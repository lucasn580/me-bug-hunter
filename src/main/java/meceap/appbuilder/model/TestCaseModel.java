package meceap.appbuilder.model;

import br.com.me.appbuilder.document.casodeTeste.PageObjectsItemInCasodeTeste;
import br.com.me.ceap.util.annotations.DateTimeFormat;
import br.com.me.ceap.util.annotations.IgnoredFieldColumn;
import br.com.me.ceap.util.annotations.SimpleLabel;
import org.joda.time.DateTime;

import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class TestCaseModel {
// ------------------------------ FIELDS ------------------------------

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.id")
    private String id;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.idwf")
    private String idWF;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.status")
    private String status;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.project")
    private String projeto;

    @SimpleLabel("testcase.field.customer")
    private String cliente;

    @SimpleLabel("testcase.field.flow")
    private String fluxo;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.scenario.name")
    private String nomeCenario;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.goal")
    private String objetivo;

    @IgnoredFieldColumn
    @SimpleLabel("testcase.field.precondition")
    private String preCondicao;

    @DateTimeFormat(showHours = true)
    @SimpleLabel("testcase.field.created")
    private DateTime dataCriacao;

    @DateTimeFormat(showHours = true)
    @SimpleLabel("testcase.field.updated")
    private DateTime dataAlteracao;

    @IgnoredFieldColumn
    private List<PageObjectsItemInCasodeTeste> pageObjects;

// --------------------------- CONSTRUCTORS ---------------------------

    public TestCaseModel() {

    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdWF() {
        return idWF;
    }

    public void setIdWF(String idWF) {
        this.idWF = idWF;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProjeto() {
        return projeto;
    }

    public void setProjeto(String projeto) {
        this.projeto = projeto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getFluxo() {
        return fluxo;
    }

    public void setFluxo(String fluxo) {
        this.fluxo = fluxo;
    }

    public String getNomeCenario() {
        return nomeCenario;
    }

    public void setNomeCenario(String nomeCenario) {
        this.nomeCenario = nomeCenario;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getPreCondicao() {
        return preCondicao;
    }

    public void setPreCondicao(String preCondicao) {
        this.preCondicao = preCondicao;
    }

    public DateTime getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(DateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public DateTime getDataAlteracao() {
        return dataAlteracao;
    }

    public void setDataAlteracao(DateTime dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public List<PageObjectsItemInCasodeTeste> getPageObjects() {
        return pageObjects;
    }

    public void setPageObjects(List<PageObjectsItemInCasodeTeste> pageObjects) {
        this.pageObjects = pageObjects;
    }
}