package meceap.appbuilder.model;

import br.com.me.appbuilder.document.bDD.BDD;
import br.com.me.appbuilder.document.casodeTeste.CasodeTeste;
import br.com.me.ceap.util.annotations.IgnoredFieldColumn;
import br.com.me.ceap.util.annotations.SimpleLabel;
import br.com.me.ceap.util.annotations.SimpleTooltip;

import javax.persistence.OrderBy;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class ClienteModel {
// ------------------------------ FIELDS ------------------------------

    @SimpleLabel("customer.field.name")
    @SimpleTooltip("customer.field.name.description")
    private String cliente;

    @OrderBy("DESC")
    @SimpleLabel("customer.field.automated")
    @SimpleTooltip("customer.field.automated.description")
    private int camposMapeados;

    @IgnoredFieldColumn
    private int qtdCenarios;

    @IgnoredFieldColumn
    private List<TestCaseModel> casosDeTeste = new ArrayList<>();

    @IgnoredFieldColumn
    private List<BDD> bdd = new ArrayList<>();

// --------------------------- CONSTRUCTORS ---------------------------

    public ClienteModel(CasodeTeste caso){
        this.cliente = caso.getCasodeTeste().getCliente().getNome();
        addCasosDeTeste(caso);
    }

    public ClienteModel(BDD bdd){
        this.cliente = bdd.getBDD().getCliente().getNome();
        addBDD(bdd);
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getCamposMapeados() {
        return camposMapeados;
    }

    public void setCamposMapeados(int camposMapeados) {
        this.camposMapeados = camposMapeados;
    }

    public int getQtdCenarios() {
        return qtdCenarios;
    }

    public void setQtdCenarios(int qtdCenarios) {
        this.qtdCenarios = qtdCenarios;
    }

    public List<TestCaseModel> getCasosDeTeste() {
        return casosDeTeste;
    }

    public void setCasosDeTeste(List<TestCaseModel> casosDeTeste) {
        this.casosDeTeste = casosDeTeste;
    }

    public boolean addCasosDeTeste(CasodeTeste ct) {

        if(ct != null && !ct.getCasodeTeste().getPageObjectsList().isEmpty()){

            TestCaseModel caso = new TestCaseModel();

            if (ct.getId() != null) caso.setId(ct.getId());
            else caso.setId("null");

            if (ct.getCasodeTeste().getIdworkflow() != null) caso.setIdWF(ct.getCasodeTeste().getIdworkflow());
            else caso.setIdWF("null");

            if (ct.getCasodeTeste().getStatus() != null) caso.setStatus(ct.getCasodeTeste().getStatus().getStatus());
            else caso.setStatus("null");

            if (ct.getCasodeTeste().getProjeto() != null) caso.setProjeto(ct.getCasodeTeste().getProjeto().getNome());
            else caso.setProjeto("null");

            if (ct.getCasodeTeste().getCliente() != null) caso.setCliente(ct.getCasodeTeste().getCliente().getNome());
            else caso.setCliente("null");

            if (ct.getCasodeTeste().getFluxo() != null) caso.setFluxo(ct.getCasodeTeste().getFluxo().getNome());
            else caso.setFluxo("null");

            if (ct.getCasodeTeste().getNome() != null) caso.setNomeCenario(ct.getCasodeTeste().getNome());
            else caso.setNomeCenario("null");

            if (ct.getCasodeTeste().getDescricao() != null) caso.setObjetivo(ct.getCasodeTeste().getDescricao());
            else caso.setObjetivo("null");

            if (ct.getCasodeTeste().getPrecondicao() != null) caso.setPreCondicao(ct.getCasodeTeste().getPrecondicao());
            else caso.setPreCondicao("null");

            if (ct.getCreated() != null) caso.setDataCriacao(ct.getCreated());
            else caso.setDataCriacao(null);

            if (ct.getUpdated() != null) caso.setDataAlteracao(ct.getUpdated());
            else caso.setDataAlteracao(null);

            if (ct.getCasodeTeste().getPageObjectsList() != null) caso.setPageObjects(ct.getCasodeTeste().getPageObjectsList());
             else caso.setPageObjects(null);

            this.casosDeTeste.add(caso);
            this.qtdCenarios = casosDeTeste.size();
            this.camposMapeados = contaCampos();
        }
        return true;
    }

    public List<BDD> getBdd() {
        return bdd;
    }

    public void setBdd(List<BDD> bdd) {
        this.bdd = bdd;
    }

    public boolean addBDD(BDD bdd) {
        if(bdd != null && !bdd.getBDD().getPassosList().isEmpty())
            this.bdd.add(bdd);

        return true;
    }

    private int contaCampos(){
        int campos = 0;
        for(TestCaseModel ct : casosDeTeste)
            campos = campos + ct.getPageObjects().size();
        return campos;
    }
}