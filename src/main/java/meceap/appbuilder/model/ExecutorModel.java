package meceap.appbuilder.model;

import br.com.me.appbuilder.Execucao;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lucasns
 * @since #1.0
 */
public class ExecutorModel {
// ------------------------------ FIELDS ------------------------------

    private String analistaEmail;
    private int qtdExecucoes;
    private List<ExecutionCommand> execucoes = new ArrayList<>();

// --------------------------- CONSTRUCTORS ---------------------------

    public ExecutorModel(Execucao execucao){
        this.analistaEmail = execucao.getUsuario();
        addExecucao(execucao);
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getAnalistaEmail() {
        return analistaEmail;
    }

    public void setAnalistaEmail(String analistaEmail) {
        this.analistaEmail = analistaEmail;
    }

    public int getQtdExecucoes() {
        return qtdExecucoes;
    }

    public void setQtdExecucoes(int qtdExecucoes) {
        this.qtdExecucoes = qtdExecucoes;
    }

    public List<ExecutionCommand> getExecucoes() {
        return execucoes;
    }

    public boolean addExecucao(Execucao execucao) {

        if (execucao != null) {
            this.execucoes.add(ExecutionCommand.mapFrom(execucao));
            this.qtdExecucoes = execucoes.size();
            return true;
        }
        return false;
    }
}