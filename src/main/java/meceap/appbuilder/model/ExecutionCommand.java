package meceap.appbuilder.model;

import br.com.me.appbuilder.Execucao;
import org.jetbrains.annotations.NotNull;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * @author lucasns
 * @since #1.0
 */
public class ExecutionCommand implements Comparable<ExecutionCommand>{
// ------------------------------ FIELDS ------------------------------

    private String id;
    private Date date;
    private String type;
    private String ids;
    private String user;
    private String report;
    private String project;
    private EnvironmentCommand environmentCommand;
    private ExecutionResultCommand resultCommand;

// --------------------------- CONSTRUCTORS ---------------------------

    public ExecutionCommand(){

    }

    public static ExecutionCommand mapFrom(Execucao exec) {
        ExecutionCommand command = new ExecutionCommand();
        command.setId(exec.getId());
        command.setDate(exec.getData().toDate());
        command.setType(exec.getTipo());
        command.setIds(exec.getIds());
        command.setUser(exec.getUsuario());
        command.setReport(exec.getReport());
        command.setResultCommand(ExecutionResultCommand.mapFrom(exec.getResultado()));
        return command;
    }

    public static Execucao mapFrom(ExecutionCommand executionCommand){
        Execucao execucao = new Execucao();
        execucao.setData(DateTime.now());
        execucao.setIds(executionCommand.getIds());
        execucao.setResultado("");
        execucao.setUsuario(executionCommand.getUser());
        execucao.setTipo(executionCommand.getType());
        return execucao;
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public EnvironmentCommand getEnvironmentCommand() {
        return environmentCommand;
    }

    public void setEnvironmentCommand(EnvironmentCommand environmentCommand) {
        this.environmentCommand = environmentCommand;
    }

    public ExecutionResultCommand getResultCommand() {
        return resultCommand;
    }

    public void setResultCommand(ExecutionResultCommand resultCommand) {
        this.resultCommand = resultCommand;
    }

    @Override
    public int compareTo(@NotNull ExecutionCommand e) {
        return getDate().compareTo(e.getDate());
    }
}