package meceap.appbuilder.model;

import br.com.me.appbuilder.Tarefa;
import br.com.me.ceap.dynamic.extension.model.base.UserHumanBeing;
import br.com.me.ceap.validator.NotEmpty;
import org.joda.time.DateTime;

/**
 * @author lucasns
 * @since #1.0
 */
public class TaskCommand {
// ------------------------------ FIELDS ------------------------------

    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.usuario")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.usuario.descricao")
    @javax.validation.constraints.NotNull
    @br.com.me.ceap.util.annotations.ComboField(id = "id", text = "name", urlOptions = "/${customRootName}/do/Home/getUsersAdmin")
    @NotEmpty
    private UserHumanBeing userHumanBeing;
    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.descricao")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.descricao.descricao")
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min = 0, max = 9999)
    @NotEmpty
    private String descricao;
    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.data.inicio")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.data.inicio.descricao")
    private DateTime dataInicio;
    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.data.fim")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.data.fim.descricao")
    private DateTime dataFim;
    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.dependencias")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.dependencias.descricao")
    @javax.validation.constraints.Size(min = 0, max = 9999)
    private String dependencias;
    @br.com.me.ceap.util.annotations.SimpleLabel("tarefa.concluido")
    @br.com.me.ceap.util.annotations.SimpleTooltip("tarefa.concluido.descricao")
    private Boolean concluido;

    private String redirectUrl;

// -------------------------- STATIC METHODS --------------------------

    public static Tarefa loadTaskFromCommand(TaskCommand taskCommand) {
        return loadTaskFromCommand(taskCommand, new Tarefa());
    }

    @SuppressWarnings("Duplicates")
    public static Tarefa loadTaskFromCommand(TaskCommand taskCommand, Tarefa tarefa) {
        if (tarefa != null && taskCommand != null) {
            tarefa.setUsuario(taskCommand.getUserHumanBeing());
            tarefa.setDescricao(taskCommand.getDescricao());
            tarefa.setDataInicio(taskCommand.getDataInicio());
            tarefa.setDataFim(taskCommand.getDataFim());
            tarefa.setDependencias(taskCommand.getDependencias());
            tarefa.setConcluido(taskCommand.getConcluido());
        }
        return tarefa;
    }

    @SuppressWarnings("Duplicates")
    public static TaskCommand loadCommandFromTask(Tarefa tarefa) {
        TaskCommand command = new TaskCommand();

        if (tarefa != null) {
            command.setUserHumanBeing(tarefa.getUsuario());
            command.setDescricao(tarefa.getDescricao());
            command.setDataInicio(tarefa.getDataInicio());
            command.setDataFim(tarefa.getDataFim());
            command.setDependencias(tarefa.getDependencias());
            command.setConcluido(tarefa.getConcluido());
        }

        return command;
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public UserHumanBeing getUserHumanBeing() {
        return userHumanBeing;
    }

    public void setUserHumanBeing(UserHumanBeing userHumanBeing) {
        this.userHumanBeing = userHumanBeing;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public DateTime getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(DateTime dataInicio) {
        this.dataInicio = dataInicio;
    }

    public DateTime getDataFim() {
        return dataFim;
    }

    public void setDataFim(DateTime dataFim) {
        this.dataFim = dataFim;
    }

    public String getDependencias() {
        return dependencias;
    }

    public void setDependencias(String dependencias) {
        this.dependencias = dependencias;
    }

    public Boolean getConcluido() {
        return concluido;
    }

    public void setConcluido(Boolean concluido) {
        this.concluido = concluido;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}