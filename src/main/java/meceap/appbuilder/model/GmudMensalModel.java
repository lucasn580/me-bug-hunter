package meceap.appbuilder.model;

import meceap.appbuilder.model.external.misytracker.DocumentModel;
import meceap.appbuilder.model.external.misytracker.IssueResultModel;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lucasns
 * @since #1.0
 */
public class GmudMensalModel {
// ------------------------------ FIELDS ------------------------------

    private String ano;
    private String mes;
    private IssueResultModel issues;

// --------------------------- CONSTRUCTORS ---------------------------

    public GmudMensalModel(){
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public IssueResultModel getIssues() {
        return issues;
    }

    public void setIssues(IssueResultModel issues) {
        this.issues = issues;
    }

    public Long getEmergenciais() {
        return issues.getDocument().stream().filter(doc ->
                "Mudança Emergencial".equals(doc.getHeader().getCategory().getDescription())).count();
    }

    public Long getPlanejados() {
        return issues.getDocument().stream().filter(doc ->
                "Mudança Planejada".equals(doc.getHeader().getCategory().getDescription())).count();
    }

    public Map<String, Long> getClassifications() {
        return issues.getDocument().stream().collect(Collectors.groupingBy(doc -> doc.getHeader().getClassification().getName(),
                Collectors.counting()));
    }

    public Map<String, Long> getCustomers() {
        return issues.getDocument().stream().collect(Collectors.groupingBy(doc -> doc.getHeader().getDemandingCustomer().getName(),
                Collectors.counting()));
    }

    public int getQtdPorStatus(String status){
        int i = 0;
        for (DocumentModel documentModel : issues.getDocument()) {
            String sts = documentModel.getHeader().getStatus().getName();
            if (sts.equals(status))
                i++;
        }
        return i;
    }
}